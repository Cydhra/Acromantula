package net.cydhra.acromantula.config

import com.uchuhimo.konf.ConfigSpec
import net.cydhra.acromantula.AcromantulaApp

/**
 * A global config spec that does not belong to any subsystem
 */
object GlobalConfigurationSpec : ConfigSpec() {
    val theme by optional(AcromantulaApp::class.java.getResource("/style/default.css").toExternalForm())
}
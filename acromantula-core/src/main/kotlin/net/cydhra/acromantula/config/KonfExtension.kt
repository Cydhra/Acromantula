package net.cydhra.acromantula.config

import com.uchuhimo.konf.Item
import kotlin.reflect.KProperty

operator fun <T> Item<T>.getValue(thisRef: Any, property: KProperty<*>): T {
    return ConfigurationManager.config[this]
}

operator fun <T> Item<T>.setValue(thisRef: Any, property: KProperty<*>, newValue: T) {
    ConfigurationManager.config[this] = newValue
}

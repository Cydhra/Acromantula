package net.cydhra.acromantula.config

import com.uchuhimo.konf.Config
import com.uchuhimo.konf.ConfigSpec
import com.uchuhimo.konf.source.json.toJson
import org.apache.logging.log4j.LogManager
import java.io.File

/**
 * A manager for configuration. Is not a component of the event bus
 */
object ConfigurationManager {

    private val logger = LogManager.getLogger()

    /**
     * The file where all the configuration is saved and restored from
     */
    private val settingsFile = File("configuration.json")

    /**
     * A set of [ConfigSpec] instances that are registered at the service. They are attached to the [Config] at every
     * reload
     */
    private val configurationSpecs: MutableSet<ConfigSpec> = mutableSetOf()

    /**
     * The current config instance. Every [reloadConfig] call assignes a new instance.
     */
    var config: Config = Config()
        private set

    init {
        this.addSpecification(GlobalConfigurationSpec)
    }

    /**
     * Add a specification to the configuration. To take effect in the model, reload the config with [reloadConfig]
     *
     * @param spec the [ConfigSpec] object
     */
    fun addSpecification(spec: ConfigSpec) {
        this.configurationSpecs += spec
    }

    /**
     * Reload the configuration with the current sources
     */
    fun reloadConfig() {
        this.config = Config {
            this@ConfigurationManager.configurationSpecs.forEach(this::addSpec)
        }.from.json.file(settingsFile.apply { takeIf { createNewFile() }?.writeText("{}") })
    }

    /**
     * Save the current configuration. The configuration must have been loaded before at least one time
     */
    fun saveConfig() {
        this.config.toJson.toFile(this.settingsFile.apply { createNewFile() })
        logger.info("saved configuration")
    }

}

package net.cydhra.acromantula.concurrency.tasks

import javafx.concurrent.Task
import net.cydhra.acromantula.workspace.AcromantulaTask

/**
 * An uncritical task that reads database contents. It works like [ContentOperationTask] but without acquiring
 * critical database access
 *
 * @param title the title of the task in GUI
 * @param msg the message displayed after the lock was acquired and the task starts work
 * @param operation the task function and state in a closure. Receiver is this task instance, so progress and message
 * can be updated
 *
 * @param T the return type of the task
 */
class ReadOperationTask<T>(title: String, private val msg: String,
                           private val operation: Task<T>.() -> T) : AcromantulaTask<T>() {

    init {
        this.updateTitle(title)
        this.updateMessage(this.msg)
    }

    override fun call(): T {
        // the actual operation should come in a closure with all necessary state.
        return this.let(operation)
    }
}
package net.cydhra.acromantula.concurrency.tasks

import kotlinx.coroutines.runBlocking
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.types.ContentOperation
import net.cydhra.acromantula.workspace.AcromantulaTask

/**
 * A task that schedules a content operation in the background. This operation must not perform critical operations
 * on the database. Instead, the operation should schedule a [ContentOperationTask] for any critical operations
 * performed.
 *
 * @param action the content operation to execute
 * @param fileBlob the file upon this action shall be executed
 * @param entity the entity of this operation
 */
class ActionTask<E>(private val action: ContentOperation<E>, private val fileBlob: FileBlob,
                    private val entity: E) : AcromantulaTask<Unit>() {

    init {
        this.updateTitle(action.name)
    }

    override fun call() {
        runBlocking {
            action.onAction(fileBlob, entity)
        }
    }
}
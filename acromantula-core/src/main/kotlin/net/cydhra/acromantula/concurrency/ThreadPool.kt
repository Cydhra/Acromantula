package net.cydhra.acromantula.concurrency

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.concurrent.Task
import javafx.concurrent.Worker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import org.apache.logging.log4j.LogManager
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * A facade to thread pools used by the application. There is a thread pool for operations that could block very long
 * times and a fixed size pool for coroutine dispatching.
 */
object ThreadPool {

    private val logger = LogManager.getLogger()

    /**
     * A cached thread pool for long living kernel threads that potentially block a long time such as workspace
     * operations waiting for other operations to finish. This pool is used to not drain the coroutine pool with
     * long-blocking threads waiting for each other.
     */
    val cachedThreadPool = Executors.newCachedThreadPool()!!

    /**
     * A fixed size thread pool for short-blocking user-level-threads
     */
    private val coroutinePool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())

    /**
     * A [CoroutineScope] for [coroutinePool]
     */
    val coroutineScope: CoroutineScope = CoroutineScope(coroutinePool.asCoroutineDispatcher())

    /**
     * A list of tasks that are scheduled in the thread pool and have not returned yet
     */
    val scheduledTaskList: ObservableList<Task<*>> = FXCollections.observableArrayList<Task<*>>()

    /**
     * A map of times when a task is started.
     */
    private val times = mutableMapOf<Task<*>, Long>()

    fun <T> submit(task: Task<T>, block: TaskContext<T>.() -> Unit) {
        val context = TaskContext(task).apply(block)

        // apply the handlers registered onto the context onto the task
        task.setOnSucceeded { event ->
            val result = (event.source as Worker<T>).value
            context.successHandlerList.forEach { it.invoke(result) }
            scheduledTaskList.remove(task)

            val time = System.currentTimeMillis() - times.remove(task)!!
            logger.debug(task.title + " finished in ${timeFormat(time)}")
        }
        task.setOnFailed { event ->
            context.failHandlerList.forEach { it.invoke(event.source.exception) }
            scheduledTaskList.remove(task)

            val time = System.currentTimeMillis() - times.remove(task)!!
            logger.debug(task.title + " failed after ${timeFormat(time)}")
        }
        task.setOnCancelled {
            context.cancelHandlerList.forEach { it.invoke() }
            scheduledTaskList.remove(task)

            val time = System.currentTimeMillis() - times.remove(task)!!
            logger.debug(task.title + " cancelled after ${timeFormat(time)}")
        }

        task.setOnRunning {
            times[task] = System.currentTimeMillis()
        }

        scheduledTaskList += task
        cachedThreadPool.submit(task)
    }

    fun shutdown() {
        logger.info("shutdown task thread pool...")
        cachedThreadPool.shutdown()
        logger.info("shutdown worker thread pool...")
        coroutinePool.shutdown()
        logger.info("thread pools successfully shut down.")
    }

    /**
     * Format the time a task ran in a readable minute format for output
     *
     * @param time the time delta a task was running
     *
     * @return a human-readable string displaying the time delta
     */
    private fun timeFormat(time: Long): String {
        return if (time > 1000)
            String.format("%d:%02d min",
                    TimeUnit.MILLISECONDS.toMinutes(time),
                    TimeUnit.MILLISECONDS.toSeconds(time) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))
            )
        else
            "$time ms"
    }

    /**
     * A context used as a receiver to a block function that can apply handlers to a submitted task.
     *
     * @see [submit]
     */
    class TaskContext<T>(private val task: Task<T>) {
        private val successHandler = mutableListOf<(T) -> Unit>()
        private val failHandler = mutableListOf<(Throwable) -> Unit>()
        private val cancelledHandler = mutableListOf<() -> Unit>()

        val successHandlerList: List<(T) -> Unit> = successHandler
        val failHandlerList: List<(Throwable) -> Unit> = failHandler
        val cancelHandlerList: List<() -> Unit> = cancelledHandler

        /**
         * Register a handler that is executed in case the task succeeds.
         *
         * @param handler a function that consumes the task's result
         */
        fun onSuccess(handler: (T) -> Unit) {
            successHandler += handler
        }

        /**
         * Register a handler that is executed in case the task fails
         *
         * @param handler a function that consumes the exception the task produced
         */
        fun onFailure(handler: (Throwable) -> Unit) {
            failHandler += handler
        }

        /**
         * Register a handler that is executed in case a task is cancelled by the user
         *
         * @param handler a function that doesn't take arguments
         */
        fun onCancelled(handler: () -> Unit) {
            cancelledHandler += handler
        }
    }
}
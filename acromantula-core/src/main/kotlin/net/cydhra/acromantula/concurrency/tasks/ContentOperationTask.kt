package net.cydhra.acromantula.concurrency.tasks

import javafx.concurrent.Task
import net.cydhra.acromantula.workspace.AcromantulaTask
import net.cydhra.acromantula.workspace.SynchronizationHelper

/**
 * A generic task to perform lightweight operations on the database content with the critical lock. This task blocks
 * until it gets the block. This task is not used to schedule
 * [net.cydhra.acromantula.workspace.handler.types.ContentOperation]s. Instead, this task is submitted by those
 * operations for critical work. To submit operations, use [ActionTask] instead.
 *
 * @param title the title of the task in GUI
 * @param msg the message displayed after the lock was acquired and the task starts work
 * @param operation the task function and state in a closure. Receiver is this task instance, so progress and message
 * can be updated
 *
 * @param T the return type of the task
 */
class ContentOperationTask<T>(title: String, private val msg: String,
                              private val operation: Task<T>.() -> T) : AcromantulaTask<T>() {

    init {
        this.updateTitle(title)
    }

    override fun call(): T {
        this.updateMessage("Waiting for critical task to finish...")

        try {
            SynchronizationHelper.acquireCriticalDatabaseAccess()

            this.updateMessage(this.msg)

            // the actual operation should come in a closure with all necessary state.
            return this.let(operation)
        } finally {
            SynchronizationHelper.releaseCriticalDatabaseAccess()
        }
    }
}
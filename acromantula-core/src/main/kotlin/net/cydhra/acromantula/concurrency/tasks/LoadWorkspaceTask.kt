package net.cydhra.acromantula.concurrency.tasks

import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.archive.ArchiveDirectoryEntry
import net.cydhra.acromantula.files.model.archive.ArchiveFileEntry
import net.cydhra.acromantula.files.model.database.Archive
import net.cydhra.acromantula.files.model.database.Directory
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.FileBlobs
import net.cydhra.acromantula.files.model.file.SingleFileEntry
import net.cydhra.acromantula.workspace.AcromantulaTask
import org.jetbrains.exposed.sql.transactions.transaction

class LoadWorkspaceTask : AcromantulaTask<List<WorkspaceEntry>>() {

    init {
        this.updateTitle("Load Workspace")
    }

    override fun call(): List<WorkspaceEntry> {
        val result = mutableListOf<WorkspaceEntry>()

        transaction {
            // add archives
            Archive.all().forEach { archive ->
                this@LoadWorkspaceTask.updateMessage("load ${archive.name}...")
                result += constructArchiveTree(archive)
            }

            // add single class entries
            FileBlob.find { FileBlobs.archive.isNull() }.forEach { blob ->
                result += SingleFileEntry(blob.filename, blob.fileType)
            }
        }

        return result
    }

    /**
     * Construct the whole [WorkspaceEntry] tree from the database persisted version
     *
     * @param archive the database entity representing the archive tree root
     *
     * @param the [WorkspaceEntry] representing the archive tree
     */
    private fun constructArchiveTree(archive: Archive): WorkspaceEntry {
        val javaArchive = net.cydhra.acromantula.files.model.archive.Archive(archive.name)

        archive.rootDirectories.forEach { dir ->
            val entry = constructDirectoryTree(javaArchive, dir)
            entry.registerAtParent()
        }

        archive.rootFiles.forEach { file ->
            val fileEntry = ArchiveFileEntry(javaArchive, file.filename, file.fileType)
            fileEntry.registerAtParent()
        }

        return javaArchive
    }

    /**
     * Construct the subtree of a directory in an archive tree
     *
     * @param parent the parent [WorkspaceEntry] of the directory to be constructed (another directory or the archive)
     * @param directory the directory database entity
     *
     * @param the [WorkspaceEntry] representing the archive subtree
     */
    private fun constructDirectoryTree(parent: WorkspaceEntry, directory: Directory): ArchiveDirectoryEntry {
        val directoryEntry = ArchiveDirectoryEntry(parent, directory.name)

        directory.subDirectories.forEach { dir ->
            val entry = constructDirectoryTree(directoryEntry, dir)
            entry.registerAtParent()
        }

        directory.files.forEach { file ->
            val fileEntry = ArchiveFileEntry(directoryEntry, file.filename, file.fileType)
            fileEntry.registerAtParent()
        }

        return directoryEntry
    }
}
package net.cydhra.acromantula.concurrency.tasks

import kotlinx.coroutines.*
import net.cydhra.acromantula.files.FileTypeRegistry
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.workspace.AcromantulaTask
import net.cydhra.acromantula.workspace.SynchronizationHelper
import org.apache.logging.log4j.LogManager

/**
 * This task is launched after files have been imported into workspace. The top-level workspace entry is passed as an
 * argument. This tasks calls the file-specific [FileProcessors][FileProcessor] for each file imported. This task
 * performs critical alteration of the database and thus acquires a lock on it.
 */
class FileProcessingTask(private val workspaceEntry: WorkspaceEntry) : AcromantulaTask<Unit>() {

    private val logger = LogManager.getLogger()

    init {
        this.updateTitle("Processing of ${workspaceEntry.displayName}")
    }

    override fun call() {
        this.updateMessage("delegate work...")

        val job = SupervisorJob()
        val scope = CoroutineScope(localDispatcher + job)
        val databaseScope = CoroutineScope(databaseDispatcher + job)
        var round = 1

        this.updateMessage("waiting for critical task to finish...")
        SynchronizationHelper.acquireCriticalDatabaseAccess()

        do {
            this.updateMessage("processing round $round...")
            val jobs = handleEntry(workspaceEntry, scope, databaseScope, round)

            val maxWork = jobs.size.toLong()
            this.updateProgress(0L, maxWork)

            val moreRounds = runBlocking {
                var index = 0L
                jobs.fold(false) { acc, job ->
                    val moreRounds = job.await()
                    this@FileProcessingTask.updateProgress(++index, maxWork)
                    return@fold moreRounds or acc
                }
            }
            logger.debug("processing round $round complete")
            round++
        } while (moreRounds)

        SynchronizationHelper.releaseCriticalDatabaseAccess()
    }

    /**
     * Recursively handle all workspace entries by calling [FileTypeRegistry.processFile] on each of them. Each
     * processing call is launched in a new coroutine
     *
     * @param workspaceEntry a part of a [WorkspaceEntry] tree that is either the root of a sub-tree to process or a
     * [FileEntry] itself
     * @param scope the scope where to dispatch the processing for each file
     * @param synchronizedDatabaseScope a single-thread dispatcher for database accesses that have to be concurrent
     * @param round the current processing round
     *
     * @return a list of [Job] instances. One for each [FileEntry] in [workspaceEntry]
     */
    private fun handleEntry(workspaceEntry: WorkspaceEntry, scope: CoroutineScope,
                            synchronizedDatabaseScope: CoroutineScope, round: Int): List<Deferred<Boolean>> {
        return if (workspaceEntry is FileEntry) {
            listOf(scope.async {
                FileTypeRegistry.processFile(workspaceEntry.fileBlob, synchronizedDatabaseScope, round)
            })
        } else {
            workspaceEntry.children
                    .fold(mutableListOf()) { list, child ->
                        list.apply {
                            addAll(handleEntry(child, scope,
                                    synchronizedDatabaseScope, round))
                        }
                    }
        }
    }
}
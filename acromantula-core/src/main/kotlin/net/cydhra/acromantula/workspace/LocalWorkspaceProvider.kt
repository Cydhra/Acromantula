package net.cydhra.acromantula.workspace

import javafx.application.Platform
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.global.AddFilesEvent
import net.cydhra.acromantula.bus.event.local.UserInterfaceInitializedEvent
import net.cydhra.acromantula.files.FileHandlerService
import net.cydhra.acromantula.files.FileTypeRegistry
import net.cydhra.acromantula.files.model.RegisteredFileTypes
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.gui.controller.utility.showErrorDialog
import net.cydhra.acromantula.workspace.importer.FileImporter
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

/**
 * Provides access to a [WorkspaceServer] that is running locally. No network interaction is required.
 */
class LocalWorkspaceProvider(workspaceConfiguration: WorkspaceConfiguration) : WorkspaceProvider {

    private val logger = LogManager.getLogger()

    private val internalServer = WorkspaceServer(workspaceConfiguration)

    override suspend fun initialize() {
        EventBroker.registerEventListener(WorkspaceService, UserInterfaceInitializedEvent::class,
                this::onUserInterfaceInitialized)
        internalServer.initialize()

        // load workspace settings
        transaction {
            FileTypeRegistry.setFileTypeMapping(
                    RegisteredFileTypes.selectAll()
                            .map { it[RegisteredFileTypes.fileTypeDescriptor] to it[RegisteredFileTypes.id].value })
        }
    }

    override suspend fun disconnect() {
        internalServer.shutdown()
    }

    override suspend fun importFile(files: Array<out File>) {
        if (files.isNullOrEmpty()) {
            return
        }

        FileImporter.importFiles(files = *files) {
            onSuccess { results ->
                val errors = mutableListOf<Throwable>()

                results.forEach { result ->
                    if (result.isSuccess) {
                        val entry = result.getOrThrow()
                        logger.info("successfully imported archive ${entry.name}")
                        notifyAboutUpdate(entry)
                    } else {
                        errors += result.exceptionOrNull()!!
                    }
                }

                if (errors.isNotEmpty()) {
                    Platform.runLater {
                        showErrorDialog("${if (errors.size > 1) "Errors" else "Error"} while importing",
                                errors.map { err ->
                                    "Unexpected exception: ${err.message}"
                                    logger.error("error while importing", err)
                                }.joinToString("\n")
                        )
                    }
                }
            }
        }
    }

    /**
     * Notify the event bus about newly imported files
     */
    private fun notifyAboutUpdate(entry: WorkspaceEntry) {
        EventBroker.fireEvent(AddFilesEvent(entry, true))
    }

    @Suppress("RedundantSuspendModifier")
    private suspend fun onUserInterfaceInitialized(@Suppress("UNUSED_PARAMETER") event: UserInterfaceInitializedEvent) {
        FileHandlerService.loadWorkspace()
    }
}
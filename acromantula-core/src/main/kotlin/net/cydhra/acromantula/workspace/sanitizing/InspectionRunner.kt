package net.cydhra.acromantula.workspace.sanitizing

import net.cydhra.acromantula.files.model.database.FileBlob

/**
 *
 * @param E entity type of this runner
 */
abstract class InspectionRunner<E : Any>(protected val fileBlob: FileBlob) {

    /**
     * The entity handled by this runner
     */
    private lateinit var entity: E

    /**
     * whether the [entity] already has been generated
     */
    private var wasGenerated: Boolean = false

    /**
     * Run an inspection upon the entity handled by this runner
     *
     * @param inspection inspection for an entity of this runner type
     *
     * @return issues that appeared during inspection
     */
    fun runInspection(inspection: SanitizerInspection<E>): List<InspectionIssue<E>> {
        if (!wasGenerated) {
            this.entity = generateEntity()
            wasGenerated = true
        }
        return inspection.inspect(this.entity)
    }

    /**
     * Write back the entity into the database. It is assumed, that critical database access has been acquired by the
     * current thread.
     */
    fun writeBackEntity() {
        if (wasGenerated)
            saveEntity(entity)
    }

    /**
     * @return the entity of this runner
     */
    fun getEntity(): E {
        if (!wasGenerated)
            this.entity = generateEntity()

        return this.entity
    }

    /**
     * Generate the entity of this runner from the [fileBlob]
     *
     * @return generated entity instance
     */
    protected abstract fun generateEntity(): E

    /**
     * Write back the entity into database. This assumes that a transaction is running and critical database access
     * is held by the current thread
     */
    protected abstract fun saveEntity(entity: E)
}
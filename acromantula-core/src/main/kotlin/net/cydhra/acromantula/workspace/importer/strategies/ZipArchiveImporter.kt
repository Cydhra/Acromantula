package net.cydhra.acromantula.workspace.importer.strategies

import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.workspace.importer.ImporterStrategy
import net.cydhra.acromantula.workspace.importer.tools.ArchiveTreeBuilder
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.ByteArrayInputStream
import java.io.File
import java.util.jar.JarFile
import java.util.zip.ZipEntry
import java.util.zip.ZipException
import java.util.zip.ZipInputStream

/**
 * Strategy to import java archives into workspace
 */
object ZipArchiveImporter : ImporterStrategy {

    private val logger = LogManager.getLogger()

    /**
     * Try parsing the file as a zip file. If successful, return true. Otherwise false
     */
    override fun canImport(file: File, fileContent: ByteArray): Boolean {
        try {
            JarFile(file)
            return true
        } catch (e: ZipException) {
        }

        return false
    }

    override fun import(fileName: String, fileContent: ByteArray): WorkspaceEntry {
        val zipInputStream = ZipInputStream(ByteArrayInputStream(fileContent))
        val treeBuilder = ArchiveTreeBuilder(fileName)

        return transaction {
            var currentEntry: ZipEntry? = zipInputStream.nextEntry
            while (currentEntry != null) {
                this@ZipArchiveImporter.logger.trace("importing ${currentEntry.name}")

                if (!currentEntry.isDirectory) {
                    val entryContent: ByteArray = zipInputStream.readBytes()

                    treeBuilder.addFileEntry(currentEntry.name, entryContent)
                }

                currentEntry = zipInputStream.nextEntry
            }

            return@transaction treeBuilder.create()
        }
    }
}
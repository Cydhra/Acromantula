package net.cydhra.acromantula.workspace.importer.tools

import org.apache.logging.log4j.LogManager
import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode

/**
 * A utility object that helps with class file parsing
 */
object ClassParser {

    private val logger = LogManager.getLogger()

    /**
     * Checks whether the array starts with the magic bytes of a java class file.
     *
     * @param content a byte array of at least size 4
     *
     * @param true, if the given byte array starts with the magic bytes of a java class
     */
    fun checkMagicBytes(content: ByteArray): Boolean {
        if (content.size < 4)
            return false

        return content.copyOfRange(0, 4)
                .contentEquals(byteArrayOf(0xCA.toByte(), 0xFE.toByte(), 0xBA.toByte(), 0xBE.toByte()))
    }

    /**
     * Tries to parse a byte array as a class node
     *
     * @param content a byte array that supposedly is the content of a class file
     *
     * @return a [ClassNode] of the parsed content or null, if parsing failed
     */
    fun tryParseClass(content: ByteArray): ClassNode? {
        if (!checkMagicBytes(content))
            return null

        return ClassNode().apply node@{
            ClassReader(content).apply {
                try {
                    this.accept(this@node, ClassReader.EXPAND_FRAMES)
                } catch (e: Exception) {
                    logger.error("error while class parsing", e)
                }
            }
        }
    }
}
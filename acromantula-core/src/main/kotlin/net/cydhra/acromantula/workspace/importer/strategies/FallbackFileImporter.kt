package net.cydhra.acromantula.workspace.importer.strategies

import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.file.SingleFileEntry
import net.cydhra.acromantula.workspace.importer.ImporterStrategy
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

/**
 * Import strategy for single files outside of archives
 */
object FallbackFileImporter : ImporterStrategy {

    /**
     * Fallback importer that can import everything
     *
     * @return true
     */
    override fun canImport(file: File, fileContent: ByteArray): Boolean {
        return true
    }

    override fun import(fileName: String, fileContent: ByteArray): WorkspaceEntry {
        return transaction {
            val blob = insertBlobIntoDatabase(fileName, fileContent, null, null, emptyList())

            return@transaction SingleFileEntry(
                    name = fileName,
                    fileType = blob.fileType
            )
        }
    }

}
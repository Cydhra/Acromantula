package net.cydhra.acromantula.workspace

import javafx.concurrent.Task
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.newSingleThreadContext

/**
 * A proxy for JavaFX tasks that offers two thread pools for hybrid coroutine contexts. One threadpool is a
 * single-thread pool for critical database tasks and one offers multiple threads for non-critical work done by
 * coroutines.
 */
abstract class AcromantulaTask<T> : Task<T>() {
    companion object {
        @JvmStatic
        val localDispatcher = newFixedThreadPoolContext(Runtime.getRuntime().availableProcessors() * 4,
                "Worker Pool")

        @JvmStatic
        val databaseDispatcher = newSingleThreadContext("Database Dispatcher")
    }
}
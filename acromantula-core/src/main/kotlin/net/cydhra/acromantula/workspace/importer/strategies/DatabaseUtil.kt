package net.cydhra.acromantula.workspace.importer.strategies

import net.cydhra.acromantula.files.FileTypeRegistry
import net.cydhra.acromantula.files.model.database.Archive
import net.cydhra.acromantula.files.model.database.Directory
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.FileMetaDataPair
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * Insert a new blob into database containing the file contents of given entry
 *
 * @param name the name of the file containing this blob
 * @param entryContent the content of the given entry
 * @param owner the archive owning this file blob or null, if it is not inside an archive
 * @param parent the parent directory of the file
 *
 * @return the [FileBlob] that has been inserted into the database
 */
fun insertBlobIntoDatabase(name: String, entryContent: ByteArray, owner: Archive?, parent: Directory?,
                           metadata: List<Pair<String, String>>):
        FileBlob {
    assert(TransactionManager.currentOrNull() != null)

    return transaction {
        val blob = FileBlob.new {
            this.filename = name
            this.parent = parent
            this.archive = owner
            this.data = connection.createBlob().apply { setBytes(1, entryContent) }
        }.apply(FileTypeRegistry::setFileType)

        metadata.forEach { (key, value) ->
            FileMetaDataPair.new {
                this.file = blob.id
                this.key = key
                this.data = value
            }
        }

        return@transaction blob
    }
}
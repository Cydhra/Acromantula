package net.cydhra.acromantula.workspace.sanitizing

/**
 * A strategy interface that is used to scan entities for any type of issue. For example a java class may
 * contain illegal attributes that are not harmful during execution but were set by an obfuscator to confuse bytecode
 * instrumenting. This strategy is supposed to be used for any type of file entity edited within the application.
 */
interface SanitizerInspection<E : Any> {

    /**
     * Display name of the inspection
     */
    val name: String

    /**
     * Whether the inspection is critical to the proper functionality of other tools. For example, an obfuscator
     * might choose to intentionally break certain requirements of the specification in order to break assumptions
     * made by tools. If the inspection intends to detect and fix those, it should be marked as critical, as critical
     * inspections are run before any tool.
     */
    val isCritical: Boolean

    /**
     * Inspect the given entity and generate a list of issues with it. The list can be empty if no issues are
     * present
     *
     * @param entity the entity to be inspected
     *
     * @return a potentially empty list of issues affecting this entity
     */
    fun inspect(entity: E): List<InspectionIssue<E>>
}
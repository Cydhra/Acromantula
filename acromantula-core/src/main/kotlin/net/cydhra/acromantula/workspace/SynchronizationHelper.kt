package net.cydhra.acromantula.workspace

import javafx.concurrent.Task
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.global.FileChangedEvent
import net.cydhra.acromantula.files.model.database.FileBlob
import java.util.concurrent.Semaphore

@Suppress("UNCHECKED_CAST")
/**
 * A service for background execution of [Task] implementations. It also handles concurrency of critical database
 * accesses and collects information about data model changes by tasks performing those accesses.
 *
 * TODO: balancing critical access only on the client is not very effective. There can only be one critical access on
 *  the server
 */
object SynchronizationHelper {

    /**
     * A semaphore that guarantees that no more than one operation on files in the workspace in database can happen
     * at the same time. This eliminates the chance of side-effects by database operation race conditions and ensures
     * that the database is in consistent state before and after a transaction. It also allows easier generation of a
     * patch history allowing undo and redo of operations. Note that using the semaphore is by design voluntarily. A
     * greedy or malicious task could always open database transactions without actually telling the
     * [SynchronizationHelper] thus endangering the database state and rendering undo impossible.
     */
    private val databaseWritingSemaphore = Semaphore(1, true)

    /**
     * A list of critical file changes that must be reported to the GUI after a critical access has been released.
     */
    private val criticalFileChanges = mutableListOf<FileBlob>()


    /**
     * Acquire the right to access the database with critical writing access. This method blocks the asking thread
     * until access can be obtained. Calling this method is expected by any thread that wants to write into the
     * database, but by design it cannot be enforced. This also allows tasks to write data into the database that does
     * not risk any side effects by critical tasks running simultaneously, without actually calling this method.
     */
    fun acquireCriticalDatabaseAccess() {
        databaseWritingSemaphore.acquire()
    }

    /**
     * Report that file content has been changed. As soon as [releaseCriticalDatabaseAccess] is called, the reported
     * file change will trigger GUI updates. This can only be used while critical database access is being held by
     * any task.
     *
     * @throws IllegalStateException if no critical database access was acquired before calling this method.
     */
    fun reportFileChange(fileBlob: FileBlob) {
        if (databaseWritingSemaphore.tryAcquire()) {
            databaseWritingSemaphore.release()
            throw IllegalStateException("No critical database access is held at the moment thus no file change can be" +
                    " reported.")
        }

        synchronized(criticalFileChanges) {
            criticalFileChanges.add(fileBlob)
        }
    }

    /**
     * Release critical database access previously acquired calling [acquireCriticalDatabaseAccess]
     */
    fun releaseCriticalDatabaseAccess() {
        synchronized(criticalFileChanges) {
            criticalFileChanges.forEach { file ->
                EventBroker.fireEvent(FileChangedEvent(file))
            }
            criticalFileChanges.clear()
        }

        databaseWritingSemaphore.release()
    }
}


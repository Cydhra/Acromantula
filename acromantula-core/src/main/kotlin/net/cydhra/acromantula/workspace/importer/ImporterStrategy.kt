package net.cydhra.acromantula.workspace.importer

import net.cydhra.acromantula.files.model.WorkspaceEntry
import java.io.File

/**
 * Strategy to import a file into the workspace. Each strategy is supposed to import another file type. Before a file is imported,
 * [canImport] is called with the file, which can decide whether this strategy is applicable to import the given file. If true is returned,
 * [import] is called.
 */
interface ImporterStrategy {

    /**
     * Decides whether this strategy is applicable to import a given file
     *
     * @param file [File] instance of the file that shall be imported
     * @param fileContent content of the file that shall be imported
     *
     * @return true, if this strategy is able to import the file
     */
    fun canImport(file: File, fileContent: ByteArray): Boolean

    /**
     * Import a file and return a [WorkspaceEntry] containing the file contents. This entry can possibly hold a tree of
     * [WorkspaceEntries][WorkspaceEntry]
     *
     * @param fileName name of the imported file
     * @param fileContent file content byte array
     *
     * @return a [WorkspaceEntry] containing the imported file contents
     */
    fun import(fileName: String, fileContent: ByteArray): WorkspaceEntry
}
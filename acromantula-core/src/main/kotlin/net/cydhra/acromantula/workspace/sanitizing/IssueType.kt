package net.cydhra.acromantula.workspace.sanitizing

/**
 * A type of issue that can be raised by an inspection [SanitizerInspection]. This class is supposed to be extended
 * by objects, as instantiation of the same type for multiple instances of the same issue does not make sense.
 *
 * @param name name of the issue type
 * @param description a description of the type of issue
 * @param fixingOptions an array of option types that must be offered by issue instances to resolve the issue
 */
open class IssueType(val name: String, val description: String, vararg val fixingOptions: String)
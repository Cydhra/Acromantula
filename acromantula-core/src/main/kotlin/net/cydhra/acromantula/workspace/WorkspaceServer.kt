package net.cydhra.acromantula.workspace

import net.cydhra.acromantula.files.model.RegisteredFileTypes
import net.cydhra.acromantula.files.model.database.Archives
import net.cydhra.acromantula.files.model.database.Directories
import net.cydhra.acromantula.files.model.database.FileBlobs
import net.cydhra.acromantula.files.model.database.FileMetaData
import net.cydhra.acromantula.files.model.database.java.*
import org.apache.logging.log4j.LogManager
import org.h2.tools.Server
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * A workspace server backed by a database that potentially allows connections through a network.
 */
class WorkspaceServer(private val config: WorkspaceConfiguration) {

    private val logger = LogManager.getLogger()

    lateinit var server: Server

    fun initialize() {
        Database.connect(
                url = "${config.url};DB_CLOSE_DELAY=10;MVCC=true",
                driver = "org.h2.Driver",
                user = config.username,
                password = config.password)

        transaction {
            SchemaUtils.create(FileMetaData, RegisteredFileTypes, Archives, Directories, FileBlobs, JavaIdentifiers,
                    JavaClasses, JavaFields, JavaMethods, JavaReferences, JavaOverrides, JavaInterfaceRelations)
        }
        logger.info("successfully connected to database")
    }

    fun shutdown() {
        transaction {
            flushCache()
        }
    }

    fun startServer() {
        TODO()
    }
}
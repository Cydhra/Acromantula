package net.cydhra.acromantula.workspace.sanitizing

/**
 * An issue raised by an inspection.
 *
 * @param type the type of issue with further description
 * @param instanceDescription a description detailing this specific instance
 * @param affectedEntity the entity that is affected by this issue instance
 * @param proposedFixes a map of functions that takes the affected entity as parameter and fix the issue. Must
 * correspond to the fix options defined in the [IssueType]
 *
 * @param E the entity type that raised this inspection
 */
class InspectionIssue<E : Any>(val type: IssueType, val instanceDescription: String, val affectedEntity: E,
                               val proposedFixes: Map<String, (E) -> Unit>) {

    init {
        val missingFixingOption = type.fixingOptions.firstOrNull { !proposedFixes.keys.contains(it) }
        if (missingFixingOption != null)
            throw IllegalStateException("InspectioNIssue instance $this requires $missingFixingOption as an option")
    }
}
package net.cydhra.acromantula.workspace

import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.ApplicationCloseEvent
import net.cydhra.acromantula.bus.event.local.ConnectToWorkspaceRequest
import net.cydhra.acromantula.bus.event.local.ImportFilesRequest
import net.cydhra.acromantula.bus.service.Service
import net.cydhra.acromantula.concurrency.ThreadPool
import java.io.File

/**
 * Facade service for the workspace sub-system. Everything related to data storage and data operation is delegated
 * from here.
 */
object WorkspaceService : Service {

    override val name: String = "workspace-service"

    lateinit var currentWorkspace: WorkspaceProvider
        private set

    /**
     * Called upon application startup. Load default workspace and subscribe to events if necessary.
     */
    override suspend fun initialize() {
        currentWorkspace = loadDefaultWorkspace()
        currentWorkspace.initialize()

        EventBroker.registerEventListener(this, ApplicationCloseEvent::class, this::onApplicationClose)
        EventBroker.registerEventListener(this, ConnectToWorkspaceRequest::class, this::connectToWorkspace)
        EventBroker.registerEventListener(this, ImportFilesRequest::class, this::importFiles)
    }

    private suspend fun onApplicationClose(@Suppress("UNUSED_PARAMETER") event: ApplicationCloseEvent) {
        ThreadPool.shutdown()
        currentWorkspace.disconnect()
    }

    private suspend fun connectToWorkspace(event: ConnectToWorkspaceRequest) {
        currentWorkspace.disconnect()

        currentWorkspace = if (event.workspaceConfiguration.local) {
            LocalWorkspaceProvider(event.workspaceConfiguration)
        } else {
            RemoteWorkspaceProvider(event.workspaceConfiguration)
        }
        currentWorkspace.initialize()
        event.fulfill()
    }

    private suspend fun importFiles(event: ImportFilesRequest) {
        currentWorkspace.importFile(event.files)
    }

    /**
     * Open a default workspace that is created automatically for convenience.
     */
    private fun loadDefaultWorkspace(): WorkspaceProvider {
        return LocalWorkspaceProvider(
                WorkspaceConfiguration(
                        url = "jdbc:h2:file:${File("./tmp").absoluteFile.absolutePath}",
                        username = "",
                        password = "",
                        local = true
                )
        )
    }


}
package net.cydhra.acromantula.workspace.sanitizing

import com.google.common.collect.MultimapBuilder
import net.cydhra.acromantula.files.FileTypeRegistry
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.workspace.sanitizing.Sanitizer.sanitizeCritical
import kotlin.reflect.KClass

/**
 * The sanitizer can be invoked on any entity. If the entity type has inspections registered, those are executed upon
 * the entity and a list of issues is returned.
 *
 * @see sanitizeCritical
 */
object Sanitizer {

    /**
     * A multimap storing the inspections for all different entity classes.
     */
    private val inspections = MultimapBuilder.hashKeys().hashSetValues().build<KClass<*>, SanitizerInspection<*>>()

    /**
     * A multimap storing the inspections by entity type
     */
    private val groupedInspectionsMap =
            MultimapBuilder.hashKeys().hashSetValues().build<String, SanitizerInspection<*>>()

    /**
     * A view on the registered inspections grouped by their entity type
     */
    val groupedInspections: Map<String, Collection<SanitizerInspection<*>>> = groupedInspectionsMap.asMap()

    /**
     * Register an inspection for a given class of entities
     *
     * @param entityType the type of entity this inspection shall be run upon
     * @param entityTypeName friendly name of the entity type
     * @param inspection the inspection to register
     */
    fun <E : Any> registerInspection(entityType: KClass<E>, entityTypeName: String,
                                     inspection: SanitizerInspection<E>) {
        inspections.put(entityType, inspection)
        groupedInspectionsMap.put(entityTypeName, inspection)
    }

    /**
     * Run inspections marked as critical upon an entity.
     *
     * @param entity the entity to be inspected
     *
     * @return a potentially empty list of issues with this entity
     */
    @Suppress("UNCHECKED_CAST")
    fun <E : Any> sanitizeCritical(entity: E): List<InspectionIssue<E>> {
        return inspections.keySet()
                .filter { it.isInstance(entity) }
                .map { cls ->
                    inspections[cls]
                            .filter { it.isCritical }
                            .map { (it as SanitizerInspection<E>).inspect(entity) }.flatten()
                }
                .flatten()
    }

    /**
     * Execute registered runners to generate entities from given list of files and then run given inspections on the
     * generated entities.
     *
     * @param files the list of [FileEntries][FileEntry] that shall be sanitized
     * @param inspections the list of inspections that shall be run
     *
     * @return a list of issues that appeared during inspection upon the files
     */
    @Suppress("UNCHECKED_CAST")
    fun executeRunners(files: List<FileEntry>, inspections: List<String>):
            Pair<List<InspectionRunner<Any>>, List<InspectionIssue<Any>>> {
        val runners = files
                .map(FileEntry::fileBlob)
                .mapNotNull(FileTypeRegistry::generateRunner)

        return runners to runners.map { runner ->
            inspections
                    .mapNotNull { name ->
                        this.inspections[runner.getEntity().javaClass.kotlin].find { it.name == name }
                    }
                    .map { runner.runInspection(it as SanitizerInspection<Any>) }
                    .flatten()
        }.flatten()
    }
}
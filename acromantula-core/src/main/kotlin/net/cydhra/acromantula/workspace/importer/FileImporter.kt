package net.cydhra.acromantula.workspace.importer

import net.cydhra.acromantula.concurrency.ThreadPool
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.workspace.AcromantulaTask
import net.cydhra.acromantula.workspace.importer.FileImporter.registerImportStrategy
import net.cydhra.acromantula.workspace.importer.strategies.FallbackFileImporter
import net.cydhra.acromantula.workspace.importer.strategies.ZipArchiveImporter
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

/**
 * Facade instance to import files into the workspace. New strategies to import different file types can be added using
 * [registerImportStrategy]
 */
object FileImporter {

    private val logger = LogManager.getLogger()

    private val importerStrategies: MutableList<ImporterStrategy> = mutableListOf()

    init {
        registerImportStrategy(ZipArchiveImporter)
    }

    /**
     * Import an arbitrary amount of files into the workspace returning a list of [Results][Result] carrying the
     * [WorkspaceEntries][WorkspaceEntry] of the successfully imported files and the Exceptions thrown by failed
     * imports.
     *
     * @param files files to import
     * @param block a block statement registering handlers to the file import task
     * @param workspace the workspace that is ordering this import
     *
     * @return a list of [Results][Result] of type [WorkspaceEntry]
     */
    fun importFiles(vararg files: File, block: ThreadPool.TaskContext<List<Result<WorkspaceEntry>>>.() -> Unit) {
        ThreadPool.submit(FileImportTask(files), block)
    }

    /**
     * Register a new strategy to import files into the workspace.
     *
     * @param importerStrategy an implementation of [ImporterStrategy]
     */
    fun registerImportStrategy(importerStrategy: ImporterStrategy) {
        this.importerStrategies += importerStrategy
    }

    /**
     * A background task that imports an array of files. This task performs write accesses to the database. However,
     * they should not be critical, as only binary file contents are inserted. Operations working on data in the
     * workspace are expected to only include those file contents after the files have been processed and respective
     * entities have been inserted into the database. Since the processing is handled in another task, no lock on the
     * database is acquired by this task.
     *
     * @param files an array of files that shall be imported into the application
     */
    class FileImportTask(private val files: Array<out File>) : AcromantulaTask<List<Result<WorkspaceEntry>>>() {
        init {
            this.updateTitle("Import ${files.joinToString(", ")}")
        }

        override fun call(): List<Result<WorkspaceEntry>> {
            return transaction {
                return@transaction files.map { file ->
                    // before each file check if cancelled
                    if (this@FileImportTask.isCancelled) {
                        TransactionManager.current().rollback()
                        throw RuntimeException("cancelled")
                    }

                    // update log and gui message
                    this@FileImportTask.updateMessage(file.absolutePath)
                    FileImporter.logger.debug("importing: ${file.path}")

                    return@map Result.runCatching {
                        val fileContent = file.readBytes()

                        (importerStrategies.firstOrNull { strategy -> strategy.canImport(file, fileContent) }
                                ?: FallbackFileImporter)
                                .import(file.name, fileContent)
                    }
                }
            }
        }
    }

}
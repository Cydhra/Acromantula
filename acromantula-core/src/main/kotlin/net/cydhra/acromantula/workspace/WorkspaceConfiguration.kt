package net.cydhra.acromantula.workspace

/**
 * User defined options for a workspace that are required for the database connection.
 */
class WorkspaceConfiguration(val url: String, val username: String, val password: String, val local: Boolean)
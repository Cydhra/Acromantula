package net.cydhra.acromantula.workspace

import java.io.File

/**
 * This class is a workspace and provides any services related to workspace and workspace synchronization.
 */
interface WorkspaceProvider {

    /**
     * Called when the workspace is opened. Ensures creation of database, database layout and NATS client
     */
    suspend fun initialize()

    /**
     * Disconnects the workspace
     */
    suspend fun disconnect()


    /**
     * Import files into the workspace
     */
    suspend fun importFile(files: Array<out File>)
}
package net.cydhra.acromantula.files.utility.model

import org.jetbrains.exposed.dao.EntityID

abstract class JumpTarget() {
    abstract override fun toString(): String;
}

data class FileJumpTarget(val fileId: EntityID<Int>, val filename: String) : JumpTarget() {
    override fun toString(): String {
        return "Class File: $filename"
    }
}

data class ClassDeclarationJumpTarget(val identifier: String) : JumpTarget() {
    override fun toString(): String {
        return "Class Declaration: $identifier"
    }
}

data class MethodDeclarationJumpTarget(val identifier: String) : JumpTarget() {
    override fun toString(): String {
        return "Method Declaration: $identifier"
    }
}

data class FieldDeclarationJumpTarget(val identifier: String) : JumpTarget() {
    override fun toString(): String {
        return "Field Declaration: $identifier"
    }
}

data class InstructionJumpTarget(val methodIdentifier: String, val instructionNumber: Int) : JumpTarget() {
    override fun toString(): String {
        return "Member Reference: $methodIdentifier:$instructionNumber"
    }
}
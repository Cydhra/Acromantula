package net.cydhra.acromantula.files.model.archive

import net.cydhra.acromantula.files.model.DefaultWorkspaceEntry
import net.cydhra.acromantula.files.model.WorkspaceEntry

class Archive(name: String) : WorkspaceEntry by DefaultWorkspaceEntry(null, name, "/images/archive.svg") {
    override fun toString(): String {
        return this.name
    }
}
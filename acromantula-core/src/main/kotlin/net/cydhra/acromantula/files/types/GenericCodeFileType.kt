package net.cydhra.acromantula.files.types

import net.cydhra.acromantula.files.model.database.FileBlob

/**
 * A supertype for binary files containing compiled code. This type does nothing on its own, it is just used to
 * differ between data binary files and executable binary files.
 */
object GenericCodeFileType : FileType(GenericBinaryFileType / "code") {

    override val fileTypeImage: String = "/images/file_generic_code.svg"

    /**
     * Delegate the call to [GenericBinaryFileType]
     */
    override fun isFileConforming(file: FileBlob): Boolean {
        return GenericBinaryFileType.isFileConforming(file)
    }
}
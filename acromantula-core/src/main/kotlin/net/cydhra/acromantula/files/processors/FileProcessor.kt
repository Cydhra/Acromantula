package net.cydhra.acromantula.files.processors

import kotlinx.coroutines.CoroutineScope
import net.cydhra.acromantula.files.model.database.FileBlob

/**
 * A processor for a specific file type, that is triggered, whenever a file of that type is added to the workspace.
 */
interface FileProcessor {

    val processingRounds: Int
        get() = 1

    /**
     * Called whenever a file is added to the workspace matching the type this processor was registered for.
     *
     * @param fileBlob the new file in workspace
     * @param concurrentDatabaseAccessDispatcher a single-threaded dispatcher that runs every assigned coroutine
     * sequentially to avoid database conflicts. Used for critical database accesses, that could result in race
     * conditions or invalid insertions
     * @param round the current processing round. Each processor gets called as often in sequential rounds as
     * specified through [processingRounds]
     */
    suspend fun processFile(fileBlob: FileBlob, concurrentDatabaseAccessDispatcher: CoroutineScope, round: Int)
}
package net.cydhra.acromantula.files.inspections

import net.cydhra.acromantula.workspace.sanitizing.InspectionIssue
import net.cydhra.acromantula.workspace.sanitizing.SanitizerInspection
import org.objectweb.asm.Opcodes
import org.objectweb.asm.signature.SignatureReader
import org.objectweb.asm.signature.SignatureVisitor
import org.objectweb.asm.tree.ClassNode

/**
 * This inspection tries to parse the signatures of a class and its members and reports illegal signatures.
 */
object SignatureInspection : SanitizerInspection<ClassNode> {

    override val name: String = "Signature Inspection"

    override val isCritical: Boolean = true

    override fun inspect(entity: ClassNode): List<InspectionIssue<ClassNode>> {
        val resultList = mutableListOf<InspectionIssue<ClassNode>>()

        if (entity.signature != null)
            inspectSignature(
                    classNode = entity,
                    memberName = entity.name,
                    signature = entity.signature,
                    inDoubtFix = { it.signature = null })
                    ?.also { resultList += it }

        resultList += entity.fields
                .filter { it.signature != null }
                .mapNotNull { fieldNode ->
                    inspectSignature(
                            classNode = entity,
                            memberName = fieldNode.name,
                            signature = fieldNode.signature,
                            inDoubtFix = { fieldNode.signature = null })
                }
        resultList += entity.methods
                .filter { it.signature != null }
                .mapNotNull { methodNode ->
                    inspectSignature(
                            classNode = entity,
                            memberName = methodNode.name,
                            signature = methodNode.signature,
                            inDoubtFix = { methodNode.signature = null })
                }

        return resultList
    }

    /**
     * Try to parse a signature and report back if it fails.
     *
     * @param classNode the entity this inspection is reported on.
     * @param memberName the name of the affected class member. Mentioned in the instance description of the report
     * @param signature the signature to inspect
     * @param inDoubtFix the fix that is proposed, if the inspection reports an issue
     */
    private fun inspectSignature(classNode: ClassNode, memberName: String, signature: String,
                                 inDoubtFix: (ClassNode) -> Unit):
            InspectionIssue<ClassNode>? {
        try {
            val signatureReader = SignatureReader(signature)
            signatureReader.accept(object : SignatureVisitor(Opcodes.ASM6) {})
        } catch (e: IllegalArgumentException) {
            return InspectionIssue(
                    type = IllegalSignatureIssue,
                    instanceDescription = "signature of $memberName does not comply signature specification",
                    affectedEntity = classNode,
                    proposedFixes = mapOf(FIX_OPTION_ILLEGAL_SIGNATURE_DELETE to inDoubtFix))
        }

        return null
    }

}
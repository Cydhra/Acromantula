package net.cydhra.acromantula.files.model

import javafx.collections.ObservableList

/**
 * Used to mark an object as hierarchical data.
 * This object can then be used as data source for an hierarchical control, like the [javafx.scene.control.TreeView].
 *
 * @author Christian Schudt
 */
interface HierarchyData<T : HierarchyData<T>> {

    /**
     * The children collection, which represents the recursive nature of the hierarchy.
     * Each child is again a [HierarchyData].
     *
     * @return A list of children.
     */
    val children: ObservableList<T>
}
package net.cydhra.acromantula.files.operations.searching

import javafx.application.Platform
import javafx.collections.FXCollections
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.types.ContentOperation
import net.cydhra.acromantula.files.utility.model.CodeReference
import net.cydhra.acromantula.gui.controller.dialog.ShowReferencesDialogView
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog

/**
 * A operation to search references of a specified code entity and show a dialog allowing to jump to those references.
 */
abstract class SearchReferencesOperation<T> : ContentOperation<String>() {
    override suspend fun onAction(file: FileBlob, entity: String) {
        val databaseEntity = this.obtainEntityFromDatabase(entity)
        val affectedEntities = this.collectReferences(databaseEntity)
        val dialog = ShowReferencesDialogView(FXCollections.observableArrayList(affectedEntities), entity)

        Platform.runLater {
            showStageCallbackDialog(
                    title = "Show References",
                    content = dialog,
                    resolveOptions = *arrayOf("Close" to { _ -> })
            )
        }
    }

    /**
     * Obtain the actual entity this operation is performed upon from database. The entity is only referenced using a
     * string representation from user interface.
     *
     * @param entityIdentification a unique string representation of the entity given by the user interface
     *
     * @return the entity from database that this operation is performed upon
     */
    protected abstract fun obtainEntityFromDatabase(entityIdentification: String): T

    /**
     * Collect all references to the given entity from database.
     */
    protected abstract fun collectReferences(entity: T): List<CodeReference>
}
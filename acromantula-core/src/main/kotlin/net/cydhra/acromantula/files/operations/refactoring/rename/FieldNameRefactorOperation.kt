package net.cydhra.acromantula.files.operations.refactoring.rename

import javafx.application.Platform
import javafx.collections.FXCollections
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.operations.refactoring.RefactorOperation
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableEntity
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableMember
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableReference
import net.cydhra.acromantula.files.operations.refactoring.remapper.FieldNameRemapper
import net.cydhra.acromantula.files.utility.constructFieldIdentity
import net.cydhra.acromantula.files.utility.model.CodeReference
import net.cydhra.acromantula.files.utility.model.FieldDeclarationJumpTarget
import net.cydhra.acromantula.files.utility.model.InstructionJumpTarget
import net.cydhra.acromantula.gui.controller.dialog.RenameElementDialogView
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog
import net.cydhra.acromantula.workspace.AcromantulaTask
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.tree.ClassNode
import java.util.concurrent.CompletableFuture

/**
 * Operation that renames fields and all their references
 */
object FieldNameRefactorOperation : RefactorOperation<JavaField, FieldNameRefactorData>() {
    override val name: String = "Rename Field"

    override fun obtainEntityFromDatabase(entityIdentification: String): JavaField {
        return transaction {
            val identifierId = JavaIdentifier.find { JavaIdentifiers.identifier eq entityIdentification }.single().id
            JavaField.find { JavaFields.identifier eq identifierId }.first()
        }
    }

    override fun collectRefactorableEntities(entity: JavaField): List<RefactorableEntity> {
        val referenceList = mutableListOf<RefactorableEntity>()
        referenceList += transaction {
            RefactorableMember(CodeReference(entity.owner.file,
                    FieldDeclarationJumpTarget(entity.identifier.identifier)), entity)
        }

        // map the reference list into an entity model usable by GUI and callbacks
        referenceList += transaction {
            JavaReference
                    .find { JavaReferences.referenced eq entity.identifier.id }
                    .map { ref ->
                        RefactorableReference(CodeReference(ref.referenceSource.owner.file,
                                InstructionJumpTarget(ref.referenceSource.identifier.identifier, ref.instruction)),
                                ref.referenceSource)
                    }
        }

        return referenceList
    }

    override suspend fun generateRefactorTarget(entity: JavaField,
                                                affectedEntities: List<RefactorableEntity>): FieldNameRefactorData? {
        val dialog = RenameElementDialogView(FXCollections.observableArrayList(affectedEntities), entity.name)

        val dataFuture = CompletableFuture<FieldNameRefactorData?>()

        Platform.runLater {
            showStageCallbackDialog(
                    title = "Rename " + entity.name,
                    content = dialog,
                    resolveOptions = *arrayOf(
                            "Rename" to rename@{ _ ->
                                transaction {
                                    dataFuture.complete(FieldNameRefactorData(entity.owner.name, entity.name,
                                            entity.descriptor, dialog.getNewMemberName()))
                                }
                            },
                            "Cancel" to { _ -> dataFuture.complete(null) })
            )
        }

        return withContext(CoroutineScope(AcromantulaTask.localDispatcher).coroutineContext) {
            dataFuture.get()
        }
    }

    override fun refactorCodeFile(classFile: FileBlob, classNode: ClassNode, refactorData: FieldNameRefactorData): ClassWriter {
        val writer = ClassWriter(0)

        // remap the class and write the result into the writer
        classNode.accept(ClassRemapper(writer,
                FieldNameRemapper(
                        fieldOwner = refactorData.fieldOwnerName,
                        originalFieldName = refactorData.oldFieldName,
                        fieldDescriptor = refactorData.fieldDescr,
                        newName = refactorData.newFieldName)))

        return writer
    }

    override fun refactorEntity(entity: RefactorableEntity, refactorData: FieldNameRefactorData) {
        if (entity is RefactorableMember<*>) {
            with(entity.member as JavaField) {
                name = refactorData.newFieldName
                identifier.identifier = constructFieldIdentity(this.owner.identifier.identifier, refactorData
                        .newFieldName, refactorData.fieldDescr)
            }
        }
    }

}

/**
 * An entity holding all information necessary to perform a remapping of a field name.
 *
 * @param fieldOwnerName name of the class declaring the field
 * @param oldFieldName name of the field before remapping
 * @param newFieldName new name for the field
 * @param fieldDescr field descriptor for unique identification
 */
data class FieldNameRefactorData(val fieldOwnerName: String, val oldFieldName: String,
                                 val fieldDescr: String, val newFieldName: String)
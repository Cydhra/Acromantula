package net.cydhra.acromantula.files.operations.refactoring

import javafx.application.Platform
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableEntity
import net.cydhra.acromantula.files.types.ContentOperation
import net.cydhra.acromantula.gui.controller.ResolveIssuesDialogController
import net.cydhra.acromantula.gui.controller.utility.showErrorDialog
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog
import net.cydhra.acromantula.workspace.AcromantulaTask
import net.cydhra.acromantula.workspace.SynchronizationHelper
import net.cydhra.acromantula.workspace.importer.tools.ClassParser
import net.cydhra.acromantula.workspace.sanitizing.InspectionIssue
import net.cydhra.acromantula.workspace.sanitizing.Sanitizer
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.tree.ClassNode
import java.util.concurrent.CompletableFuture

/**
 * A general strategy for refactoring code members. Implementing subclasses should be objects, as no state between
 * refactorings should remain. The strategy will ask the subclass to retrieve the entity and all its references,
 * generate the ASM nodes from it, sanitize the code and then ask the subclass to perform refactoring on all affected
 * classes and database entities.
 *
 * @param T the refactored entity type
 * @param D a data entity that holds necessary information about concrete entity refactoring
 */
abstract class RefactorOperation<T, D> : ContentOperation<String>() {

    companion object {
        private val logger = LogManager.getLogger()
    }

    /**
     * Outlines the general approach to refactor code entities. The actual refactoring of database contents and code
     * is performed by concrete implementations of this strategy. Gathering references and calling update functions
     * on them is done by this general approach.
     */
    override suspend fun onAction(file: FileBlob, entity: String) {
        val databaseEntity = this.obtainEntityFromDatabase(entity)
        val refactorEntities: List<RefactorableEntity> = this.collectRefactorableEntities(databaseEntity)
        val affectedFiles = refactorEntities.map(RefactorableEntity::codeFileBlob).toHashSet()
        val refactorData = generateRefactorTarget(databaseEntity, refactorEntities) ?: return

        // generate required class nodes from code files
        val classNodes = transaction {
            affectedFiles
                    .mapNotNull { FileBlob.reload(it) }
                    .map(FileBlob::data)
                    .map { it.getBytes(1, it.length().toInt()) }
                    .mapNotNull(ClassParser::tryParseClass)
        }
        check(classNodes.size == affectedFiles.size) { "Not all affected class files could be parsed as class nodes" }

        // run sanitizer
        val issues: List<InspectionIssue<ClassNode>> = classNodes.map(Sanitizer::sanitizeCritical).flatten()
        logger.debug("${issues.size} issues were found")

        // acquire the permission for critical database access
        val scope = CoroutineScope(AcromantulaTask.localDispatcher)
        withContext(scope.coroutineContext) { SynchronizationHelper.acquireCriticalDatabaseAccess() }

        if (issues.isNotEmpty()) {
            val issuesResolvedFuture = CompletableFuture<Boolean>()
            @Suppress("UNCHECKED_CAST")
            val issuesDialog = ResolveIssuesDialogController(issues as List<InspectionIssue<Any>>)
            Platform.runLater {
                showStageCallbackDialog(
                        title = "Critical Issues",
                        content = issuesDialog,
                        resolveOptions = *arrayOf(
                                "Continue" to continueDialog@{ _ ->
                                    if (issuesDialog.hasMoreIssues()) {
                                        Platform.runLater {
                                            // TODO remain in dialog instead of aborting
                                            showErrorDialog("Critical Issues Remaining",
                                                    "Not all issues were resolved. Aborting.")
                                        }
                                        issuesResolvedFuture.complete(false)
                                    } else {
                                        issuesResolvedFuture.complete(true)
                                    }
                                },
                                "Cancel" to { _ ->
                                    logger.debug("resolve issues dialog cancelled")
                                    issuesResolvedFuture.complete(false)
                                })
                )
            }

            if (!withContext(scope.coroutineContext) { issuesResolvedFuture.get() }) { // if aborting
                // release the critical access permission
                SynchronizationHelper.releaseCriticalDatabaseAccess()
                return
            }
        }

        transaction {
            affectedFiles.zip(classNodes).forEach { (file, node) ->
                val writer = refactorCodeFile(file, node, refactorData)
                file.data = connection.createBlob().apply { setBytes(1, writer.toByteArray()) }
                SynchronizationHelper.reportFileChange(file)
            }

            refactorEntities.forEach { refactorEntity(it, refactorData) }
        }

        // release the critical access permission
        SynchronizationHelper.releaseCriticalDatabaseAccess()
    }

    /**
     * Obtain the actual entity this operation is performed upon from database. The entity is only referenced using a
     * string representation from user interface.
     *
     * @param entityIdentification a unique string representation of the entity given by the user interface
     *
     * @return the entity from database that this operation is performed upon
     */
    protected abstract fun obtainEntityFromDatabase(entityIdentification: String): T

    /**
     * Obtain all declarations and references to the entity that is refactored. Each member and each reference
     * affected by the refactor operation must be in the list exactly one time.
     *
     * @param entity the entity that shall be refactored
     *
     * @return a list of [RefactorableEntity] elements
     */
    protected abstract fun collectRefactorableEntities(entity: T): List<RefactorableEntity>

    /**
     * Generate the necessary data to perform the refactoring. This data is usually generated by asking the user for
     * the data.
     *
     * @param entity the database entity that is refactored
     * @param affectedEntities the list of code entities affected by refactoring
     *
     * @return an instance of [D] that holds any information later necessary to perform the refactoring or null, if
     * the operation shall be cancelled.
     */
    protected abstract suspend fun generateRefactorTarget(entity: T, affectedEntities: List<RefactorableEntity>): D?

    /**
     * Refactor the code in a code-file and the file if necessary.
     *
     * @param classFile the file this class resides in
     * @param classNode the asm class node to be refactored
     * @param refactorData the data required for the refactoring action
     *
     * @return a [ClassWriter] containing the refactored class data ready to be rewritten into database
     */
    protected abstract fun refactorCodeFile(classFile: FileBlob, classNode: ClassNode, refactorData: D): ClassWriter

    /**
     * Refactor a database entity using the provided data.
     *
     * @param entity the database entity to be refactored
     * @param refactorData the data of this refactoring action
     */
    protected abstract fun refactorEntity(entity: RefactorableEntity, refactorData: D)
}
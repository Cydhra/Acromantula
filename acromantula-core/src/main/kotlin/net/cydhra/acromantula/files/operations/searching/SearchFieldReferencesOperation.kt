package net.cydhra.acromantula.files.operations.searching

import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.utility.model.CodeReference
import net.cydhra.acromantula.files.utility.model.FieldDeclarationJumpTarget
import net.cydhra.acromantula.files.utility.model.InstructionJumpTarget
import org.jetbrains.exposed.sql.transactions.transaction

object SearchFieldReferencesOperation : SearchReferencesOperation<JavaField>() {
    override val name: String = "Search Field References"

    override fun obtainEntityFromDatabase(entityIdentification: String): JavaField {
        return transaction {
            val identifierId = JavaIdentifier.find { JavaIdentifiers.identifier eq entityIdentification }.single().id
            JavaField.find { JavaFields.identifier eq identifierId }.first()
        }
    }

    override fun collectReferences(entity: JavaField): List<CodeReference> {
        val referenceList = mutableListOf<CodeReference>()
        referenceList += transaction {
            CodeReference(entity.owner.file,
                    FieldDeclarationJumpTarget(entity.identifier.identifier))
        }

        // map the reference list into an entity model usable by GUI and callbacks
        referenceList += transaction {
            JavaReference
                    .find { JavaReferences.referenced eq entity.identifier.id }
                    .map { ref ->
                        CodeReference(ref.referenceSource.owner.file,
                                InstructionJumpTarget(ref.referenceSource.identifier.identifier, ref.instruction))

                    }
        }

        return referenceList
    }

}
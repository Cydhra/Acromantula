package net.cydhra.acromantula.files.utility.analysis.cfg

import org.objectweb.asm.Label
import org.objectweb.asm.tree.*
import org.objectweb.asm.tree.AbstractInsnNode.*

/**
 * A control flow graph of a method node. This utility can parse a given method node into the graph model. The
 * control flow graph ignores line numbers. This means, if code is generated using the control flow graph, line
 * numbers will vanish. This is considered to be non-harmful behaviour, as modifying bytecode will violate the line
 * numbers of the original source code anyway.
 *
 * @param methodNode the node this control flow graph shall be constructed from
 */
class ControlFlowGraph(methodNode: MethodNode) {

    /**
     * a sorted list of control flow nodes. The list is sorted by the instruction order of the method
     */
    private val controlFlowNodes: MutableList<ControlFlowNode> = mutableListOf()

    /**
     * A mapping of labels in the method to a unique index of their declaration
     */
    private val labelMappingMap: MutableMap<Label, Int> = mutableMapOf()

    /**
     * An immutable view of the internal mapping for labels in the graph.
     */
    val labelMapping: Map<Label, Int> = labelMappingMap

    init {
        // construct cfg nodes
        var currentInstructionList = mutableListOf<AbstractInsnNode>()

        for (instruction in methodNode.instructions) {
            when (instruction.type) {
                JUMP_INSN, LOOKUPSWITCH_INSN, TABLESWITCH_INSN -> {
                    currentInstructionList.add(instruction)
                    addControlFlowNode(currentInstructionList)
                    currentInstructionList = mutableListOf()
                }
                LABEL -> {
                    if (currentInstructionList.isNotEmpty()) {
                        addControlFlowNode(currentInstructionList)
                        currentInstructionList = mutableListOf()
                    }
                    currentInstructionList.add(instruction)
                }
                LINE -> {
                }
                else -> currentInstructionList.add(instruction)
            }
        }

        if (currentInstructionList.isNotEmpty())
            addControlFlowNode(currentInstructionList)

        // count labels
        var labelCounter = 0
        controlFlowNodes.forEach {
            if (it.instructions[0] is LabelNode) {
                it.labelNumber = labelCounter++
                labelMappingMap[(it.instructions[0] as LabelNode).label] = it.labelNumber
            }
        }

        // construct edges
        controlFlowNodes.forEach(ControlFlowNode::constructEdges)
    }

    /**
     * Add a new node using the given list of instructions as content to the list of graph nodes
     *
     * @param currentInstructionList the list of instructions that are in the node
     */
    private fun addControlFlowNode(currentInstructionList: List<AbstractInsnNode>) {
        val nextNode = ControlFlowNode(currentInstructionList)

        if (controlFlowNodes.isNotEmpty())
            controlFlowNodes[controlFlowNodes.size - 1].next = nextNode

        controlFlowNodes.add(nextNode)
    }

    /**
     * A node that contains some instructions until either a jump or switch instruction branches control flow, or a
     * label node marks a jump target.
     *
     * @param instructions the list of instructions in this block
     */
    inner class ControlFlowNode(val instructions: List<AbstractInsnNode>) {

        /**
         * The list of edges leaving this block.
         */
        private val outgoingEdgesList: MutableList<ControlFlowEdge> = mutableListOf()

        /**
         * The naturally next control flow node. This is only reachable, if the jumps finishing this block of code
         * are non-exhaustive. It is the node that is physically behind this node in the code file. Null, if this is
         * the last node of the method
         */
        var next: ControlFlowNode? = null

        /**
         * the number the label starting this node has, if any (otherwise -1)
         */
        var labelNumber = -1

        /**
         * Construct the outgoing edges of this node.
         */
        fun constructEdges() {
            when (val node = this.instructions[this.instructions.size - 1]) {
                // add jump target and next edge
                is JumpInsnNode -> {
                    outgoingEdgesList += ControlFlowEdge.BooleanTrueEdge(
                            controlFlowNodes.first { it.labelNumber == labelMappingMap[node.label.label] })

                    if (next != null)
                        outgoingEdgesList += ControlFlowEdge.BooleanFalseEdge(next!!)
                }

                // add switch labels and default or next node
                is TableSwitchInsnNode -> {
                    (node.min..node.max).forEach { literal ->
                        outgoingEdgesList += ControlFlowEdge.SwitchEdge(literal.toString(),
                                controlFlowNodes.first {
                                    it.labelNumber == labelMappingMap[node.labels[literal - node.min].label]
                                })
                    }
                    if (node.dflt != null)
                        outgoingEdgesList += ControlFlowEdge.DefaultSwitchEdge(
                                controlFlowNodes.first { it.labelNumber == labelMappingMap[node.dflt.label] }
                        )
                    else if (next != null)
                        outgoingEdgesList += ControlFlowEdge.DefaultSwitchEdge(next!!)
                }

                // add switch labels and default or next node
                is LookupSwitchInsnNode -> {
                    node.keys.forEachIndexed { index, literal ->
                        outgoingEdgesList += ControlFlowEdge.SwitchEdge(literal.toString(),
                                controlFlowNodes.first { it.labelNumber == labelMappingMap[node.labels[index].label] })
                    }

                    if (node.dflt != null)
                        outgoingEdgesList += ControlFlowEdge.DefaultSwitchEdge(
                                controlFlowNodes.first { it.labelNumber == labelMappingMap[node.dflt.label] }
                        )
                    else if (next != null)
                        outgoingEdgesList += ControlFlowEdge.DefaultSwitchEdge(next!!)
                }

                // add next node
                else -> {
                    if (next != null)
                        outgoingEdgesList += ControlFlowEdge.UnconditionalEdge(next!!)
                }
            }
        }

    }

    /**
     * Control flow edges originate from a [ControlFlowNode] that saves them as outgoing nodes. They have a label and
     * a target node. The subclasses of the sealed superclass allow further specialization.
     *
     * @param conditionLabel a human readable label for the edge
     * @param target the target node for the edge
     */
    sealed class ControlFlowEdge(val conditionLabel: String, val target: ControlFlowNode) {
        /**
         * A control flow edge that marks a jump path that is taken
         *
         * @param target the target node for the edge
         */
        class BooleanTrueEdge(target: ControlFlowNode) : ControlFlowEdge("true", target)

        /**
         * A control flow edge that marks a jump path that is not taken
         *
         * @param target the target node for the edge
         */
        class BooleanFalseEdge(target: ControlFlowNode) : ControlFlowEdge("false", target)

        /**
         * A control flow edge that marks a jump by a switch statement or natural flow if no jump is taken and no
         * default path exists. Default path or natural flow is specialized through [DefaultSwitchEdge]
         *
         * @param literal the literal of the switch block that is responsible for this jump
         * @param target the target node for the edge
         */
        open class SwitchEdge(literal: String, target: ControlFlowNode) : ControlFlowEdge(literal, target)

        /**
         * A control flow edge that marks a default jump of a switch statement or natural flow, if no default path
         * exists.
         *
         * @param target the target node for the edge
         */
        class DefaultSwitchEdge(target: ControlFlowNode) : SwitchEdge("default", target)

        /**
         * Natural control flow edge that is not influenced by jumps or switch statements.
         *
         * @param target the target node for the edge
         */
        class UnconditionalEdge(target: ControlFlowNode) : ControlFlowEdge("", target)
    }
}


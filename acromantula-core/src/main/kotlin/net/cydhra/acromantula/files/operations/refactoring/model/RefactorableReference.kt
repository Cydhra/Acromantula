package net.cydhra.acromantula.files.operations.refactoring.model

import net.cydhra.acromantula.files.model.database.java.JavaMethod
import net.cydhra.acromantula.files.utility.model.CodeReference

class RefactorableReference(codeReference: CodeReference, val referenceSource: JavaMethod) :
        RefactorableEntity(codeReference)
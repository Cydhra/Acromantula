package net.cydhra.acromantula.files.inspections

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.workspace.importer.tools.ClassParser
import net.cydhra.acromantula.workspace.sanitizing.InspectionRunner
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.tree.ClassNode

class ClassFileInspectionRunner(fileBlob: FileBlob) : InspectionRunner<ClassNode>(fileBlob) {
    override fun generateEntity(): ClassNode {
        return transaction {
            val reloaded = FileBlob.reload(fileBlob)!!
            ClassParser.tryParseClass(reloaded.data.getBytes(1, reloaded.data.length().toInt()))!!
        }
    }

    override fun saveEntity(entity: ClassNode) {
        transaction {
            val writer = ClassWriter(0)
            entity.accept(writer)

            fileBlob.data = connection.createBlob().apply { setBytes(1, writer.toByteArray()) }
        }
    }
}
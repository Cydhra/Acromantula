package net.cydhra.acromantula.files.types.action.disassembly

import javafx.application.Platform
import javafx.geometry.Point2D
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.OpenEditorTabRequest
import net.cydhra.acromantula.concurrency.ThreadPool
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.files.types.ContentViewOperation
import net.cydhra.acromantula.files.utility.model.JumpTarget
import net.cydhra.acromantula.gui.controller.EditorTabView
import net.cydhra.acromantula.gui.controller.components.CodeEditorView
import net.cydhra.acromantula.gui.controller.utility.EditorTextStyle
import net.cydhra.acromantula.gui.controller.utility.showErrorDialog
import org.apache.logging.log4j.LogManager
import org.fxmisc.richtext.model.StyleSpansBuilder

/**
 * Context menu action for class file to open their disassembly view
 */
object DisassembleClassAction : ContentViewOperation<FileEntry>() {
    override val name: String = "Disassemble"

    private val logger = LogManager.getLogger()

    override suspend fun onViewAction(file: FileBlob, entity: FileEntry, jumpTarget: JumpTarget?) {
        // define a function that can update the content of the editor that is now created
        val contentUpdateFunction = { editorTab: EditorTabView, file: FileBlob, jumpTarget: JumpTarget? ->
            ThreadPool.submit(DisassembleTask(file)) {
                onSuccess { metaText ->
                    Platform.runLater {
                        (editorTab as CodeEditorView).codeArea.replaceText(metaText.text)
                        editorTab.codeArea.setStyleSpans(0, StyleSpansBuilder<EditorTextStyle>().addAll(metaText
                                .styleSpans).create())
                        editorTab.jumpTargets = metaText.jumpTargets
                        if (jumpTarget == null)
                            editorTab.codeArea.scrollToPixel(Point2D(0.0, 0.0))
                        else
                            editorTab.attemptJump(jumpTarget)
                    }
                }

                onFailure { t ->
                    logger.error("Unexpected exception while disassembling", t)
                    Platform.runLater {
                        showErrorDialog("Error while disassembling", "Unexpected exception while disassembling: ${t.message}")
                    }
                }
            }
        }


        val editorTab = CodeEditorView(file, contentUpdateFunction)

        val request = OpenEditorTabRequest(entity.displayName, "disassembly", editorTab)
        request.onFulfill {
            contentUpdateFunction(editorTab, file, jumpTarget)
        }
        EventBroker.fireEvent(request)
    }

}
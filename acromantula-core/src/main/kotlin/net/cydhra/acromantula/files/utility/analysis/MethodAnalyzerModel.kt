package net.cydhra.acromantula.files.utility.analysis

import net.cydhra.acromantula.files.utility.analysis.cfg.ControlFlowGraph
import org.objectweb.asm.tree.MethodNode

class MethodAnalyzerModel(methodNode: MethodNode) {

    /**
     * The method's control flow graph
     */
    val controlFlowGraph = ControlFlowGraph(methodNode)
}
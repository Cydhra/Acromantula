package net.cydhra.acromantula.files.types

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.utility.model.JumpTarget

/**
 * A [ContentOperation] that is specifically to generate and offer a view on the content data. It takes an additional
 * [JumpTarget] as parameter, as the view might jump to a specific location in its data.
 */
abstract class ContentViewOperation<E> : ContentOperation<E>() {
    override suspend fun onAction(file: FileBlob, entity: E) {
        this.onViewAction(file, entity, null)
    }

    abstract suspend fun onViewAction(file: FileBlob, entity: E, jumpTarget: JumpTarget?)
}
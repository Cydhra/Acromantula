@file:Suppress("DuplicatedCode")

package net.cydhra.acromantula.files.types.action.disassembly

import com.strobel.assembler.ir.OpCode
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.types.action.disassembly.context.ClassDefinitionContextFactory
import net.cydhra.acromantula.files.types.action.disassembly.context.FieldDefinitionContextFactory
import net.cydhra.acromantula.files.types.action.disassembly.context.MethodDefinitionContextFactory
import net.cydhra.acromantula.files.utility.analysis.MethodAnalyzerModel
import net.cydhra.acromantula.files.utility.getIdentity
import net.cydhra.acromantula.files.utility.model.*
import net.cydhra.acromantula.gui.controller.utility.*
import org.apache.commons.lang3.StringEscapeUtils
import org.objectweb.asm.Label
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type
import org.objectweb.asm.tree.*

/**
 * Builder for [MetaText] instances from a given [classNode]
 */
class DisassemblyMetatextGenerator(private val file: FileBlob, private val classNode: ClassNode) {

    /**
     * The analyzer model of the current method
     */
    private lateinit var methodModel: MethodAnalyzerModel

    private var instructionCounter = 0

    private lateinit var currentMethodNode: MethodNode

    /**
     * Create the meta text from the class node
     * @return the [MetaText] instance containing the assembly text with meta data
     */
    fun create(): MetaText {
        return metaText {
            // add a jump target for the file in the beginning.
            write("", paragraphJumpTarget = FileJumpTarget(file.id, file.filename))

            if (classNode.signature != null)
                dumpSignature(classNode.signature)


            classNode.invisibleAnnotations?.forEach {
                dumpAnnotation(it)
                writeLine("")
            }
            classNode.visibleAnnotations?.forEach {
                dumpAnnotation(it)
                writeLine("")
            }

            accessFlags(classNode.access, true)

            block(classNode.name, contextFactory = ClassDefinitionContextFactory(file, classNode.getIdentity()),
                    paragraphJumpTarget = ClassDeclarationJumpTarget(classNode.getIdentity()),
                    styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR)) {
                if (classNode.superName != null) {
                    write("extends ", styleClasses = *arrayOf(TEXT_CODE))
                    writeLine(classNode.superName, styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR))
                }

                if (classNode.interfaces.isNotEmpty()) {
                    write("implements ", styleClasses = *arrayOf(TEXT_CODE))
                    write(classNode.interfaces[0], styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR))

                    classNode.interfaces.subList(1, classNode.interfaces.size).forEach {
                        write(", ", styleClasses = *arrayOf(TEXT_CODE))
                        write(it, styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR))
                    }
                    writeLine("", styleClasses = *arrayOf(TEXT_CODE))
                }
                writeLine("", styleClasses = *arrayOf(TEXT_CODE))

                classNode.fields.forEach { dumpField(it) }
                writeLine("", styleClasses = *arrayOf(TEXT_CODE))
                classNode.methods.forEach { dumpMethod(it) }
            }
        }
    }

    /**
     * Convenience extension method to write access flags into the receiving [MetaTextWriter]
     *
     * @param flags packed access flags
     * @param isClass whether the access flags belong to a class-like node
     */
    private fun MetaTextWriter.accessFlags(flags: Int, isClass: Boolean = false) {
        when {
            flags and Opcodes.ACC_PUBLIC != 0 -> write("public ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
            flags and Opcodes.ACC_PRIVATE != 0 -> write("private ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
            flags and Opcodes.ACC_PROTECTED != 0 -> write("protected ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        }

        if (flags and Opcodes.ACC_FINAL != 0)
            write("final ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_STATIC != 0)
            write("static ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_SYNCHRONIZED != 0 && !isClass)
            write("synchronized ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_VOLATILE != 0)
            write("volatile ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_TRANSIENT != 0)
            write("transient ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_ABSTRACT != 0)
            write("abstract ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_STRICT != 0)
            write("strictfp ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_SYNTHETIC != 0)
            write("synthetic ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_MANDATED != 0)
            write("mandated ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        if (flags and Opcodes.ACC_ENUM != 0)
            write("enum ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))

        when {
            flags and Opcodes.ACC_ANNOTATION != 0 -> write("@interface ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
            flags and Opcodes.ACC_INTERFACE != 0 -> write("interface ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
            flags and Opcodes.ACC_ENUM != 0 -> write("enum ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
            isClass -> write("class ", styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
        }
    }

    private fun MetaTextWriter.dumpField(node: FieldNode) {
        if (node.signature != null)
            dumpSignature(node.signature)

        node.invisibleAnnotations?.forEach {
            dumpAnnotation(it)
            writeLine("")
        }
        node.visibleAnnotations?.forEach {
            dumpAnnotation(it)
            writeLine("")
        }

        accessFlags(node.access)
        write(node.name, contextFactory = FieldDefinitionContextFactory(file, node.getIdentity(classNode)),
                paragraphJumpTarget = FieldDeclarationJumpTarget(node.getIdentity(classNode)),
                styleClasses = *arrayOf(TEXT_CODE))
        write(": ", styleClasses = *arrayOf(TEXT_CODE))
        write(node.desc, styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR))
        writeLine("", styleClasses = *arrayOf(TEXT_CODE))
    }

    /**
     * Convenience extension method to write a method node into the receiving [MetaTextWriter]
     *
     * @param node method node
     */
    private fun MetaTextWriter.dumpMethod(node: MethodNode) {
        if (node.signature != null)
            dumpSignature(node.signature)

        node.invisibleAnnotations?.forEach {
            dumpAnnotation(it)
            writeLine("")
        }
        node.visibleAnnotations?.forEach {
            dumpAnnotation(it)
            writeLine("")
        }

        accessFlags(node.access)
        write(node.name, contextFactory = MethodDefinitionContextFactory(file, node.getIdentity(classNode)),
                paragraphJumpTarget = MethodDeclarationJumpTarget(node.getIdentity(classNode)),
                styleClasses = *arrayOf(TEXT_CODE))
        write(node.desc, styleClasses = *arrayOf(TEXT_CODE))

        methodModel = MethodAnalyzerModel(node)

        instructionCounter = 0
        currentMethodNode = node

        block("", styleClasses = *arrayOf(TEXT_CODE)) {
            indent()

            // write all instructions into the MetaTextWriter, but also write preceding labels
            node.instructions.iterator().forEach { instruction ->
                instructionCounter++
                when (instruction) {
                    is LabelNode -> dumpLabel(instruction)
                    is FrameNode -> dumpFrame(instruction)
                    is LineNumberNode -> {
                    }
                    is InsnNode -> dumpInsn(instruction)
                    is IntInsnNode -> dumpIntInsn(instruction)
                    is VarInsnNode -> dumpVarInsn(instruction, node)
                    is TypeInsnNode -> dumpTypeInsn(instruction)
                    is FieldInsnNode -> dumpFieldInsn(instruction)
                    is MethodInsnNode -> dumpMethodInsn(instruction)
                    is InvokeDynamicInsnNode -> dumpInvokeDynamicInsn(instruction)
                    is JumpInsnNode -> dumpJumpInsn(instruction)
                    is LdcInsnNode -> dumpLdcInsn(instruction)
                    is IincInsnNode -> dumpIincInsn(instruction, node)
                    is TableSwitchInsnNode -> dumpTableSwitchInsn(instruction)
                    is LookupSwitchInsnNode -> dumpLookupSwitchInsn(instruction)
                    is MultiANewArrayInsnNode -> dumpMultiNewArrayInsn(instruction)
                    is AbstractInsnNode -> throw AssertionError()
                }
            }

            unindent()
        }

        writeLine("", styleClasses = *arrayOf(TEXT_CODE))
    }

    /**
     * Dump a member signature. The signature will be enclosed in brackets
     *
     * @param signature the signature string.
     */
    private fun MetaTextWriter.dumpSignature(signature: String) {
        writeLine("[$signature]", styleClasses = *arrayOf(TEXT_CODE, CODE_COMMENT))
    }

    private fun MetaTextWriter.dumpAnnotation(annotation: AnnotationNode) {
        write("@${annotation.desc}", styleClasses = *arrayOf(CODE_PRAGMA))
        write("(")
        val iter = annotation.values?.iterator()
        if (iter != null) {
            while (iter.hasNext()) {
                write(iter.next().toString() + ": ")
                dumpLiteral(iter.next())

                if (iter.hasNext()) {
                    write(", ")
                }
            }
        }
        write(") ")
    }

    /**
     * @param node [LabelNode] to dump
     */
    private fun MetaTextWriter.dumpLabel(node: LabelNode) {
        unindent()
        val number = obtainLabelNumber(node.label)

        write("Label $number", styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
        if (node.next is LineNumberNode) {
            writeLine(" (Line ${(node.next as LineNumberNode).line}):", styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
        } else {
            writeLine(":", styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
        }
        indent()
    }

    private fun MetaTextWriter.dumpFrame(node: FrameNode) {
        writeLine("// Frame: ${
        when (node.type) {
            Opcodes.F_NEW -> "NEW"
            Opcodes.F_FULL -> "FULL"
            Opcodes.F_APPEND -> "APPEND"
            Opcodes.F_CHOP -> "CHOP"
            Opcodes.F_SAME -> "SAME"
            Opcodes.F_SAME1 -> "SAME_1"
            else -> "error"
        }
        } local=[${node.local.joinToString(", ")}] stack=[${node.stack.joinToString(", ")}]",
                styleClasses = *arrayOf(TEXT_CODE, CODE_COMMENT))
    }

    private fun MetaTextWriter.writeOpcode(opCode: Int) {
        write(OpCode.get(opCode).name,
                paragraphJumpTarget = InstructionJumpTarget(currentMethodNode.getIdentity(classNode), instructionCounter),
                styleClasses = *arrayOf(TEXT_CODE, CODE_INSTRUCTION))
    }

    /**
     * Dump an operand-less instruction
     *
     * @param node instruction node
     */
    private fun MetaTextWriter.dumpInsn(node: InsnNode) {
        writeOpcode(node.opcode)
        writeLine("", styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun MetaTextWriter.dumpIntInsn(node: IntInsnNode) {
        writeOpcode(node.opcode)
        write(" ", styleClasses = *arrayOf(TEXT_CODE))
        dumpLiteral(node.operand)
        writeLine("", styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun MetaTextWriter.dumpVarInsn(node: VarInsnNode, method: MethodNode) {
        writeOpcode(node.opcode)
        writeLine(" " + obtainLocalVariable(node.`var`, method), styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun MetaTextWriter.dumpTypeInsn(node: TypeInsnNode) {
        writeOpcode(node.opcode)
        writeLine(" " + node.desc, styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR))
    }

    private fun MetaTextWriter.dumpFieldInsn(node: FieldInsnNode) {
        writeOpcode(node.opcode)
        write(" ${node.owner}::${node.name}")
        writeLine(" // (${node.desc})", styleClasses = *arrayOf(TEXT_CODE, CODE_COMMENT))
    }

    private fun MetaTextWriter.dumpMethodInsn(node: MethodInsnNode) {
        writeOpcode(node.opcode)
        writeLine(" ${node.owner}::${node.name}${node.desc}", styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun MetaTextWriter.dumpInvokeDynamicInsn(node: InvokeDynamicInsnNode) {
        writeOpcode(node.opcode)
        writeLine(" ${node.bsm.owner}::${node.name}${node.desc}", styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun MetaTextWriter.dumpJumpInsn(node: JumpInsnNode) {
        val number = obtainLabelNumber(node.label.label)

        writeOpcode(node.opcode)
        writeLine(" Label $number", styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
    }

    private fun MetaTextWriter.dumpLdcInsn(node: LdcInsnNode) {
        writeOpcode(node.opcode)
        write(" ", styleClasses = *arrayOf(TEXT_CODE))
        dumpLiteral(node.cst)
        writeLine("", styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun MetaTextWriter.dumpIincInsn(node: IincInsnNode, method: MethodNode) {
        writeOpcode(node.opcode)
        write(" ${obtainLocalVariable(node.`var`, method)} + ", styleClasses = *arrayOf(TEXT_CODE))
        writeLine(node.incr.toString(), styleClasses = *arrayOf(TEXT_CODE, CODE_LITERAL))
    }

    private fun MetaTextWriter.dumpTableSwitchInsn(instruction: TableSwitchInsnNode) {
        writeOpcode(instruction.opcode)
        writeLine("", styleClasses = *arrayOf(TEXT_CODE))
        indent()
        (instruction.min..instruction.max)
                .forEach { id ->
                    dumpLiteral(id)
                    write(" -> ", styleClasses = *arrayOf(TEXT_CODE))
                    writeLine("Label " + obtainLabelNumber(instruction.labels[id - instruction.min].label),
                            styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
                }
        write("default -> ", styleClasses = *arrayOf(TEXT_CODE))
        writeLine("Label " + obtainLabelNumber(instruction.dflt.label), styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
        unindent()
    }

    private fun MetaTextWriter.dumpLookupSwitchInsn(instruction: LookupSwitchInsnNode) {
        writeOpcode(instruction.opcode)
        writeLine("", styleClasses = *arrayOf(TEXT_CODE))
        indent()
        instruction.keys.forEachIndexed { index, key ->
            dumpLiteral(key)
            write(" -> ", styleClasses = *arrayOf(TEXT_CODE))
            writeLine("Label " + obtainLabelNumber(instruction.labels[index].label),
                    styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
        }
        write("default ->", styleClasses = *arrayOf(TEXT_CODE))
        writeLine("Label " + obtainLabelNumber(instruction.dflt.label), styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
        unindent()
    }

    private fun MetaTextWriter.dumpMultiNewArrayInsn(instruction: MultiANewArrayInsnNode) {
        writeOpcode(instruction.opcode)
        write(" ", styleClasses = *arrayOf(TEXT_CODE))
        dumpLiteral(instruction.dims)
        write(" ", styleClasses = *arrayOf(TEXT_CODE))
        writeLine(instruction.desc, styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR))
    }

    private fun MetaTextWriter.dumpLiteral(literal: Any) {
        when (literal) {
            is String -> write("\"${StringEscapeUtils.escapeJava(literal)}\"",
                    styleClasses = *arrayOf(TEXT_CODE, CODE_STRING))
            is Type -> write(literal.className, styleClasses = *arrayOf(TEXT_CODE, CODE_DESCRIPTOR))
            else -> write(StringEscapeUtils.escapeJava(literal.toString()),
                    styleClasses = *arrayOf(TEXT_CODE, CODE_LITERAL))
        }
    }

    /**
     * Get the number associated with the given label for the current method
     *
     * @param label the label
     *
     * @return the unique number for this label
     */
    private fun obtainLabelNumber(label: Label): Int {
        return methodModel.controlFlowGraph.labelMapping[label]!!
    }

    /**
     * Obtain an identifier for a local variable of a method mentioned in an instruction
     *
     * @param varIndex the index given in the instruction operand
     * @param methodNode the method node owning the instruction
     */
    private fun obtainLocalVariable(varIndex: Int, methodNode: MethodNode): String {
        return (methodNode.localVariables.getOrNull(varIndex)?.name?.takeIf { it.isNotEmpty() }
                ?: "locals[$varIndex]")
    }
}

package net.cydhra.acromantula.files.utility.analysis

import org.objectweb.asm.tree.ClassNode

/**
 * A utiltity model that can be generated from a class node to run analysis on it and generate state for transformations
 */
class ClassAnalyzerModel(classNode: ClassNode) {

    /**
     * A list of [MethodAnalyzerModels][MethodAnalyzerModel] for each method of the modelled class.
     */
    val methodModels: List<MethodAnalyzerModel> = classNode.methods.map(::MethodAnalyzerModel)
}
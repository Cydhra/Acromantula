package net.cydhra.acromantula.files.model.database

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * Table of imported archives
 */
object Archives : IntIdTable() {
    val name = varchar("name", 255)
}

/**
 * Database entity for imported archives. It is used to persist the structure of archives imported into the database.
 */
class Archive(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Archive>(Archives)

    var name by Archives.name

    val rootDirectories: List<Directory>
        get() = transaction { Directory.find { Directories.archive eq this@Archive.id and Directories.parent.isNull() } }.toList()

    val rootFiles: List<FileBlob>
        get() = transaction { FileBlob.find { FileBlobs.archive eq this@Archive.id and FileBlobs.parent.isNull() } }.toList()
}
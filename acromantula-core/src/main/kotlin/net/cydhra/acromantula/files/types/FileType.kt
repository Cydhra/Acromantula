package net.cydhra.acromantula.files.types

import net.cydhra.acromantula.files.model.database.FileBlob

/**
 * Represents a type of file that can be imported into the workspace
 */
abstract class FileType(val typeDescriptor: String) {

    /**
     * Whether this type can be auto assigned by calling [isFileConforming]. If false, this type can not be assigned
     * during [FileBlob] creation but only explicitly.
     */
    open val autoAssignable: Boolean = false


    open val fileTypeImage: String = "/images/file_generic.svg"

    /**
     * @return true, if the given file is of this file type
     */
    abstract fun isFileConforming(file: FileBlob): Boolean

    /**
     * A convenient way to specialize a file type further by chaining a subtype to it.
     *
     * @param subTypeDescriptor a string that names the specialized type
     *
     * @return a file type descriptor for the specialized type
     */
    operator fun div(subTypeDescriptor: String): String {
        return "$typeDescriptor/$subTypeDescriptor"
    }

    /**
     * @param otherFileType any other file type.
     *
     * @return true, if this file type is a specialized form of [otherFileType]. Also returns true, if
     * [otherFileType] is this file type.
     */
    fun isDerivedFrom(otherFileType: FileType): Boolean {
        return this.typeDescriptor.startsWith(otherFileType.typeDescriptor)
    }
}
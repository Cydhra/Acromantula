package net.cydhra.acromantula.files.operations.refactoring.rename

import javafx.application.Platform
import javafx.collections.FXCollections
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.operations.refactoring.RefactorOperation
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableEntity
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableMember
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableReference
import net.cydhra.acromantula.files.operations.refactoring.remapper.MethodNameRemapper
import net.cydhra.acromantula.files.utility.constructMethodIdentity
import net.cydhra.acromantula.files.utility.model.CodeReference
import net.cydhra.acromantula.files.utility.model.InstructionJumpTarget
import net.cydhra.acromantula.files.utility.model.MethodDeclarationJumpTarget
import net.cydhra.acromantula.gui.controller.dialog.RenameElementDialogView
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog
import net.cydhra.acromantula.workspace.AcromantulaTask
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.tree.ClassNode
import java.util.*
import java.util.concurrent.CompletableFuture

/**
 * A refactoring operation to rename methods identified by their internal identifier string.
 */
object MethodNameRefactorOperation : RefactorOperation<JavaMethod, MethodNameData>() {
    override val name: String = "Rename Method"

    override fun obtainEntityFromDatabase(entityIdentification: String): JavaMethod {
        return transaction {
            val identifierId = JavaIdentifier
                    .find { JavaIdentifiers.identifier eq entityIdentification }.single().id
            JavaMethod.find { JavaMethods.identifier eq identifierId }.first()
        }
    }

    override fun collectRefactorableEntities(entity: JavaMethod): List<RefactorableEntity> {
        val inheritanceTree: Queue<Int> = ArrayDeque()
        inheritanceTree.add(entity.id.value)

        val workedSet: MutableSet<Int> = HashSet()
        var currentMethod: JavaMethod? = entity
        val referenceList = mutableListOf<RefactorableEntity>()

        transaction {
            referenceList += RefactorableMember(CodeReference(entity.owner.file,
                    MethodDeclarationJumpTarget(entity.identifier.identifier)), entity)

            while (currentMethod != null) {
                workedSet += currentMethod!!.id.value

                referenceList += transaction {
                    JavaReference
                            .find { JavaReferences.referenced eq currentMethod!!.identifier.id }
                            .map {
                                RefactorableReference(CodeReference(it.referenceSource.owner.file,
                                        InstructionJumpTarget(it.referenceSource.identifier.identifier, it.instruction)),
                                        it.referenceSource)
                            }
                }

                val overrides = transaction {
                    JavaOverride
                            .find { JavaOverrides.method eq currentMethod!!.id }
                            .filter { !workedSet.contains(it.overrides.id.value) }
                            .map {
                                RefactorableMember(CodeReference(it.overrides.owner.file,
                                        MethodDeclarationJumpTarget(it.overrides.identifier.identifier)), it.overrides)
                            }
                }

                overrides.forEach { reference ->
                    if (!inheritanceTree.contains(reference.member.id.value)) {
                        inheritanceTree.add(reference.member.id.value)
                        referenceList += reference
                    }
                }

                val overriddenBy = transaction {
                    JavaOverride
                            .find { JavaOverrides.overrides eq currentMethod!!.id }
                            .filter { !workedSet.contains(it.method.id.value) }
                            .map {
                                RefactorableMember(CodeReference(it.method.owner.file,
                                        MethodDeclarationJumpTarget(it.method.identifier.identifier)), it.method)
                            }
                }

                overriddenBy.forEach { reference ->
                    if (!inheritanceTree.contains(reference.member.id.value)) {
                        inheritanceTree.add(reference.member.id.value)
                        referenceList += reference
                    }
                }

                currentMethod = transaction { JavaMethod.find { JavaMethods.id eq inheritanceTree.poll() }.singleOrNull() }
            }
        }

        return referenceList
    }

    override suspend fun generateRefactorTarget(entity: JavaMethod, affectedEntities: List<RefactorableEntity>):
            MethodNameData? {
        val dialog = RenameElementDialogView(FXCollections.observableArrayList(affectedEntities), entity.name)
        val dataFuture = CompletableFuture<MethodNameData?>()

        Platform.runLater {
            showStageCallbackDialog(
                    title = "Rename " + entity.name,
                    content = dialog,
                    resolveOptions = *arrayOf(
                            "Rename" to rename@{ _ ->
                                transaction {
                                    dataFuture.complete(MethodNameData(
                                            affectedEntities
                                                    .mapNotNull {
                                                        if (it is RefactorableMember<*>)
                                                            (it.member as JavaMethod).owner.name
                                                        else null
                                                    }.toHashSet(),
                                            entity.name,
                                            entity.descriptor, dialog.getNewMemberName()))
                                }
                            },
                            "Cancel" to { _ -> dataFuture.complete(null) })
            )
        }

        return withContext(CoroutineScope(AcromantulaTask.localDispatcher).coroutineContext) {
            dataFuture.get()
        }
    }

    override fun refactorCodeFile(classFile: FileBlob, classNode: ClassNode, refactorData: MethodNameData): ClassWriter {
        val writer = ClassWriter(0)

        // remap the class and write the result into the writer
        classNode.accept(ClassRemapper(writer,
                MethodNameRemapper(
                        classTreeSet = refactorData.classTreeSet,
                        methodName = refactorData.oldName,
                        methodDescriptor = refactorData.descr,
                        newName = refactorData.newName)))

        return writer
    }

    override fun refactorEntity(entity: RefactorableEntity, refactorData: MethodNameData) {
        if (entity is RefactorableMember<*>) {
            with(entity.member as JavaMethod) {
                name = refactorData.newName
                identifier.identifier = constructMethodIdentity(owner.identifier.identifier, refactorData.newName,
                        refactorData.descr)
            }

        }
    }

}

/**
 * All data required for renaming a method node.
 *
 * @param classTreeSet all classes containing either the method or an polymorphic version of it
 * @param oldName the original method name
 * @param descr the method descriptor string representation
 * @param newName the new name for the method
 */
data class MethodNameData(val classTreeSet: Set<String>, val oldName: String, val descr: String, val newName: String)
package net.cydhra.acromantula.files.types.action.decompile

import com.strobel.decompiler.Decompiler
import com.strobel.decompiler.DecompilerSettings
import javafx.application.Platform
import javafx.geometry.Point2D
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.OpenEditorTabRequest
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.files.types.ContentViewOperation
import net.cydhra.acromantula.files.utility.model.JumpTarget
import net.cydhra.acromantula.gui.controller.EditorTabView
import net.cydhra.acromantula.gui.controller.components.CodeEditorView
import net.cydhra.acromantula.gui.controller.utility.EditorTextStyle
import net.cydhra.acromantula.gui.controller.utility.MetaTextWriter
import org.fxmisc.richtext.model.StyleSpansBuilder

object DecompileAction : ContentViewOperation<FileEntry>() {

    /**
     * Decompiler settings used for decompilation output generation
     * TODO: make configurable
     */
    private val decompilerSettings = DecompilerSettings.javaDefaults().let {
        // TODO sanitize types before loading
        it.typeLoader = DatabaseTypeLoader()
        it.showSyntheticMembers = true

        return@let it
    }

    override val name: String = "Decompile (Procyon)"

    override suspend fun onViewAction(file: FileBlob, entity: FileEntry, jumpTarget: JumpTarget?) {
        val contentUpdateFunction = { editorTab: EditorTabView, file: FileBlob, jumpTarget: JumpTarget? ->
            val writer = MetaTextWriter()
            Decompiler.decompile(file.filename, ProcyonMetaTextOutput(writer, file), decompilerSettings)

            Platform.runLater {
                (editorTab as CodeEditorView).codeArea.replaceText(writer.metaText.text)
                editorTab.codeArea.setStyleSpans(0, StyleSpansBuilder<EditorTextStyle>().addAll(writer.metaText
                        .styleSpans).create())
                editorTab.jumpTargets = writer.metaText.jumpTargets
                if (jumpTarget == null)
                    editorTab.codeArea.scrollToPixel(Point2D(0.0, 0.0))
                else
                    editorTab.attemptJump(jumpTarget)
            }
        }

        val editorTab = CodeEditorView(file, contentUpdateFunction)

        val request = OpenEditorTabRequest(entity.displayName, "procyon", editorTab)
        request.onFulfill {
            contentUpdateFunction(editorTab, file, jumpTarget)
        }
        EventBroker.fireEvent(request)
    }
}
package net.cydhra.acromantula.files.types

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.workspace.importer.tools.ClassParser

/**
 * Java class file type
 */
object ClassFileType : FileType(GenericCodeFileType / "java") {

    /**
     * [isFileConforming] does check the magic values of the file, therefore auto-assigning is possible
     */
    override val autoAssignable: Boolean = true

    override val fileTypeImage: String = "/images/file_java.svg"

    override fun isFileConforming(file: FileBlob): Boolean {
        return ClassParser.checkMagicBytes(file.data.getBytes(1, file.data.length().toInt()))
    }
}
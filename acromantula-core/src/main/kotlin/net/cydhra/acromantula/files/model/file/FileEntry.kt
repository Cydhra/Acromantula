package net.cydhra.acromantula.files.model.file

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.types.FileType

/**
 * Archive entries that represent a file should implement this interface
 */
interface FileEntry {
    val fileType: FileType
    val fileBlob: FileBlob
    val displayName: String
}
package net.cydhra.acromantula.files.operations.refactoring.remapper

import org.objectweb.asm.commons.Remapper

/**
 * A remapper that traverses a class tree and remappes every mention of a field to a new name. The field is
 * identified by its owner, its name and its type descriptor.
 *
 * @param fieldOwner the name of the class owning the field
 * @param originalFieldName the current field name
 * @param fieldDescriptor the type of the field
 * @param newName the new name that shall be applied
 */
class FieldNameRemapper(private val fieldOwner: String, private val originalFieldName: String,
                        private val fieldDescriptor: String, private val newName: String) : Remapper() {
    override fun mapFieldName(owner: String, name: String, descriptor: String): String {
        return if (owner == fieldOwner && name == originalFieldName && descriptor == fieldDescriptor)
            newName
        else
            super.mapFieldName(owner, name, descriptor)
    }
}
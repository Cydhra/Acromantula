package net.cydhra.acromantula.files.model

/**
 * A model for the view: The MainView's tree view holds a tree of instances of this interface.
 */
interface WorkspaceEntry : HierarchyData<WorkspaceEntry> {

    val name: String

    /**
     * Name to be shown in GUI
     */
    val displayName: String

    /**
     * location in resources where to find this entry's icon
     */
    val imageLocation: String

    /**
     * This entry's parent [WorkspaceEntry]. If this entry is a root entry, then this shall be null.
     */
    val parent: WorkspaceEntry?

    /**
     * Register a child entry in the list of children
     */
    fun addChild(childEntry: WorkspaceEntry)

    /**
     * Register this item at its parent item
     */
    fun registerAtParent()

    fun comparingType(): Int {
        if (this.name.endsWith("/"))
            return 0

        return 1
    }
}

/**
 * The default comparator used to compare [WorkspaceEntry]
 */
val defaultWorkspaceEntryComparator = Comparator
        .comparing(WorkspaceEntry::comparingType)
        .thenComparing { c1, c2 -> c1.name.toLowerCase().compareTo(c2.name.toLowerCase()) }

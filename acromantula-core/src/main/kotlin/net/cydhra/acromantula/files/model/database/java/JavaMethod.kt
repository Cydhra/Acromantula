package net.cydhra.acromantula.files.model.database.java

import net.cydhra.acromantula.MAX_IDENTIFIER_LENGTH
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

/**
 * Database table containing methods within [JavaClasses]
 */
object JavaMethods : IntIdTable() {
    val identifier = reference("identifier", JavaIdentifiers, ReferenceOption.RESTRICT)
    val owner = reference("owner", JavaClasses, ReferenceOption.CASCADE)
    val name = varchar("name", MAX_IDENTIFIER_LENGTH)
    val descriptor = varchar("descriptor", MAX_IDENTIFIER_LENGTH)
}

/**
 *
 */
class JavaMethod(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<JavaMethod>(JavaMethods)

    var identifier by JavaIdentifier referencedOn JavaMethods.identifier
    var owner by JavaClass referencedOn JavaMethods.owner
    var name by JavaMethods.name
    var descriptor: String by JavaMethods.descriptor
}
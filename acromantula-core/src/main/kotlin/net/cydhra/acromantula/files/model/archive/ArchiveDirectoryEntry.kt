package net.cydhra.acromantula.files.model.archive

import net.cydhra.acromantula.files.model.WorkspaceEntry

class ArchiveDirectoryEntry(parent: WorkspaceEntry, name: String)
    : ArchiveEntry(parent, name, "/images/directory.svg")
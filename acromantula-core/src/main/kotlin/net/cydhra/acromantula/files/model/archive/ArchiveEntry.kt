package net.cydhra.acromantula.files.model.archive

import net.cydhra.acromantula.files.model.DefaultWorkspaceEntry
import net.cydhra.acromantula.files.model.WorkspaceEntry

abstract class ArchiveEntry(parent: WorkspaceEntry, name: String, imageLocation: String)
    : WorkspaceEntry by DefaultWorkspaceEntry(parent, name, imageLocation) {

    override fun toString(): String {
        return displayName
    }

    override fun registerAtParent() {
        this.parent?.addChild(this)
    }
}
package net.cydhra.acromantula.files.types.action.disassembly.context

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.operations.refactoring.rename.MethodNameRefactorOperation
import net.cydhra.acromantula.files.operations.searching.SearchMethodReferencesOperation
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory
import net.cydhra.acromantula.gui.controller.context.constructActionArray

/**
 * Creates context menus for method declarations in disassembled text on demand.
 *
 * @param file the disassembled file
 */
class MethodDefinitionContextFactory(file: FileBlob, entityIdentifier: String) :
        ContextMenuFactory<String>(
                file,
                entityIdentifier,
                *constructActionArray {
                    add(MethodNameRefactorOperation)
                    add(SearchMethodReferencesOperation)
                }
        )
package net.cydhra.acromantula.files.model.database.java

import net.cydhra.acromantula.MAX_IDENTIFIER_LENGTH
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

/**
 * Database table containing field members
 */
object JavaFields : IntIdTable() {
    val identifier = reference("identifier", JavaIdentifiers, ReferenceOption.RESTRICT)
    val owner = reference("owner", JavaClasses)
    val name = varchar("name", MAX_IDENTIFIER_LENGTH)
    val descriptor = varchar("descriptor", MAX_IDENTIFIER_LENGTH)
    val type = reference("type", JavaClasses).nullable()
}

/**
 * Database entity for fields in classes
 */
class JavaField(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<JavaField>(JavaFields)

    var identifier by JavaIdentifier referencedOn JavaFields.identifier

    /**
     * Field's owner class entity
     */
    var owner by JavaClass referencedOn JavaFields.owner

    /**
     * simple name of the field
     */
    var name by JavaFields.name

    /**
     * Field type descriptor
     */
    var descriptor by JavaFields.descriptor

    /**
     * The field's type if known
     */
    var type by JavaClass optionalReferencedOn JavaFields.type
}
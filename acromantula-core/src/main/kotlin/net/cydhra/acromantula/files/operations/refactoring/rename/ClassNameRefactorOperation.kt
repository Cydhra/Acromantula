package net.cydhra.acromantula.files.operations.refactoring.rename

import javafx.application.Platform
import javafx.collections.FXCollections
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.operations.refactoring.RefactorOperation
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableEntity
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableMember
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableReference
import net.cydhra.acromantula.files.operations.refactoring.remapper.ClassNameRemapper
import net.cydhra.acromantula.files.utility.constructClassIdentity
import net.cydhra.acromantula.files.utility.constructFieldIdentity
import net.cydhra.acromantula.files.utility.constructMethodIdentity
import net.cydhra.acromantula.files.utility.model.*
import net.cydhra.acromantula.gui.controller.dialog.RenameElementDialogView
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog
import net.cydhra.acromantula.workspace.AcromantulaTask
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.tree.ClassNode
import java.util.concurrent.CompletableFuture

object ClassNameRefactorOperation : RefactorOperation<JavaClass, ClassNameRefactorData>() {
    override val name: String = "Rename Class"

    override fun obtainEntityFromDatabase(entityIdentification: String): JavaClass {
        return transaction {
            val identifierId = JavaIdentifier.find { JavaIdentifiers.identifier eq entityIdentification }.single().id
            JavaClass.find { JavaClasses.identifier eq identifierId }.first()
        }
    }

    override fun collectRefactorableEntities(entity: JavaClass): List<RefactorableEntity> {
        val referenceList = FXCollections.observableArrayList(mutableListOf<RefactorableEntity>())

        transaction {

            // the file itself is also a reference
            referenceList += RefactorableMember(CodeReference(entity.file, FileJumpTarget(entity.file.id,
                    entity.file.filename)), entity.file)
            //  save class declaration                  # identifier, internalname and file have to be renamed
            referenceList += RefactorableMember(CodeReference(entity.file,
                    ClassDeclarationJumpTarget(entity.identifier.identifier)), entity)

            //  for each method of renamed class:
            //      save method as member-declaration   # identifier has to be renamed
            //      find references to method           # have to be in-class-remapped
            JavaMethod
                    .find { JavaMethods.owner eq entity.id }
                    .forEach { method ->
                        referenceList += RefactorableMember(CodeReference(entity.file,
                                MethodDeclarationJumpTarget(method.identifier.identifier)), method)
                        referenceList += JavaReference.find { JavaReferences.referenced eq method.identifier.id }
                                .map {
                                    RefactorableReference(CodeReference(it.referenceSource.owner.file,
                                            InstructionJumpTarget(it.referenceSource.identifier.identifier, it.instruction)),
                                            it.referenceSource)
                                }
                    }

            //  for each field of renamed class:
            //      save as member-declaration          # identifier has to be renamed
            //      find references to field            # have to be in-class-remapped
            JavaField
                    .find { JavaFields.owner eq entity.id }
                    .forEach { field ->
                        referenceList += RefactorableMember(CodeReference(entity.file,
                                FieldDeclarationJumpTarget(entity.identifier.identifier)), field)
                        referenceList += JavaReference.find { JavaReferences.referenced eq field.identifier.id }
                                .map {
                                    RefactorableReference(CodeReference(it.referenceSource.owner.file,
                                            InstructionJumpTarget(it.referenceSource.identifier.identifier,
                                                    it.instruction)), it.referenceSource)
                                }
                    }

            //  for each reference of class
            //      save reference                      # has to be in-class-remapped
            JavaReference
                    .find { JavaReferences.referenced eq entity.identifier.id }
                    .forEach { ref ->
                        referenceList += RefactorableReference(CodeReference(ref.referenceSource.owner.file,
                                InstructionJumpTarget(ref.referenceSource.identifier.identifier, ref.instruction)),
                                ref.referenceSource)
                    }

            // for each field of type of this class
            // TODO
            // for each method with this class in descriptor
            // TODO
        }

        return referenceList
    }

    override suspend fun generateRefactorTarget(entity: JavaClass, affectedEntities: List<RefactorableEntity>): ClassNameRefactorData? {
        val className = entity.name.removeRange(0, entity.name.lastIndexOf('/') + 1)
        val dialog = RenameElementDialogView(FXCollections.observableArrayList(affectedEntities), className)
        val dataFuture = CompletableFuture<ClassNameRefactorData?>()

        Platform.runLater {

            showStageCallbackDialog(
                    title = "Rename $className",
                    content = dialog,
                    resolveOptions = *arrayOf(
                            "Rename" to rename@{ _ ->
                                transaction {
                                    dataFuture.complete(ClassNameRefactorData(entity.name.removeSuffix(className),
                                            className, dialog.getNewMemberName()))
                                }
                            },
                            "Cancel" to { _ -> dataFuture.complete(null) })
            )
        }

        return withContext(CoroutineScope(AcromantulaTask.localDispatcher).coroutineContext) {
            dataFuture.get()
        }
    }

    override fun refactorCodeFile(classFile: FileBlob, classNode: ClassNode, refactorData: ClassNameRefactorData): ClassWriter {
        val writer = ClassWriter(0)

        // remap the class and write the result into the writer
        classNode.accept(ClassRemapper(writer,
                ClassNameRemapper(
                        oldName = refactorData.packagePrefix + refactorData.oldName,
                        newName = refactorData.packagePrefix + refactorData.newName)))

        return writer
    }

    override fun refactorEntity(entity: RefactorableEntity, refactorData: ClassNameRefactorData) {
        if (entity is RefactorableMember<*>) {
            when (entity.member) {
                is JavaMethod -> {
                    entity.member.identifier.identifier =
                            constructMethodIdentity(
                                    constructClassIdentity(refactorData.packagePrefix + refactorData.newName),
                                    entity.member.name,
                                    entity.member.descriptor
                            )
                }
                is JavaField -> {
                    entity.member.identifier.identifier =
                            constructFieldIdentity(
                                    constructClassIdentity(refactorData.packagePrefix + refactorData.newName),
                                    entity.member.name,
                                    entity.member.descriptor
                            )
                }
                is JavaClass -> {
                    entity.member.name = refactorData.newName
                    entity.member.identifier.identifier =
                            constructClassIdentity(refactorData.packagePrefix + refactorData.newName)
                }
                is FileBlob -> {
                    entity.member.filename = entity.member.filename.replaceRange(
                            entity.member.filename.lastIndexOf('/'),
                            entity.member.filename.lastIndexOf('.'),
                            refactorData.newName)
                }
            }
        }
    }

}

data class ClassNameRefactorData(val packagePrefix: String, val oldName: String, val newName: String)

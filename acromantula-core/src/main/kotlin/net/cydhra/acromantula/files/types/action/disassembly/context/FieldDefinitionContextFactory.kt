package net.cydhra.acromantula.files.types.action.disassembly.context

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.operations.refactoring.rename.FieldNameRefactorOperation
import net.cydhra.acromantula.files.operations.searching.SearchFieldReferencesOperation
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory
import net.cydhra.acromantula.gui.controller.context.constructActionArray

/**
 * Creates context menus for field declarations in disassembled text on demand.
 *
 * @param file the disassembled file
 */
class FieldDefinitionContextFactory(file: FileBlob, fieldIdentity: String) :
        ContextMenuFactory<String>(
                file,
                fieldIdentity,
                *constructActionArray {
                    add(FieldNameRefactorOperation)
                    add(SearchFieldReferencesOperation)
                }
        )
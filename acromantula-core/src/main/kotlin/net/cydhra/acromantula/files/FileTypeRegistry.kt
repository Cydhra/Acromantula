package net.cydhra.acromantula.files

import com.google.common.collect.HashBiMap
import com.google.common.collect.HashMultimap
import kotlinx.coroutines.CoroutineScope
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.DefaultFileActionRequest
import net.cydhra.acromantula.files.model.RegisteredFileTypes
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.files.processors.FileProcessor
import net.cydhra.acromantula.files.types.ContentOperation
import net.cydhra.acromantula.files.types.ContentViewOperation
import net.cydhra.acromantula.files.types.FileType
import net.cydhra.acromantula.files.types.GenericBinaryFileType
import net.cydhra.acromantula.workspace.sanitizing.InspectionRunner
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * Facade to the file handling. Dispatches files to file handlers that claim responsibility for that specific file.
 * During file import a file type is specified. A file can have at most one file type.
 */
object FileTypeRegistry {

    /**
     * A list of all [FileType] instances registered at the application
     */
    private val registeredFileTypes = mutableListOf<FileType>()

    /**
     * A map that maps registered actions to their file type, so they can be applied to new actions
     */
    private val registeredActions = mutableMapOf<FileType, ContentOperation<FileEntry>>()

    /**
     * A mapping from string type descriptor to file types. Each descriptor only can be assigned to one entity. If
     * two file type entites have the same descriptor, they are considered equal.
     */
    private val descriptorToTypeMap = mutableMapOf<String, FileType>()

    /**
     * A multimap that assignes actions to file types. Each file type can have multiple assigned actions. Those
     * actions can be performed on files that are of this file type.
     */
    private val fileTypeContextActions = HashMultimap.create<String, ContentOperation<FileEntry>>()

    /**
     * A mapping from type descriptors to id. Each type descriptor must have a unique id within a workspace, but
     * those ids can be different between workspaces.
     */
    private val descriptorIdMap = HashBiMap.create<String, Int>()

    /**
     * A map of processors together with their intended file types
     */
    private val fileProcessors = mutableListOf<Pair<FileType, FileProcessor>>()

    private val fileInspectionRunners = mutableMapOf<FileType, (FileBlob) -> InspectionRunner<Any>>()

    /**
     * A map of default actions per file type.
     */
    private val defaultActions = mutableMapOf<FileType, ContentOperation<FileEntry>>()

    suspend fun initialize() {
        EventBroker.registerEventListener(FileHandlerService, DefaultFileActionRequest::class, this::onRequestDefaultAction)
    }

    /**
     * Register a file type and apply all registered actions to it, that conform to this type
     *
     * @param type type of file with unique [FileType.typeDescriptor]
     *
     * @throws IllegalArgumentException if the type id is already registered
     */
    fun registerFileType(type: FileType) {
        if (!descriptorToTypeMap.containsKey(type.typeDescriptor)) {
            registeredFileTypes += type
            descriptorToTypeMap[type.typeDescriptor] = type

            // register actions to this file type, if they are meant for more general versions of this new type
            registeredActions.forEach { (actionTargetType, action) ->
                if (type.isDerivedFrom(actionTargetType))
                    fileTypeContextActions.put(type.typeDescriptor, action)
            }
        }
    }

    /**
     * Register a file action for a given file type. The action will be displayed in the context menus for files of that
     * type and any type that was derived from the given type. (e.g. an action for a ``text`` type will also be
     * available for a ``text/xml`` type)
     *
     * @param type a [FileType] instance
     * @param action a [ContentOperation] implementation that can be applied to files of the given type
     * @param isDefaultAction whether this should be registered as the default action for the given file type. If the
     * file type already has a default action, an [IllegalStateException] is thrown. The default action is only
     * registered for the most specific file type.
     */
    fun registerFileAction(type: FileType, action: ContentOperation<FileEntry>, isDefaultAction: Boolean = false) {
        registeredFileTypes.forEach {
            if (it.isDerivedFrom(type))
                fileTypeContextActions.put(it.typeDescriptor, action)
        }

        if (isDefaultAction) {
            if (defaultActions.containsKey(type))
                throw IllegalStateException("type ${type.typeDescriptor} already has default action " +
                        "${defaultActions[type]!!.name}")

            defaultActions[type] = action
        }
    }

    /**
     * Register a [FileProcessor] instance for a specific file type and all of its subtypes
     *
     * @param type a [FileType] the new processor is intended for
     * @param processor a [FileProcessor] implementation
     */
    fun registerFileProcessor(type: FileType, processor: FileProcessor) {
        fileProcessors += type to processor
    }

    /**
     * Register an [InspectionRunner] for the given file type. There can only be one runner per file type.
     *
     * @param type the file type
     * @param runnerConstructor the inspection runner
     */
    @Suppress("UNCHECKED_CAST")
    fun <E, R : InspectionRunner<E>> registerFileInspectionRunner(type: FileType,
                                                                  runnerConstructor: (FileBlob) -> R) {
        fileInspectionRunners[type] = runnerConstructor as (FileBlob) -> InspectionRunner<Any>
    }

    /**
     * Decide a file type and sets it for the given file. Asserts that the blob in the entity is valid in the current
     * transaction.
     *
     * @param file [FileBlob] entity holding the file content as a blob
     */
    fun setFileType(file: FileBlob) {
        assert(TransactionManager.manager.currentOrNull() != null)

        transaction {
            file.fileType = registeredFileTypes
                    .filter { it.autoAssignable }
                    .firstOrNull { type -> type.isFileConforming(file) } ?: GenericBinaryFileType
        }
    }

    /**
     * @return a set of [ContentOperation]s for the given [type]
     */
    fun getFileTypeActions(type: FileType): Set<ContentOperation<FileEntry>> {
        return fileTypeContextActions.get(type.typeDescriptor)
    }

    /**
     * Setup the mapping from file type ids to their [FileType] entities. All file types that are not registered
     */
    fun setFileTypeMapping(mapping: List<Pair<String, Int>>) {
        descriptorIdMap.clear()
        mapping.toMap(descriptorIdMap)

        transaction {
            registeredFileTypes
                    .filter { !descriptorIdMap.containsKey(it.typeDescriptor) }
                    .forEach(this@FileTypeRegistry::insertFileTypeIntoWorkspace)
        }
    }

    /**
     * Insert a previously unregistered file type into database and save the assigned entity id to [descriptorIdMap]
     *
     * @param type unregistered file type
     */
    private fun insertFileTypeIntoWorkspace(type: FileType) {
        transaction {
            RegisteredFileTypes.insert { it[fileTypeDescriptor] = type.typeDescriptor }

            descriptorIdMap[type.typeDescriptor] =
                    RegisteredFileTypes.select {
                        RegisteredFileTypes.fileTypeDescriptor eq type.typeDescriptor
                    }.first()[RegisteredFileTypes.id].value
        }
    }

    /**
     * Get the file type that is registered onto this id
     *
     * @param fileTypeId any file type id
     */
    fun getFileType(fileTypeId: Int): FileType? {
        return descriptorToTypeMap[descriptorIdMap.inverse()[fileTypeId] ?: return null]
    }

    /**
     * @param value any file type
     *
     * @return the workspace-unique id of the given file type
     */
    fun getId(value: FileType): Int {
        return descriptorIdMap[value.typeDescriptor] ?: throw IllegalArgumentException("Tried to get id for " +
                "unregistered type: " + value.typeDescriptor)
    }

    /**
     * Generate an [InspectionRunner] for the given file
     *
     * @return the generated runner or null, if there is no runner defined for the given file's type
     */
    fun generateRunner(file: FileBlob): InspectionRunner<Any>? {
        return fileInspectionRunners[file.fileType]?.invoke(file)
    }

    /**
     * Call all file processors intended for a file of this type. The processors that are eligible for this type and
     * have more rounds to go, will be called. If more processors require at least one more round, true is returned.
     *
     * @param fileBlob a file that is to be processed
     * @param databaseDispatcher a single-thread concurrent dispatcher for critical database accesses
     * @param round the current processing round
     *
     * @return true, if further processing rounds are required for full processing, false if all processors are
     * handled completely
     */
    suspend fun processFile(fileBlob: FileBlob, databaseDispatcher: CoroutineScope, round: Int): Boolean {
        return fileProcessors
                .filter { (type, _) -> fileBlob.fileType.isDerivedFrom(type) }
                .map { (_, processor) -> processor.apply { processFile(fileBlob, databaseDispatcher, round) } }
                .any { it.processingRounds > round }
    }

    suspend fun onRequestDefaultAction(event: DefaultFileActionRequest) {
        if (defaultActions.containsKey(event.file.fileType)) {
            val action = defaultActions[event.file.fileType]!!

            if (event.jumpTarget != null) {
                if (action is ContentViewOperation) {
                    action.onViewAction(event.file.fileBlob, event.file, event.jumpTarget)
                } else {
                    throw IllegalStateException("there is no default view-action for file type ${event.file.fileType.typeDescriptor}")
                }
            } else {
                action.onAction(event.file.fileBlob, event.file)
            }
            event.fulfill()
        }
    }
}
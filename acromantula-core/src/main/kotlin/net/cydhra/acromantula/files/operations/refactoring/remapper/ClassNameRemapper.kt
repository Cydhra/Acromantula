package net.cydhra.acromantula.files.operations.refactoring.remapper

import org.objectweb.asm.commons.Remapper

class ClassNameRemapper(private val oldName: String, private val newName: String) : Remapper() {

    override fun map(internalName: String): String {
        return if (internalName == oldName) newName else super.map(internalName)
    }
}
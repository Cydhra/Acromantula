package net.cydhra.acromantula.files.types.action.decompile

import com.strobel.assembler.metadata.*
import com.strobel.decompiler.ITextOutput
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.gui.controller.utility.*
import net.cydhra.acromantula.files.types.action.disassembly.context.ClassDefinitionContextFactory
import net.cydhra.acromantula.files.types.action.disassembly.context.ClassReferenceContextFactory
import net.cydhra.acromantula.files.types.action.disassembly.context.FieldDefinitionContextFactory
import net.cydhra.acromantula.files.types.action.disassembly.context.MethodDefinitionContextFactory
import net.cydhra.acromantula.files.utility.constructClassIdentity
import net.cydhra.acromantula.files.utility.constructFieldIdentity
import net.cydhra.acromantula.files.utility.constructMethodIdentity

class ProcyonMetaTextOutput(private val metaTextWriter: MetaTextWriter, private val file: FileBlob) : ITextOutput {

    override fun indent() {
        metaTextWriter.indent()
    }

    override fun writeReference(text: String, reference: Any) {
        val contextFactory = getReferenceContextFactory(reference)
        metaTextWriter.write(text, contextFactory = contextFactory, styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun writeReference(text: String, reference: Any, isLocal: Boolean) {
        val contextFactory = getReferenceContextFactory(reference)
        metaTextWriter.write(text, contextFactory = contextFactory, styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun getReferenceContextFactory(reference: Any): ContextMenuFactory<String>? {
        return when (reference) {
            is TypeReference -> ClassReferenceContextFactory(file, constructClassIdentity(reference.internalName))
            else -> null
        }
    }

    override fun markFoldStart(collapsedText: String?, defaultCollapsed: Boolean) {

    }

    override fun unindent() {
        metaTextWriter.unindent()
    }

    override fun isFoldingSupported(): Boolean {
        return false
    }

    override fun write(ch: Char) {
        metaTextWriter.write(ch.toString(), styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun write(text: String) {
        metaTextWriter.write(text, styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun write(format: String, vararg args: Any?) {
        metaTextWriter.write(String.format(format, args), styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun writeComment(value: String) {
        metaTextWriter.write(value, styleClasses = *arrayOf(TEXT_CODE, CODE_COMMENT))
    }

    override fun writeComment(format: String, vararg args: Any?) {
        metaTextWriter.write(String.format(format, args), styleClasses = *arrayOf(TEXT_CODE, CODE_COMMENT))
    }

    override fun writeOperator(text: String) {
        metaTextWriter.write(text, styleClasses = *arrayOf(TEXT_CODE, CODE_OPERATOR))
    }

    override fun getIndentToken(): String {
        return metaTextWriter.indentationToken
    }

    override fun writeKeyword(text: String) {
        metaTextWriter.write(text, styleClasses = *arrayOf(TEXT_CODE, CODE_KEYWORD))
    }

    override fun getRow(): Int {
        return -1
    }

    override fun writeDefinition(text: String, definition: Any) {
        metaTextWriter.write(text, styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun writeDefinition(text: String, definition: Any, isLocal: Boolean) {
        metaTextWriter.write(text, contextFactory = getDefinitionContextFactory(definition),
                styleClasses = *arrayOf(TEXT_CODE))
    }

    private fun getDefinitionContextFactory(definition: Any): ContextMenuFactory<String>? {
        return when (definition) {
            is TypeDefinition -> ClassDefinitionContextFactory(file, constructClassIdentity(definition.internalName))
            is FieldDefinition -> FieldDefinitionContextFactory(file,
                    constructFieldIdentity(constructClassIdentity(definition.declaringType.internalName),
                            definition.name, reconstructPrimitiveTypes(definition.fieldType.internalName)))
            is MethodDefinition -> MethodDefinitionContextFactory(file,
                    constructMethodIdentity(constructClassIdentity(definition.declaringType.internalName),
                            definition.name, constructMethodDescriptor(definition)))
            else -> null
        }
    }

    /**
     * Reconstruct a method descriptor from its procyon definition model
     */
    private fun constructMethodDescriptor(definition: MethodDefinition): String {
        return ("(" +
                definition.parameters
                        .map(ParameterDefinition::getParameterType)
                        .map(TypeReference::getInternalName).joinToString("", transform = this::reconstructPrimitiveTypes) +
                ")" +
                reconstructPrimitiveTypes(definition.returnType.internalName))
    }

    /**
     * Reconstruct the short names for primitive types from a type name.
     *
     * @return the jvm internal representation of the given type name
     */
    private fun reconstructPrimitiveTypes(typeName: String): String {
        return when {
            typeName.startsWith("[") -> "[L${typeName.removeRange(0, 1)};"
            !arrayOf("B", "C", "D", "F", "I", "J", "S", "Z").contains(typeName) -> "L$typeName;"
            else -> typeName
        }

    }

    override fun writeError(value: String) {
        metaTextWriter.write(value, styleClasses = *arrayOf(TEXT_CODE, CODE_COMMENT))
    }

    override fun writeAttribute(text: String) {
        metaTextWriter.write(text, styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun getColumn(): Int {
        return -1
    }

    override fun writeLabel(value: String) {
        metaTextWriter.write(value, styleClasses = *arrayOf(TEXT_CODE, CODE_LABEL))
    }

    override fun writeLiteral(value: Any?) {
        metaTextWriter.write(value.toString(), styleClasses = *arrayOf(TEXT_CODE,
                if (value is String) CODE_STRING else CODE_LITERAL))
    }

    override fun writeLine(text: String) {
        metaTextWriter.writeLine(text, styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun writeLine(format: String, vararg args: Any?) {
        metaTextWriter.writeLine(String.format(format, args), styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun writeLine() {
        metaTextWriter.writeLine("", styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun writeTextLiteral(value: Any?) {
        metaTextWriter.write(value.toString(), styleClasses = *arrayOf(TEXT_CODE, CODE_STRING))
    }

    override fun setIndentToken(indentToken: String) {
        throw NotImplementedError("cannot change indentToken")
    }

    override fun writeDelimiter(text: String) {
        metaTextWriter.write(text, styleClasses = *arrayOf(TEXT_CODE))
    }

    override fun markFoldEnd() {

    }
}
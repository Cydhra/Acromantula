package net.cydhra.acromantula.files.types

import net.cydhra.acromantula.files.model.database.FileBlob

/**
 * [FileType] for files of unknown type
 */
object GenericBinaryFileType : FileType("bin") {

    override fun isFileConforming(file: FileBlob): Boolean {
        return true
    }
}
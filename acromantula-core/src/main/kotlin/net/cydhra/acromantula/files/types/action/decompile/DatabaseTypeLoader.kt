package net.cydhra.acromantula.files.types.action.decompile

import com.strobel.assembler.metadata.ArrayTypeLoader
import com.strobel.assembler.metadata.Buffer
import com.strobel.assembler.metadata.ClasspathTypeLoader
import com.strobel.assembler.metadata.ITypeLoader
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.java.JavaClass
import net.cydhra.acromantula.files.model.database.java.JavaClasses
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * A type loader that loads classes from the database or the classpath if the database does not hold the classes
 */
class DatabaseTypeLoader : ITypeLoader {

    override fun tryLoadType(internalName: String, buffer: Buffer): Boolean {
        val internalNameParsed = internalName.removeSuffix(".class")

        val success = transaction {
            val file = FileBlob.reload(
                    JavaClass.find { JavaClasses.name eq internalNameParsed }.firstOrNull()?.file
                            ?: return@transaction false)!!

            val classContent = file.data.getBytes(1, file.data.length().toInt())
            return@transaction ArrayTypeLoader(classContent).tryLoadType(internalNameParsed, buffer)
        }

        if (success)
            return true

        return ClasspathTypeLoader().tryLoadType(internalNameParsed, buffer)
    }
}
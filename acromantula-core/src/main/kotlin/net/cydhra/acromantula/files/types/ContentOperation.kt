package net.cydhra.acromantula.files.types

import net.cydhra.acromantula.files.model.database.FileBlob

/**
 * An operation of any workspace content. This may be a [FileType] that specifies an action that can be applied upon
 * it, or contents of the file that specify operations possible on them.
 *
 * @param E entity type that specifies the action
 */
abstract class ContentOperation<E> {

    /**
     * Name of the action displayed in the user interface. This does not take the actual entity into account, but
     * instead serves as a general title for the operation.
     */
    abstract val name: String

    /**
     * This method is executed when the user selects this action for a file. The method is suspendable to account for
     * user dialogs. Dialogs must be opened in the UI thread and the user then must complete the dialog. Until then,
     * the action tasks should be blocking which is achieved by executing this function using ``runBlocking {}``.
     *
     * @param file the file the user tries to apply this action on
     * @param entity the entity that is target of this operation
     */
    abstract suspend fun onAction(file: FileBlob, entity: E)
}
package net.cydhra.acromantula.files.model.database.java

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.transactions.transaction

object JavaReferences : IntIdTable() {
    val referenceSource = reference("source", JavaMethods, ReferenceOption.CASCADE)
    val instructionNumber = integer("instruction")
    val referenced = reference("referenced", JavaIdentifiers, ReferenceOption.NO_ACTION)
}

class JavaReference(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<JavaReference>(JavaReferences)

    var referenceSource by JavaMethod referencedOn JavaReferences.referenceSource
    var referenced by JavaIdentifier referencedOn JavaReferences.referenced
    var instruction by JavaReferences.instructionNumber

    /**
     * Used by GUI. Referenced element is not relevant to the GUI
     */
    override fun toString(): String {
        return transaction { referenceSource.identifier.identifier }
    }
}


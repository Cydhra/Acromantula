package net.cydhra.acromantula.files.operations.refactoring.model

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.utility.model.CodeReference

/**
 * Any code entity that is to be refactored. This can either be a code [member][RefactorableMember] of some kind or a
 * [reference][RefactorableReference] to a member.
 *
 * @param codeFileBlob the file containing the code to be refactored
 */
abstract class RefactorableEntity(val codeReference: CodeReference) {

    val codeFileBlob: FileBlob
        get() = codeReference.codeFileBlob

    override fun toString(): String {
        return codeReference.toString()
    }
}
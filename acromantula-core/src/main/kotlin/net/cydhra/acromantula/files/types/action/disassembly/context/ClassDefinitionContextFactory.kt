package net.cydhra.acromantula.files.types.action.disassembly.context

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.operations.refactoring.rename.ClassNameRefactorOperation
import net.cydhra.acromantula.files.operations.searching.SearchClassReferencesOperation
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory
import net.cydhra.acromantula.gui.controller.context.constructActionArray

class ClassDefinitionContextFactory(file: FileBlob, classIdentifier: String) :
        ContextMenuFactory<String>(
                file,
                classIdentifier,
                *constructActionArray {
                    add(ClassNameRefactorOperation)
                    add(SearchClassReferencesOperation)
                }
        )
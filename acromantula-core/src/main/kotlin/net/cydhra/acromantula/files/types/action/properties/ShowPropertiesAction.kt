package net.cydhra.acromantula.files.types.action.properties

import javafx.application.Platform
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.files.types.ContentOperation
import net.cydhra.acromantula.gui.controller.dialog.PropertyDialogView
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog
import org.controlsfx.control.PropertySheet
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

/**
 * Context action for all file types that displays a dialog with metadata and file properties
 */
object ShowPropertiesAction : ContentOperation<FileEntry>() {
    override val name: String = "View Metadata"

    override suspend fun onAction(file: FileBlob, entity: FileEntry) {
        Platform.runLater {
            val observableList = FXCollections.observableArrayList<MetadataPropertyItem>()

            // obtain available metadata keys
            transaction {
                file.metadataMap.forEach { (key, value) ->
                    observableList += MetadataPropertyItem(key, "metadata", key, value)
                }
            }

            // construct property sheet
            @Suppress("UNCHECKED_CAST")
            val propertySheet = PropertySheet(observableList as ObservableList<PropertySheet.Item>).apply {
                this.isModeSwitcherVisible = false
            }

            // construct save-properties callback
            val saveProperties = { e: ActionEvent ->
                transaction {
                    observableList.filterIsInstance<MetadataPropertyItem>()
                            .filter { it.isDirty }
                            .forEach { item ->
                                file.metadata.find { it.key == item.key }!!.data = item.value
                            }
                }
            }

            // show dialog (no owner)
            showStageCallbackDialog("Properties", PropertyDialogView(propertySheet), "OK" to saveProperties,
                    "Cancel" to { _ -> })
        }
    }

    /**
     * Item that acts as an observable adapter for the [PropertySheet] constructed by [ShowPropertiesAction]. It offers
     * an interface for the properties that are shown for files.
     */
    class MetadataPropertyItem(val key: String, private val categoryName: String,
                               private val propertyName: String, private var value: String,
                               private val propertyDescription: String = "") : PropertySheet.Item {
        var isDirty = false
            private set

        override fun setValue(value: Any?) {
            this.value = value.toString()
            isDirty = true
        }

        override fun getName(): String {
            return this.propertyName
        }

        override fun getDescription(): String {
            return this.propertyDescription
        }

        override fun getType(): Class<*> {
            return String::class.java
        }

        override fun getValue(): String {
            return this.value
        }

        override fun isEditable(): Boolean {
            return true
        }

        override fun getObservableValue(): Optional<ObservableValue<out Any>> {
            return Optional.empty()
        }

        override fun getCategory(): String {
            return this.categoryName
        }
    }
}
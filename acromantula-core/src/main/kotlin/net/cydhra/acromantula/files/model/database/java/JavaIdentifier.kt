package net.cydhra.acromantula.files.model.database.java

import net.cydhra.acromantula.MAX_IDENTIFIER_LENGTH
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * A table containing all identifiers of Java members. Storing them in an extra table saves space and enables to link them.
 */
object JavaIdentifiers : IntIdTable() {
    val identifier = varchar("identifier", MAX_IDENTIFIER_LENGTH).uniqueIndex()

}
/**
 * Data model of a member identifier for any Java symbol
 *
 * @param id database entity id
 */
class JavaIdentifier(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<JavaIdentifier>(JavaIdentifiers)
    /**
     * The string representation of a Java member
     */
    var identifier by JavaIdentifiers.identifier

}

/**
 * Select the [JavaIdentifier] entity with given identity string or insert it, if it does not exist yet.
 *
 * @param identity a unique java class member identity
 *
 * @return a [JavaIdentifier] entity wrapping the unique identify of a member
 */
fun JavaIdentifiers.selectOrInsert(identity: String): JavaIdentifier {
    return transaction {
        JavaIdentifier.find { identifier eq identity }.firstOrNull() ?: JavaIdentifier.new { identifier = identity }
    }
}

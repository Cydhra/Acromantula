package net.cydhra.acromantula.files.operations.renaming

import javafx.application.Platform
import javafx.collections.ObservableList
import net.cydhra.acromantula.concurrency.ThreadPool
import net.cydhra.acromantula.concurrency.tasks.ContentOperationTask
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.java.JavaClass
import net.cydhra.acromantula.files.types.ContentOperation
import net.cydhra.acromantula.gui.controller.ResolveIssuesDialogController
import net.cydhra.acromantula.gui.controller.utility.showErrorDialog
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog
import net.cydhra.acromantula.workspace.importer.tools.ClassParser
import net.cydhra.acromantula.workspace.sanitizing.InspectionIssue
import net.cydhra.acromantula.workspace.sanitizing.Sanitizer
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.tree.ClassNode

/**
 * An abstract strategy to rename class members. Overwritten to specify exact remapping procedure.
 */
abstract class RenameAction<T>(override val name: String) : ContentOperation<String>() {

    companion object {
        private val logger = LogManager.getLogger()
    }

    /**
     * The title for the dialog asking to rename the member
     */
    abstract val renameDialogTitle: String

    override suspend fun onAction(file: FileBlob, entity: String) {
        val memberNode = getEntity(entity)
        val referenceList = generateReferenceList(entity)

        // show the rename element dialog
        val dialog = TODO()//RenameElementDialogView(referenceList, getEntityName(entity))
        Platform.runLater {
            showStageCallbackDialog(
                    title = renameDialogTitle + getEntityName(entity),
                    content = dialog,
                    resolveOptions = *arrayOf(
                            "Rename" to rename@{ _ ->
                                logger.debug("rename element dialog \"$renameDialogTitle\" approved")

                                val newName = ""//dialog.getNewMemberName()

                                // don't rename if already equal
                                if (entity == newName)
                                    return@rename


                                // generate class nodes of the to be remapped classes.
                                val referencedClasses: List<Pair<JavaClass, ClassNode>> = transaction {
                                    // deduplicate class entries to save on transactions and dont confuse the user
                                    val resolvedClasses = mutableSetOf<Int>()

                                    referenceList.mapNotNull { reference ->
                                        if (!resolvedClasses.contains(reference.referencingClass.id.value)) {
                                            resolvedClasses += reference.referencingClass.id.value
                                            val reloadedBlob = FileBlob.reload(reference.referencingClass.file)!!
                                            // generate the classnode
                                            reference.referencingClass to ClassParser.tryParseClass(
                                                    reloadedBlob.data.getBytes(1, reloadedBlob.data.length().toInt()))!!
                                        } else {
                                            null
                                        }
                                    }
                                }

                                // run inspections on classnodes
                                val issues = referencedClasses
                                        .map(Pair<JavaClass, ClassNode>::second)
                                        .map(Sanitizer::sanitizeCritical)
                                        .flatten()

                                logger.debug("${issues.size} issues were found")

                                // pre-generate the task, that handles the remapping
                                val operationTask = generateContentOperationTask(entity, referenceList,
                                        referencedClasses, getEntityName(entity), newName)

                                // define task callbacks. This is saved in a lambda because the task might be submitted
                                // by the issue resolving dialog or directly, if no such dialog is opened
                                val renameOperationCallbacks: ThreadPool.TaskContext<Unit>.() -> Unit = {
                                    onSuccess { logger.info("renamed $entity to $newName") }
                                    onFailure { t ->
                                        logger.error("error during remapping for field $entity", t)
                                        Platform.runLater {
                                            showErrorDialog("error during renaming of $entity", t.message
                                                    ?: "No further" +
                                                    " details. See log output.")
                                        }
                                    }
                                    onCancelled { logger.info("renaming $entity was cancelled.") }
                                }


                                // resolve pending issues if any
                                if (issues.isNotEmpty()) {
                                    @Suppress("UNCHECKED_CAST")
                                    val issuesDialog = ResolveIssuesDialogController(issues as List<InspectionIssue<Any>>)
                                    showStageCallbackDialog(
                                            title = "Critical Issues",
                                            content = issuesDialog,
                                            resolveOptions = *arrayOf(
                                                    "Continue" to continueDialog@{ _ ->
                                                        if (issuesDialog.hasMoreIssues()) {
                                                            Platform.runLater {
                                                                // TODO remain in dialog instead of aborting
                                                                showErrorDialog("Critical Issues Remaining",
                                                                        "Not all issues were resolved. Aborting.")
                                                            }
                                                            return@continueDialog
                                                        }

                                                        // remap with the fixed class nodes
                                                        ThreadPool.submit(operationTask, renameOperationCallbacks)
                                                    },
                                                    "Cancel" to { _ ->
                                                        logger.debug("resolve issues dialog cancelled")
                                                    })
                                    )
                                } else {
                                    // remap with the unchanged classnodes
                                    ThreadPool.submit(operationTask, renameOperationCallbacks)
                                }
                            },
                            "Cancel" to { _ ->
                                logger.debug("rename element dialog \"$renameDialogTitle\" cancelled")
                            }
                    ) // end of resolve options array
            ) // end of show stage dialog
        }
    }

    /**
     * Generate an observable list of references to the member that is going to be renamed.
     *
     * @param entity the entity to be renamed
     */
    abstract fun generateReferenceList(entity: String): ObservableList<MemberHandleReference>

    /**
     * Generate the [ContentOperationTask] that is used to remap the member.
     *
     * @param entity the entity to be renamed
     * @param referenceList the list of references to be changed
     * @param affectedClassNodes a list of class nodes and class entities affected by the change
     * @param oldName old member name
     * @param newName new member name
     */
    abstract fun generateContentOperationTask(entity: String,
                                              referenceList: List<MemberHandleReference>,
                                              affectedClassNodes: List<Pair<JavaClass, ClassNode>>,
                                              oldName: String, newName: String): ContentOperationTask<Unit>

    /**
     * Generate the member node that is to be renamed
     *
     * @param identifier the member identity
     *
     * @return the member instance used for renaming
     */
    abstract fun getEntity(identifier: String): T

    /**
     * Get name of the entity to rename
     *
     * @param identifier the member identity
     */
    abstract fun getEntityName(identifier: String): String
}
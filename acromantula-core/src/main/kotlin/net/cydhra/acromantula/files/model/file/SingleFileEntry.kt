package net.cydhra.acromantula.files.model.file

import net.cydhra.acromantula.files.model.DefaultWorkspaceEntry
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.FileBlobs
import net.cydhra.acromantula.files.types.FileType
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * A single class file imported directly. It is placed directly in the workspace tree, not in a sub-element.
 *
 * @param workspace the workspace this file is imported into
 * @param name the name of the class entry
 */
class SingleFileEntry(name: String, fileType: FileType)
    : FileEntry, WorkspaceEntry by DefaultWorkspaceEntry(null, name, fileType?.fileTypeImage) {

    override val fileType: FileType by lazy {
        if (fileType != null)
            fileType
        else
            transaction {
                fileBlob.fileType
            }
    }

    override val fileBlob: FileBlob by lazy {
        transaction {
            FileBlob.find { FileBlobs.filename eq this@SingleFileEntry.name }.first()
        }
    }

    override fun registerAtParent() {
        this.parent?.addChild(this)
    }

    override fun toString(): String {
        return this.displayName
    }
}
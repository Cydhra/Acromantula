package net.cydhra.acromantula.files.model.database.java

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

/**
 * Many-to-many relation table that saves relations of classes implementing interfaces
 */
object JavaInterfaceRelations : IntIdTable() {
    val implementingClass = reference("implementing_class", JavaClasses)
    val interfaceIdentifier = reference("interface_identifier", JavaIdentifiers)
    val interfaceEntity = reference("interface", JavaClasses).nullable()

    init {
        uniqueIndex(implementingClass, interfaceIdentifier, interfaceEntity)
    }
}

class JavaInterfaceRelation(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<JavaInterfaceRelation>(JavaInterfaceRelations)

    var implementingClass by JavaClass referencedOn JavaInterfaceRelations.implementingClass
    var interfaceIdentifier by JavaIdentifier referencedOn JavaInterfaceRelations.interfaceIdentifier
    var interfaceEntity by JavaClass optionalReferencedOn JavaInterfaceRelations.interfaceEntity
}
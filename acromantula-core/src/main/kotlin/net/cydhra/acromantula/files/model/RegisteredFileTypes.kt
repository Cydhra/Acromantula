package net.cydhra.acromantula.files.model

import org.jetbrains.exposed.dao.IntIdTable

/**
 * A SQL table model for file types that are registered at the application. Each file type is assigned a unique
 * id, but since plugins can register file types dynamically, the id mapping must be stored per workspace to keep
 * it unique across different plugin setups
 */
object RegisteredFileTypes : IntIdTable() {
    val fileTypeDescriptor = varchar("descriptor", 255)
}
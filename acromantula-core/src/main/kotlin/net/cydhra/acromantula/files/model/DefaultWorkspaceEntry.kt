package net.cydhra.acromantula.files.model

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.collections.transformation.SortedList

/**
 * Delegate for [WorkspaceEntries][WorkspaceEntry] that implement default behaviour of those.
 */
class DefaultWorkspaceEntry(override var parent: WorkspaceEntry?, override val name: String,
                            override val imageLocation: String) : WorkspaceEntry {

    override val displayName: String = this.name.substring(this.name.lastIndexOf('/', this.name.length - 2)
            .takeIf { it > 0 }?.let { it + 1 } ?: 0, this.name.length)

    private val mutableChildrenList: ObservableList<WorkspaceEntry> = FXCollections.observableArrayList()
    override val children: ObservableList<WorkspaceEntry> = SortedList(mutableChildrenList, defaultWorkspaceEntryComparator)

    override fun addChild(childEntry: WorkspaceEntry) {
        this.mutableChildrenList += childEntry
    }

    override fun toString(): String {
        return displayName
    }

    override fun registerAtParent() {
        throw IllegalStateException("DefaultWorkspaceEntry delegate is not allowed to register itself at the parent. " +
                "Reimplement this function in the delegating class.")
    }
}
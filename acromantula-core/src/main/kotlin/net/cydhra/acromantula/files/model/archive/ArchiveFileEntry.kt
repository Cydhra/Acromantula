package net.cydhra.acromantula.files.model.archive

import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.FileBlobs
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.files.types.FileType
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * A file entry from an archive that is displayed in the file view
 */
class ArchiveFileEntry(parent: WorkspaceEntry, name: String, fileType: FileType) : FileEntry,
        ArchiveEntry(parent, name, fileType.fileTypeImage) {
    override val fileType: FileType by lazy {
        if (fileType != null)
            fileType
        else
            transaction {
                fileBlob.fileType
            }
    }

    override val fileBlob: FileBlob by lazy {
        transaction {
            FileBlob.find { FileBlobs.filename eq this@ArchiveFileEntry.name }.first()
        }
    }
}

package net.cydhra.acromantula.files.types.action.text

import javafx.application.Platform
import javafx.geometry.Point2D
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.OpenEditorTabRequest
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.META_DATA_KEY_ENCODING
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.files.types.ContentViewOperation
import net.cydhra.acromantula.files.utility.model.JumpTarget
import net.cydhra.acromantula.gui.controller.EditorTabView
import net.cydhra.acromantula.gui.controller.components.TextEditorView
import net.cydhra.acromantula.plugins.internal.InternalPlugin
import org.jetbrains.exposed.sql.transactions.transaction
import java.nio.charset.Charset

/**
 * An action for context menus of all binary file types to open a raw text editor on them
 */
object EditTextAction : ContentViewOperation<FileEntry>() {
    override val name: String = "Edit Text"

    override suspend fun onViewAction(file: FileBlob, entity: FileEntry, jumpTarget: JumpTarget?) {
        val contentUpdateFunction = { editor: EditorTabView, file: FileBlob, jumpTarget: JumpTarget? ->
            Platform.runLater {
                transaction {
                    val reloadedFile = FileBlob.reload(file)!!
                    (editor as TextEditorView).codeArea.replaceText(
                            String(bytes = reloadedFile.data.getBytes(0, reloadedFile.data.length().toInt()),
                                    charset = Charset.forName(reloadedFile.metadataMap[META_DATA_KEY_ENCODING]
                                            ?: InternalPlugin.defaultEncoding)))
                    editor.codeArea.scrollToPixel(Point2D(0.0, 0.0))
                }
            }
        }

        val editorTab = TextEditorView(file, contentUpdateFunction)

        val request = OpenEditorTabRequest(entity.displayName + " [Text View]", "text", editorTab)
        request.onFulfill {
            contentUpdateFunction(editorTab, file, jumpTarget)
        }
        EventBroker.fireEvent(request)
    }
}
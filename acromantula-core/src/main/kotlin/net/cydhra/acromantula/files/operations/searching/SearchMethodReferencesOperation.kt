package net.cydhra.acromantula.files.operations.searching

import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableEntity
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableMember
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableReference
import net.cydhra.acromantula.files.utility.model.CodeReference
import net.cydhra.acromantula.files.utility.model.InstructionJumpTarget
import net.cydhra.acromantula.files.utility.model.MethodDeclarationJumpTarget
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

object SearchMethodReferencesOperation : SearchReferencesOperation<JavaMethod>() {
    override val name: String = "Search Method References"

    override fun obtainEntityFromDatabase(entityIdentification: String): JavaMethod {
        return transaction {
            val identifierId = JavaIdentifier
                    .find { JavaIdentifiers.identifier eq entityIdentification }.single().id
            JavaMethod.find { JavaMethods.identifier eq identifierId }.first()
        }
    }

    override fun collectReferences(entity: JavaMethod): List<CodeReference> {
        val inheritanceTree: Queue<Int> = ArrayDeque()
        inheritanceTree.add(entity.id.value)

        val workedSet: MutableSet<Int> = HashSet()
        var currentMethod: JavaMethod? = entity
        val referenceList = mutableListOf<RefactorableEntity>()

        transaction {
            referenceList += RefactorableMember(CodeReference(entity.owner.file,
                    MethodDeclarationJumpTarget(entity.identifier.identifier)), entity)

            while (currentMethod != null) {
                workedSet += currentMethod!!.id.value

                referenceList += transaction {
                    JavaReference
                            .find { JavaReferences.referenced eq currentMethod!!.identifier.id }
                            .map {
                                RefactorableReference(CodeReference(it.referenceSource.owner.file,
                                        InstructionJumpTarget(it.referenceSource.identifier.identifier, it.instruction)),
                                        it.referenceSource)
                            }
                }

                val overrides = transaction {
                    JavaOverride
                            .find { JavaOverrides.method eq currentMethod!!.id }
                            .filter { !workedSet.contains(it.overrides.id.value) }
                            .map {
                                RefactorableMember(CodeReference(it.overrides.owner.file,
                                        MethodDeclarationJumpTarget(it.overrides.identifier.identifier)), it.overrides)
                            }
                }

                overrides.forEach { reference ->
                    if (!inheritanceTree.contains(reference.member.id.value)) {
                        inheritanceTree.add(reference.member.id.value)
                        referenceList += reference
                    }
                }

                val overriddenBy = transaction {
                    JavaOverride
                            .find { JavaOverrides.overrides eq currentMethod!!.id }
                            .filter { !workedSet.contains(it.method.id.value) }
                            .map {
                                RefactorableMember(CodeReference(it.method.owner.file,
                                        MethodDeclarationJumpTarget(it.method.identifier.identifier)), it.method)
                            }
                }

                overriddenBy.forEach { reference ->
                    if (!inheritanceTree.contains(reference.member.id.value)) {
                        inheritanceTree.add(reference.member.id.value)
                        referenceList += reference
                    }
                }

                currentMethod = transaction { JavaMethod.find { JavaMethods.id eq inheritanceTree.poll() }.singleOrNull() }
            }
        }

        return referenceList.map { it.codeReference }
    }

}
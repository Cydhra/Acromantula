package net.cydhra.acromantula.files.operations.refactoring.remapper

import org.objectweb.asm.commons.Remapper

/**
 * Remaps a method name of a set of classes. The method is member of all classes in the set through inheritance. The
 * method is identified by its name and its descriptor.
 *
 * @param classTreeSet a set of classes that declare or override the method that shall be renamed
 * @param methodName old name of the method
 * @param methodDescriptor descriptor of the method to rename
 * @param newName new name of the method
 */
class MethodNameRemapper(private val classTreeSet: Set<String>, private val methodName: String,
                         private val methodDescriptor: String, private val newName: String) : Remapper() {
    override fun mapMethodName(owner: String, name: String, descriptor: String): String {
        return if (classTreeSet.contains(owner) && name == methodName && descriptor == methodDescriptor)
            newName
        else
            super.mapMethodName(owner, name, descriptor)
    }
}
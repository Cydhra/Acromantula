package net.cydhra.acromantula.files.model.database

import net.cydhra.acromantula.MAX_IDENTIFIER_LENGTH
import net.cydhra.acromantula.files.FileTypeRegistry
import net.cydhra.acromantula.files.types.FileType
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

/**
 * A table that saves imported files, so they don't need to be tracked outside of the application and can be altered
 * without changing the original file.
 */
object FileBlobs : IntIdTable() {
    val filename = varchar("filename", MAX_IDENTIFIER_LENGTH).index()
    val parent = reference("parent", Directories, ReferenceOption.CASCADE).nullable().index()
    val archive = reference("archive", Archives, ReferenceOption.CASCADE).nullable()
    val data = blob("data")
    val fileType = integer("type").default(0)
}

/**
 * A file blob entity of the [FileBlobs] table.
 *
 * @param id the entity id inside the table.
 */
class FileBlob(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<FileBlob>(FileBlobs)

    private var fileTypeId by FileBlobs.fileType

    var filename by FileBlobs.filename
    var parent by Directory optionalReferencedOn FileBlobs.parent
    var archive by Archive optionalReferencedOn FileBlobs.archive
    var data by FileBlobs.data

    val metadata by FileMetaDataPair referrersOn FileMetaData.file

    var fileType: FileType
        get() = FileTypeRegistry.getFileType(this.fileTypeId)
                ?: throw IllegalStateException("unknown file type typeDescriptor " + this.fileTypeId)
        set(value) {
            this.fileTypeId = FileTypeRegistry.getId(value)
        }

    /**
     * A map view on [metadata]
     */
    val metadataMap: Map<String, String>
        get() = metadata.map { it.key to it.data }.toMap()

}
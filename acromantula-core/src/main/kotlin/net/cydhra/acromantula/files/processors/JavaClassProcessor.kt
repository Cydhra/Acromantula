package net.cydhra.acromantula.files.processors

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.utility.constructClassIdentity
import net.cydhra.acromantula.files.utility.constructFieldIdentity
import net.cydhra.acromantula.files.utility.constructMethodIdentity
import net.cydhra.acromantula.files.utility.getIdentity
import net.cydhra.acromantula.workspace.importer.tools.ClassParser
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.Type
import org.objectweb.asm.tree.FieldInsnNode
import org.objectweb.asm.tree.LdcInsnNode
import org.objectweb.asm.tree.MethodInsnNode
import org.objectweb.asm.tree.TypeInsnNode

/**
 * A processor for java classes, that adds all classes and members into the database. All member identities must be
 * retrieved or inserted concurrently from within the same thread to avoid insertion conflicts or select-misses from
 * uncommitted transactions. Therefore a single-thread-dispatcher is used for identity-retrieval, parallel database
 * accesses are used to insert the respective entities, as they are uncritical.
 */
object JavaClassProcessor : FileProcessor {

    override val processingRounds: Int = 3

    override suspend fun processFile(fileBlob: FileBlob, concurrentDatabaseAccessDispatcher: CoroutineScope, round: Int) {
        when (round) {
            1 -> generateEntitiesAndReferences(fileBlob, concurrentDatabaseAccessDispatcher)
            2 -> updatePolymorphicRelations(fileBlob, concurrentDatabaseAccessDispatcher)
            3 -> insertMissingPolymorphicAndUpdateMethodOverrides(fileBlob)
        }
    }

    /**
     * Generate all direct class and member entities and the method and field references (as they do not depend on
     * other entities). Critical database accesses include searching and generating missing
     * [Identifiers][JavaIdentifier].
     *
     * @param fileBlob the processed file
     * @param concurrentDatabaseAccessDispatcher concurrent dispatcher for critical database accesses
     */
    private suspend fun generateEntitiesAndReferences(fileBlob: FileBlob, concurrentDatabaseAccessDispatcher: CoroutineScope) {
        // reload class blob from DB and create a ClassNode from it
        val classNode = transaction {
            val blob = FileBlob.reload(fileBlob)!!
            ClassParser.tryParseClass(blob.data.getBytes(0, blob.data.length().toInt()))
                    ?: throw IllegalArgumentException("invalid java class file")
        }

        // concurrently get the class identity entity
        val classIdentifierEntity = concurrentDatabaseAccessDispatcher
                .async { JavaIdentifiers.selectOrInsert(classNode.getIdentity()) }
                .await()

        // insert a class representation in parallel
        val javaClassEntity = transaction {
            JavaClass.new {
                this.identifier = classIdentifierEntity
                this.name = classNode.name
                this.file = fileBlob
            }
        }

        classNode.fields.forEach { fieldNode ->
            // concurrently get the field identity entity from DB
            val fieldIdentifierEntity = concurrentDatabaseAccessDispatcher
                    .async { JavaIdentifiers.selectOrInsert(fieldNode.getIdentity(classNode)) }
                    .await()

            // insert the field entity in parallel
            transaction {
                JavaField.new {
                    this.identifier = fieldIdentifierEntity
                    this.owner = javaClassEntity
                    this.name = fieldNode.name
                    this.descriptor = fieldNode.desc
                }
            }
        }

        classNode.methods.forEach { methodNode ->
            // concurrently get the field identity entity from DB
            val methodIdentifierEntity = concurrentDatabaseAccessDispatcher
                    .async { JavaIdentifiers.selectOrInsert(methodNode.getIdentity(classNode)) }
                    .await()

            // insert the method entity in parallel
            val methodEntity = transaction {
                JavaMethod.new {
                    this.identifier = methodIdentifierEntity
                    this.owner = javaClassEntity
                    this.name = methodNode.name
                    this.descriptor = methodNode.desc
                }
            }

            var opCodeCounter = 0

            // insert all method and field references of the method
            methodNode.instructions.iterator().forEach { instruction ->
                opCodeCounter++
                if (instruction is MethodInsnNode) {
                    // concurrently get an identity for the referenced method
                    val referencedIdEntity = concurrentDatabaseAccessDispatcher
                            .async {
                                JavaIdentifiers.selectOrInsert(
                                        constructMethodIdentity(
                                                ownerIdentity = constructClassIdentity(
                                                        fullName = instruction.owner
                                                ),
                                                methodName = instruction.name,
                                                methodDescriptor = instruction.desc))
                            }
                            .await()

                    // insert the reference entity in parallel
                    transaction {
                        JavaReference.new {
                            this.referenceSource = methodEntity
                            this.referenced = referencedIdEntity
                            this.instruction = opCodeCounter
                        }
                    }
                } else if (instruction is FieldInsnNode) {
                    // concurrently get the identity for the referenced field
                    val referencedIdEntity = concurrentDatabaseAccessDispatcher
                            .async {
                                JavaIdentifiers.selectOrInsert(
                                        constructFieldIdentity(
                                                ownerIdentity = constructClassIdentity(
                                                        fullName = instruction.owner
                                                ),
                                                fieldName = instruction.name,
                                                fieldDescriptor = instruction.desc
                                        ))
                            }
                            .await()

                    // insert the reference entity in parallel
                    transaction {
                        JavaReference.new {
                            this.referenceSource = methodEntity
                            this.referenced = referencedIdEntity
                            this.instruction = opCodeCounter
                        }
                    }
                } else if (instruction is LdcInsnNode) {
                    if (instruction.cst is Type) {
                        val referencedEntity = concurrentDatabaseAccessDispatcher
                                .async {
                                    JavaIdentifiers.selectOrInsert(
                                            constructClassIdentity(
                                                    (instruction.cst as Type).descriptor
                                            )
                                    )
                                }
                                .await()

                        transaction {
                            JavaReference.new {
                                this.referenceSource = methodEntity
                                this.referenced = referencedEntity
                                this.instruction = opCodeCounter
                            }
                        }
                    }
                } else if (instruction is TypeInsnNode) {
                    val referencedEntity = withContext(concurrentDatabaseAccessDispatcher.coroutineContext) {
                        JavaIdentifiers.selectOrInsert(
                                constructClassIdentity(instruction.desc)
                        )
                    }

                    transaction {
                        JavaReference.new {
                            this.referenceSource = methodEntity
                            this.referenced = referencedEntity
                            this.instruction = opCodeCounter
                        }
                    }
                }
            }
        }
    }

    /**
     * Check, whether the superclass is available and if so, update the reference to it. Also insert a superclass
     * identifier, so at least the superclass can be named. Also, if the superclass gets available at a later import,
     * the references can be injected from the superclass processor in a later processing round.
     *
     * @param fileBlob the processed class file
     * @param concurrentDatabaseAccessDispatcher concurrent dispatcher for critical database accesses
     */
    private suspend fun updatePolymorphicRelations(fileBlob: FileBlob, concurrentDatabaseAccessDispatcher: CoroutineScope) {
        val (classNode, classEntity) = transaction {
            // reload class blob from DB and create a ClassNode from it
            val blob = FileBlob.reload(fileBlob)!!
            val classNode = ClassParser.tryParseClass(blob.data.getBytes(0, blob.data.length().toInt()))
                    ?: throw IllegalArgumentException("invalid java class file")

            val classEntity = JavaClass.find { JavaClasses.name eq classNode.name }.first()
            return@transaction classNode to classEntity
        }

        // update superclass
        if (classNode.superName != null) {
            val superIdentifier = concurrentDatabaseAccessDispatcher
                    .async {
                        JavaIdentifiers.selectOrInsert(constructClassIdentity(classNode.superName))
                    }
                    .await()

            transaction {
                // insert identifier reference on superclass
                classEntity.superIdentifier = superIdentifier

                // if superclass is a known entity, add reference
                val superClassEntity = JavaClass.find { JavaClasses.identifier eq superIdentifier.id }.firstOrNull()

                if (superClassEntity != null) {
                    classEntity.superClass = superClassEntity
                }
            }

            classNode.interfaces.forEach { itf ->
                val interfaceIdentifier = concurrentDatabaseAccessDispatcher
                        .async {
                            JavaIdentifiers.selectOrInsert(constructClassIdentity(itf))
                        }
                        .await()

                transaction {
                    val interfaceEntity = JavaClass
                            .find { JavaClasses.identifier eq interfaceIdentifier.id }
                            .firstOrNull()

                    JavaInterfaceRelation.new {
                        this.implementingClass = classEntity
                        this.interfaceIdentifier = interfaceIdentifier
                        this.interfaceEntity = interfaceEntity
                    }
                }
            }
        }
    }

    /**
     * If any class entities do exist, that have this entity as their superclass, but do not reference it, they are
     * of an older import and are already processed. Therefore those references get injected now. Furthermore,
     * methods of this class and of all sub-classes identified in the first step, are checked for overrides and
     * respective database entries are created.
     *
     * @param fileBlob the processed file
     */
    private fun insertMissingPolymorphicAndUpdateMethodOverrides(fileBlob: FileBlob) {
        transaction {
            // step 1: identify old pending polymorphic updates
            // reload class blob from DB and create a ClassNode from it
            val blob = FileBlob.reload(fileBlob)!!
            val classNode = ClassParser.tryParseClass(blob.data.getBytes(0, blob.data.length().toInt()))
                    ?: throw IllegalArgumentException("invalid java class file")

            val classEntity = JavaClass.find { JavaClasses.name eq classNode.name }.first()

            val subClasses = JavaClass.find {
                JavaClasses.superIdentifier eq classEntity.identifier.id and
                        JavaClasses.superClass.isNull()
            }

            subClasses.forEach {
                it.superClass = classEntity
            }

            val subInterfaces = JavaInterfaceRelation.find {
                JavaInterfaceRelations.interfaceIdentifier eq classEntity.identifier.id and
                        JavaInterfaceRelations.interfaceEntity.isNull()
            }

            subInterfaces.forEach {
                it.interfaceEntity = classEntity
            }

            // step 2: update Override entities of methods of this class and the late updated sub-classes
            classEntity.methods.forEach { method ->
                val firstLayer: MutableList<JavaClass> = classEntity.interfaces.toMutableList()
                if (classEntity.superClass != null)
                    firstLayer += classEntity.superClass!!

                identifyOverriddenMethods(
                        newPolymorphicParents = *firstLayer.toTypedArray(),
                        method = method)
                        .forEach { overriddenMethod ->
                            JavaOverride.new {
                                this.method = method
                                this.overrides = overriddenMethod
                            }
                        }
            }

            subClasses.forEach { cls ->
                cls.methods.forEach { subMethod ->
                    identifyOverriddenMethods(classEntity, method = subMethod).forEach { overriddenMethod ->
                        JavaOverride.new {
                            this.method = subMethod
                            this.overrides = overriddenMethod
                        }
                    }
                }
            }

            subInterfaces.forEach { itf ->
                itf.implementingClass.methods.forEach { subMethod ->
                    identifyOverriddenMethods(classEntity, method = subMethod).forEach { overriddenMethod ->
                        if (JavaOverride.find {
                                    JavaOverrides.method eq subMethod.id and (JavaOverrides.overrides eq overriddenMethod.id)
                                }.firstOrNull() == null) {
                            JavaOverride.new {
                                this.method = subMethod
                                this.overrides = overriddenMethod
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Identifies a list of methods that are directly overridden by the given method. This does not include methods,
     * that are already overridden by another method that is also overridden by the given method. This does however
     * include all methods that are overridden by the method at the same time (e.g. equivalent method declarations of
     * two interfaces implemented by the same class)
     *
     * @param newPolymorphicParents the first layer of classes, where potential overrides might have taken place
     * @param method the method of which the parents are searched
     *
     * @return a list of directly overridden methods
     */
    private fun identifyOverriddenMethods(vararg newPolymorphicParents: JavaClass,
                                          method: JavaMethod): List<JavaMethod> {
        return transaction {
            // build layer of override candidates. The layer is the first set of classes that the method could
            // potentially override at the same time
            var layer: MutableList<JavaClass> = mutableListOf(*newPolymorphicParents)

            while (layer.isNotEmpty()) {
                // check if this layer contains the method
                val overriddenMethods = layer.map { it.methods }
                        .flatten()
                        .filter { it.name == method.name && it.descriptor == method.descriptor }

                if (overriddenMethods.isNotEmpty())
                    return@transaction overriddenMethods

                // generate next layer out of the super classes to current layer and all implemented interfaces of
                // current layer
                val nextLayer = mutableListOf<JavaClass>()
                nextLayer += layer.map { it.superClass }.filterNotNull()
                nextLayer += layer.map { it.interfaces }.flatten()

                layer = nextLayer
            }

            return@transaction emptyList<JavaMethod>()
        }
    }
}
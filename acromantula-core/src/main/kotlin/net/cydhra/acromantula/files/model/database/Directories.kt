package net.cydhra.acromantula.files.model.database

import net.cydhra.acromantula.MAX_IDENTIFIER_LENGTH
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

object Directories : IntIdTable() {
    val name = varchar("name", MAX_IDENTIFIER_LENGTH)
    val parent = reference("parent", Directories, ReferenceOption.CASCADE).nullable().index()
    val archive = reference("archive", Archives, ReferenceOption.CASCADE)
}

class Directory(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Directory>(Directories)

    var name by Directories.name
    var parent by Directory optionalReferencedOn Directories.parent
    var archive by Archive referencedOn Directories.archive

    val subDirectories by Directory optionalReferrersOn Directories.parent
    val files by FileBlob optionalReferrersOn FileBlobs.parent
}
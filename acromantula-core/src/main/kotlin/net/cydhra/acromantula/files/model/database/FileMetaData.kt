package net.cydhra.acromantula.files.model.database

import net.cydhra.acromantula.MAX_IDENTIFIER_LENGTH
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

// common meta data values
const val META_DATA_KEY_ENCODING = "file.encoding"
const val META_DATA_KEY_TIME = "file.timestamp"

/**
 * A table for storing meta data for files. Any file can have unlimited meta data key-value pairs attached to it.
 *
 * @see [FileBlobs]
 */
object FileMetaData : IntIdTable() {
    val file = reference("file", FileBlobs, ReferenceOption.CASCADE).index()
    val key = varchar("key", 100)
    val data = varchar("data", MAX_IDENTIFIER_LENGTH)
}

/**
 * A key-value pair of metadata
 */
class FileMetaDataPair(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<FileMetaDataPair>(FileMetaData)

    var file by FileMetaData.file
    var key by FileMetaData.key
    var data by FileMetaData.data
}
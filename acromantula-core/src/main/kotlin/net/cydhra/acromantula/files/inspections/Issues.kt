package net.cydhra.acromantula.files.inspections

import net.cydhra.acromantula.workspace.sanitizing.IssueType


const val FIX_OPTION_ILLEGAL_SIGNATURE_DELETE = "Delete Faulty Signature"
const val FIX_OPTION_DISPERSE_EXCEPTION_HANDLERS = "Disperse Intersecting Exception Handlers"

/**
 * Issue with the signature. Raised when parsing a java class signatures fails
 */
object IllegalSignatureIssue : IssueType(
        name = "Illegal Signature",
        description = "A member has a signature not conforming to the signature specification. A signature is not " +
                "utilized by the JVM directly, it is only required by the compiler and for special reflection and " +
                "debugging purposes. Therefore, faulty signatures can be removed safely.",
        fixingOptions = *arrayOf(FIX_OPTION_ILLEGAL_SIGNATURE_DELETE)
)

object IntersectingExceptionHandlersIssue : IssueType(
        name = "Intersecting Exception Handlers",
        description = "The method has multiple exception handlers where at least two of them cross their respective " +
                "boundaries. This is legal in Bytecode, however cannot be represented in high level code. Dispersing " +
                "them will split the error handlers into chunks, so multiple distinct handlers coexist without " +
                "intersection.",
        fixingOptions = *arrayOf(FIX_OPTION_DISPERSE_EXCEPTION_HANDLERS)
)
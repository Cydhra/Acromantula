package net.cydhra.acromantula.files.types.action.disassembly

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.gui.controller.utility.MetaText
import net.cydhra.acromantula.workspace.AcromantulaTask
import net.cydhra.acromantula.workspace.importer.tools.ClassParser
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * Disassembly a java class file into readable bytecode assembler
 */
class DisassembleTask(private val fileBlob: FileBlob) : AcromantulaTask<MetaText>() {

    init {
        this.updateTitle("Generating disassembly of ${fileBlob.filename}")
    }

    override fun call(): MetaText {
        this.updateMessage("reloading class data...")
        val classNode = transaction {
            val blob = FileBlob.reload(fileBlob)!!
            ClassParser.tryParseClass(blob.data.getBytes(0, blob.data.length().toInt()))
        }

        @Suppress("FoldInitializerAndIfToElvis")
        if (classNode == null)
            throw IllegalArgumentException("Parsing failed")

        this.updateMessage("generating text...")
        return DisassemblyMetatextGenerator(fileBlob, classNode).create()
    }

}
package net.cydhra.acromantula.files.utility.model

import net.cydhra.acromantula.files.model.database.FileBlob

/**
 * References on in-code entities. They are used as gui model elements. Therefore [toString] must be overriden to
 * give a meaningful GUI description. The model must define an exact jump target to jump to the place in code that is
 * referenced.
 *
 * @param codeFileBlob the code file holding the referenced entity
 */
class CodeReference(val codeFileBlob: FileBlob, val jumpTarget: JumpTarget) {

    override fun toString(): String {
        codeFileBlob.filename + ": " + return jumpTarget.toString()
    }
}
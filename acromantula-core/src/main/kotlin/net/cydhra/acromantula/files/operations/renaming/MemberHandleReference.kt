package net.cydhra.acromantula.files.operations.renaming

import net.cydhra.acromantula.files.model.database.java.JavaClass
import net.cydhra.acromantula.files.model.database.java.JavaMethod

open class MemberHandleReference(val type: MemberHandleReferenceType, val referencingClass: JavaClass,
                                 val referencingMethod: JavaMethod?) {

    override fun toString(): String {
        return "$type in: ${referencingClass.name}" + if (referencingMethod != null) "::" + referencingMethod.name else ""
    }
}

enum class MemberHandleReferenceType {
    INVOCATION, INHERITANCE, DECLARATION, ACCESS, MEMBER
}
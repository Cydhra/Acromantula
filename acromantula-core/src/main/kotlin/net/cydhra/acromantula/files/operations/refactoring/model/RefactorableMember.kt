package net.cydhra.acromantula.files.operations.refactoring.model

import net.cydhra.acromantula.files.utility.model.CodeReference

/**
 *
 * @param T member type that can be refactored
 */
class RefactorableMember<T : Any>(codeReference: CodeReference, val member: T) : RefactorableEntity(codeReference)
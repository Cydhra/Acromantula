package net.cydhra.acromantula.files.operations.searching

import javafx.collections.FXCollections
import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.utility.model.*
import org.jetbrains.exposed.sql.transactions.transaction

object SearchClassReferencesOperation : SearchReferencesOperation<JavaClass>() {
    override val name: String = "Search Class References"

    override fun obtainEntityFromDatabase(entityIdentification: String): JavaClass {
        return transaction {
            val identifierId = JavaIdentifier.find { JavaIdentifiers.identifier eq entityIdentification }.single().id
            JavaClass.find { JavaClasses.identifier eq identifierId }.first()
        }
    }

    override fun collectReferences(entity: JavaClass): List<CodeReference> {
        val referenceList = FXCollections.observableArrayList(mutableListOf<CodeReference>())

        transaction {
            // the file itself is also a reference
            referenceList += CodeReference(entity.file, FileJumpTarget(entity.file.id, entity.file.filename))
            //  save class declaration
            referenceList += CodeReference(entity.file, ClassDeclarationJumpTarget(entity.identifier.identifier))

            //  for each method of renamed class:
            JavaMethod
                    .find { JavaMethods.owner eq entity.id }
                    .forEach { method ->
                        referenceList += CodeReference(entity.file,
                                MethodDeclarationJumpTarget(method.identifier.identifier))
                        referenceList += JavaReference.find { JavaReferences.referenced eq method.identifier.id }
                                .map {
                                    CodeReference(it.referenceSource.owner.file,
                                            InstructionJumpTarget(it.referenceSource.identifier.identifier, it.instruction))
                                }
                    }

            //  for each field of renamed class:
            JavaField
                    .find { JavaFields.owner eq entity.id }
                    .forEach { field ->
                        referenceList += CodeReference(entity.file,
                                FieldDeclarationJumpTarget(entity.identifier.identifier))
                        referenceList += JavaReference.find { JavaReferences.referenced eq field.identifier.id }
                                .map {
                                    CodeReference(it.referenceSource.owner.file,
                                            InstructionJumpTarget(it.referenceSource.identifier.identifier, it.instruction))
                                }
                    }

            //  for each reference of class
            JavaReference
                    .find { JavaReferences.referenced eq entity.identifier.id }
                    .forEach { ref ->
                        referenceList += CodeReference(ref.referenceSource.owner.file,
                                InstructionJumpTarget(ref.referenceSource.identifier.identifier, ref.instruction))
                    }

            // for each field of type of this class
            // TODO
            // for each method with this class in descriptor
            // TODO
        }

        return referenceList
    }

}
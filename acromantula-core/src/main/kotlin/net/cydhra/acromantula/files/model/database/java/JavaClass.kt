package net.cydhra.acromantula.files.model.database.java

import net.cydhra.acromantula.MAX_IDENTIFIER_LENGTH
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.FileBlobs
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * A table containing the classes of java archives.
 */
object JavaClasses : IntIdTable() {
    val identifier = reference("identifier", JavaIdentifiers, ReferenceOption.RESTRICT).index()
    val name = varchar("name", MAX_IDENTIFIER_LENGTH)
    val superIdentifier = reference("super_identifier", JavaIdentifiers).nullable()
    val superClass = reference("super", JavaClasses).nullable()
    val file = reference("file", FileBlobs)
}

/**
 * A database entity representing a java class.
 */
class JavaClass(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<JavaClass>(JavaClasses)

    /**
     * The unique class identifier
     */
    var identifier by JavaIdentifier referencedOn JavaClasses.identifier

    /**
     * The class name
     */
    var name by JavaClasses.name

    /**
     * Identifier of superclass. This is always set, except for the Object class, even if no [superClass] is set.
     */
    var superIdentifier by JavaIdentifier optionalReferencedOn JavaClasses.superIdentifier

    /**
     * Optional super class entity.
     */
    var superClass by JavaClass optionalReferencedOn JavaClasses.superClass

    /**
     * The file this class is located in
     */
    var file by FileBlob referencedOn JavaClasses.file

    /**
     * Interfaces implemented by this class
     */
    val interfaces: List<JavaClass>
        get() = transaction {
            JavaInterfaceRelation
                    .find { JavaInterfaceRelations.implementingClass eq this@JavaClass.id }
                    .mapNotNull(JavaInterfaceRelation::interfaceEntity)
        }

    /**
     * all [fields][JavaField] of this class
     */
    val fields by JavaField referrersOn JavaFields.owner

    /**
     * all [methods][JavaMethod] of this class
     */
    val methods by JavaMethod referrersOn JavaMethods.owner
}
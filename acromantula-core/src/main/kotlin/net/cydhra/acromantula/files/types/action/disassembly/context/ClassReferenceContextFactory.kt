package net.cydhra.acromantula.files.types.action.disassembly.context

import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory
import net.cydhra.acromantula.gui.controller.context.constructActionArray
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.java.JavaClass
import net.cydhra.acromantula.files.model.database.java.JavaClasses
import net.cydhra.acromantula.files.model.database.java.JavaIdentifier
import net.cydhra.acromantula.files.model.database.java.JavaIdentifiers
import net.cydhra.acromantula.files.operations.refactoring.rename.ClassNameRefactorOperation
import org.jetbrains.exposed.sql.transactions.transaction

class ClassReferenceContextFactory(file: FileBlob, classIdentifier: String) :
        ContextMenuFactory<String>(
                file,
                classIdentifier,
                *constructActionArray {
                    ifTrue(
                            {
                                transaction {
                                    val id = JavaIdentifier
                                            .find { JavaIdentifiers.identifier eq classIdentifier }
                                            .firstOrNull() ?: return@transaction false

                                    !JavaClass.find { JavaClasses.identifier eq id.id }.empty()
                                }
                            }
                            , ClassNameRefactorOperation)
                }

        )
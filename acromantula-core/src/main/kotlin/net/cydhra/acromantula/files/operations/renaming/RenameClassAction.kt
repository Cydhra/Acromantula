package net.cydhra.acromantula.files.operations.renaming

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import net.cydhra.acromantula.concurrency.tasks.ContentOperationTask
import net.cydhra.acromantula.files.model.database.java.*
import net.cydhra.acromantula.files.operations.refactoring.remapper.ClassNameRemapper
import net.cydhra.acromantula.files.utility.constructClassIdentity
import net.cydhra.acromantula.files.utility.constructFieldIdentity
import net.cydhra.acromantula.files.utility.constructMethodIdentity
import net.cydhra.acromantula.workspace.SynchronizationHelper
import net.cydhra.acromantula.workspace.importer.tools.ClassParser
import org.jetbrains.exposed.sql.transactions.transaction
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.tree.ClassNode

class RenameClassAction : RenameAction<ClassNode>("Rename Class") {

    override val renameDialogTitle: String = "Rename Class: "

    /**
     * The class database entity that is to be renamed
     */
    private var classEntity: JavaClass? = null

    /**
     * The class node used during renaming
     */
    private var classNode: ClassNode? = null

    override fun getEntity(identifier: String): ClassNode {
        if (classEntity == null) {
            transaction {
                val identifierId = JavaIdentifier.find { JavaIdentifiers.identifier eq identifier }.single().id
                classEntity = JavaClass.find { JavaClasses.identifier eq identifierId }.single()

                classNode = ClassParser.tryParseClass(classEntity!!.file.data.getBytes(1L,
                        classEntity!!.file.data.length().toInt()))!!
            }
        }

        return classNode!!
    }

    override fun getEntityName(identifier: String): String {
        return with(getEntity(identifier)) {
            name.removeRange(0, name.lastIndexOf('/') + 1)
        }
    }

    override fun generateReferenceList(entity: String): ObservableList<MemberHandleReference> {
        val referenceList = FXCollections.observableArrayList(mutableListOf<MemberHandleReference>())

        transaction {
            //  save class declaration                  # identifier, internalname and file have to be renamed
            referenceList += MemberHandleReference(MemberHandleReferenceType.DECLARATION, classEntity!!, null)

            //  for each method of renamed class:
            //      save method as member-declaration   # identifier has to be renamed
            //      find references to method           # have to be in-class-remapped
            JavaMethod
                    .find { JavaMethods.owner eq classEntity!!.id }
                    .forEach { method ->
                        referenceList += MemberHandleReference(MemberHandleReferenceType.MEMBER, method.owner, method)
                        referenceList += JavaReference.find { JavaReferences.referenced eq method.identifier.id }
                                .map {
                                    MemberHandleReference(MemberHandleReferenceType.ACCESS,
                                            it.referenceSource.owner, it.referenceSource)
                                }
                    }

            //  for each field of renamed class:
            //      save as member-declaration          # identifier has to be renamed
            //      find references to field            # have to be in-class-remapped
            JavaField
                    .find { JavaFields.owner eq classEntity!!.id }
                    .forEach { field ->
                        referenceList += MemberHandleReference(MemberHandleReferenceType.MEMBER, field.owner, null)
                        referenceList += JavaReference.find { JavaReferences.referenced eq field.identifier.id }
                                .map {
                                    MemberHandleReference(MemberHandleReferenceType.ACCESS,
                                            it.referenceSource.owner, it.referenceSource)
                                }
                    }

            //  for each reference of class
            //      save reference                      # has to be in-class-remapped
            JavaReference
                    .find { JavaReferences.referenced eq classEntity!!.identifier.id }
                    .forEach { ref ->
                        referenceList += MemberHandleReference(MemberHandleReferenceType.ACCESS,
                                ref.referenceSource.owner, ref.referenceSource)
                    }

            // for each field of type of this class
            JavaField
                    .find { JavaFields.owner eq classEntity!!.id }
                    .forEach { field ->
                        // TODO: member must be in-class-remapped and identifier must be changed (descriptor)
                    }
        }

        return referenceList
    }

    override fun generateContentOperationTask(entity: String, referenceList: List<MemberHandleReference>,
                                              affectedClassNodes: List<Pair<JavaClass, ClassNode>>, oldName: String,
                                              newName: String): ContentOperationTask<Unit> {
        return ContentOperationTask(
                title = "Rename Class: ${getEntity(entity).name}",
                msg = "Remapping...",
                operation = {
                    transaction {
                        // update all classes referencing the class
                        affectedClassNodes.forEach { (reference, classNode) ->
                            // prepare a writer for the refactored class
                            val writer = ClassWriter(0)

                            val namePrefix = classNode.name
                                    .removeRange(classNode.name.lastIndexOf('/') + 1, classNode.name.length)
                            // remap the class and write the result into the writer
                            classNode.accept(ClassRemapper(writer,
                                    ClassNameRemapper(oldName = namePrefix + oldName, newName = namePrefix + newName)))

                            // update database content
                            reference.file.data = connection.createBlob().apply { setBytes(1, writer.toByteArray()) }
                            SynchronizationHelper.reportFileChange(reference.file)
                        }

                        // update class entity in database
                        classEntity!!.name = newName
                        classEntity!!.identifier.identifier = constructClassIdentity(newName)
                        classEntity!!.file.filename = "$newName.class"

                        // update member methods
                        classEntity!!.methods.forEach { method ->
                            method.identifier.identifier = constructMethodIdentity(
                                    ownerIdentity = classEntity!!.identifier.identifier,
                                    methodName = method.name,
                                    methodDescriptor = method.descriptor
                            )
                        }

                        // update member fields
                        classEntity!!.fields.forEach { field ->
                            field.identifier.identifier = constructFieldIdentity(
                                    ownerIdentity = classEntity!!.identifier.identifier,
                                    fieldName = field.name,
                                    fieldDescriptor = field.descriptor
                            )
                        }
                    }
                })
    }
}
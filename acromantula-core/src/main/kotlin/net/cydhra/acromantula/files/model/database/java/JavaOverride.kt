package net.cydhra.acromantula.files.model.database.java

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

object JavaOverrides : IntIdTable() {
    val method = reference("method", JavaMethods, ReferenceOption.CASCADE).index()

    /**
     * The overridden method
     */
    val overrides = reference("overrides", JavaMethods, ReferenceOption.CASCADE).index()
}

class JavaOverride(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<JavaOverride>(JavaOverrides)

    var method by JavaMethod referencedOn JavaOverrides.method

    /**
     * The overridden method entity
     */
    var overrides by JavaMethod referencedOn JavaOverrides.overrides
}
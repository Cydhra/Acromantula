package net.cydhra.acromantula.files

import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.collections.transformation.SortedList
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.global.AddFilesEvent
import net.cydhra.acromantula.bus.event.local.ConnectToWorkspaceRequest
import net.cydhra.acromantula.bus.service.Service
import net.cydhra.acromantula.concurrency.ThreadPool
import net.cydhra.acromantula.concurrency.tasks.FileProcessingTask
import net.cydhra.acromantula.concurrency.tasks.LoadWorkspaceTask
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.defaultWorkspaceEntryComparator
import org.apache.logging.log4j.LogManager

object FileHandlerService : Service {

    override val name: String = "file-handler"

    private val logger = LogManager.getLogger()

    /**
     * The list of loaded top-level [WorkspaceEntries][WorkspaceEntry]
     */
    private val loadedItems: ObservableList<WorkspaceEntry> = FXCollections.observableArrayList()

    /**
     * A sorted view on the list of all top-level [WorkspaceEntries][WorkspaceEntry]. This is supposed to be touched
     * by other modules creating a direct dependency.
     */
    val sortedLoadedItems = SortedList(loadedItems, defaultWorkspaceEntryComparator)

    override suspend fun initialize() {
        EventBroker.registerEventListener(this, AddFilesEvent::class, this::onFilesAdded)
        EventBroker.registerEventListener(this, ConnectToWorkspaceRequest::class, this::onConnectToWorkspaceRequest)

        FileTypeRegistry.initialize()
    }

    /**
     * Load the files that already imported into the current workspace. It is assumed that this method has not been
     * called before on the same workspace, as that would produce unpredictable results
     */
    fun loadWorkspace() {
        // load workspace contents
        ThreadPool.submit(LoadWorkspaceTask()) {
            onSuccess { list ->
                logger.info("loaded ${list.size} workspace file trees from database")
                for (entry in list) {
                    EventBroker.fireEvent(AddFilesEvent(entry, false))
                }
            }
            onFailure { t ->
                logger.error("failed to load workspace contents", t)
            }
            onCancelled {
                TODO("cancelling worspace loading must be adressed")
            }
        }
    }

    /**
     * Gets called whenever new files are added into the workspace
     */
    private suspend fun onFilesAdded(event: AddFilesEvent) {
        if (event.importedLocally) {
            processFiles(event.entry)
        }

        Platform.runLater {
            loadedItems.add(event.entry)
        }
    }

    /**
     * If a new workspace is opened, clear the previous item list.
     */
    private suspend fun onConnectToWorkspaceRequest(event: ConnectToWorkspaceRequest) {
        event.onFulfill {
            this@FileHandlerService.loadedItems.clear()
        }
    }

    /**
     * Schedule file processing for newly imported files
     */
    private fun processFiles(entry: WorkspaceEntry) {
        ThreadPool.submit(FileProcessingTask(entry)) {
            onFailure { t -> logger.error("error during file processing of ${entry.displayName}", t) }
            onCancelled { logger.warn("user cancelled file processing of ${entry.displayName}") }
        }
    }
}
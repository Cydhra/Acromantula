package net.cydhra.acromantula.gui.controller

import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.TextArea
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import net.cydhra.acromantula.gui.controller.utility.StageDialog
import net.cydhra.acromantula.workspace.sanitizing.InspectionIssue
import net.cydhra.acromantula.workspace.sanitizing.IssueType
import tornadofx.View
import tornadofx.button

class ResolveIssuesDialogController(val issues: List<InspectionIssue<Any>>) : View(), StageDialog {
    override val root: Parent by fxml("/view/dialog/issues_fixing_wrapper.fxml")

    /**
     * Pane for all dialog resolving buttons
     */
    private val resolvePane: Pane by fxid()

    /**
     * A tree view displaying the issues
     */
    private val issueTreeView: TreeView<IssueTreeItem> by fxid()

    /**
     * A textarea that displays a description to the selected item from the issue tree
     */
    private val descriptionTextArea: TextArea by fxid()

    /**
     * A pane that shall hold buttons used for resolving an issue
     */
    private val issueResolvePane: HBox by fxid()

    /**
     * The invisible tree root item
     */
    private val rootItem = TreeItem<IssueTreeItem>(IssueTreeItem("issues", false, null))

    init {
        issueTreeView.root = rootItem

        var currentType: IssueType? = null
        var currentItem: TreeItem<IssueTreeItem>? = null

        // fill issue tree with issues
        for (inspectionIssue in issues.sortedBy { it.type.name }) {
            if (inspectionIssue.type != currentType) {
                currentType = inspectionIssue.type
                currentItem = TreeItem(IssueTreeItem(inspectionIssue.type.name, true, inspectionIssue))
                currentItem.isExpanded = true
                rootItem.children.add(currentItem)
            }

            currentItem!!.children.add(TreeItem(IssueTreeItem(inspectionIssue.instanceDescription, false,
                    inspectionIssue)))
        }

        // add selection listener to issue tree
        issueTreeView.selectionModel.selectedItemProperty().addListener { _, _, issueTreeItem ->
            if (issueTreeItem.value.isTypeItem) {
                descriptionTextArea.text = issueTreeItem.value.inspectionIssue!!.type.description
                issueResolvePane.children.clear()

                // offer issue resolving buttons for the whole issue type group
                issueTreeItem.value.inspectionIssue!!.proposedFixes.forEach { (name, _) ->
                    issueResolvePane.add(button {
                        this.text = "$name (All)"
                        this.onAction = EventHandler {
                            val parentItem = issueTreeItem
                            parentItem.children.forEach { childItem ->
                                childItem.value.inspectionIssue!!.proposedFixes[name]!!
                                        .invoke(childItem.value.inspectionIssue!!.affectedEntity!!)
                            }

                            // remove this issue from the tree
                            val currentSelectedIndex = issueTreeView.selectionModel.selectedIndex
                            rootItem.children.remove(parentItem)

                            // select next available entry
                            issueTreeView.selectionModel.select(currentSelectedIndex)
                        }
                    })
                }
            } else {
                descriptionTextArea.text = issueTreeItem.value.inspectionIssue!!.type.description + "\n\n" +
                        issueTreeItem.value.inspectionIssue!!.instanceDescription
                issueResolvePane.children.clear()

                issueTreeItem.value.inspectionIssue!!.proposedFixes.forEach { (name, callback) ->
                    issueResolvePane.add(button {
                        this.text = name
                        this.onAction = EventHandler {
                            // call the selected solve callback
                            callback(issueTreeItem.value.inspectionIssue!!.affectedEntity!!)

                            // remove this issue from the tree
                            val nodeItem = issueTreeItem.parent
                            val currentSelectedIndex = issueTreeView.selectionModel.selectedIndex

                            nodeItem.children.remove(issueTreeItem)
                            if (nodeItem.children.isEmpty()) {
                                rootItem.children.remove(nodeItem)
                            }

                            // select next available entry
                            issueTreeView.selectionModel.select(currentSelectedIndex)
                        }
                    })
                }
            }
        }
    }

    /**
     * @return whether issues remain in the tree
     */
    fun hasMoreIssues(): Boolean {
        return rootItem.children.isNotEmpty()
    }

    override fun addButton(button: Button) {
        resolvePane.add(button)
    }

    /**
     * View model class for the tree view to hold the issues
     *
     * @param name name of the item to display
     * @param isTypeItem whether this is a node item with child nodes
     * @param inspectionIssue the issue this item displays. Only allowed to be null for node items, not for issue items
     */
    private class IssueTreeItem(val name: String, val isTypeItem: Boolean,
                                val inspectionIssue: InspectionIssue<in Any>?) {
        override fun toString(): String {
            return this.name
        }
    }
}
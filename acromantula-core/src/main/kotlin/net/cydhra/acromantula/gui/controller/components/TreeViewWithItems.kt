package net.cydhra.acromantula.gui.controller.components

import javafx.beans.InvalidationListener
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.collections.WeakListChangeListener
import javafx.event.EventHandler
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import net.cydhra.acromantula.AcromantulaApp
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.DefaultFileActionRequest
import net.cydhra.acromantula.files.model.HierarchyData
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory
import net.cydhra.acromantula.gui.controller.context.FileTreeItemContextFactory
import tornadofx.onDoubleClick
import java.util.*

/**
 * This class extends the [TreeView] to use items as a data source. This allows you to treat a [TreeView] in a
 * similar way as a [javafx.scene.control.ListView] or [javafx.scene.control.TableView]. Each item in the list must
 * implement the [HierarchyData] interface, in order to map the recursive nature of the tree data to the tree view.
 * Each change in the underlying data (adding, removing, sorting) will then be automatically reflected in the UI.
 *
 * @param rootItem the root item of the tree.
 *
 * @author Christian Schudt
 * @author Cydhra
 */
class TreeViewWithItems(rootItem: WorkspaceEntry)
    : TreeView<WorkspaceEntry>(TreeItem(rootItem, ImageView(getImage(rootItem.imageLocation)))) {

    companion object {
        private val imageStore = mutableMapOf<String, Image>()

        private fun getImage(imageLocation: String): Image {
            if (imageStore.containsKey(imageLocation)) {
                return imageStore[imageLocation]!!
            }

            val img = Image(AcromantulaApp::class.java.getResourceAsStream(imageLocation), 20.0,
                    20.0, false, false)
            imageStore[imageLocation] = img
            return img
        }
    }

    /**
     * Keep hard references for each listener, so that they don't get garbage collected too soon.
     */
    private val hardReferences = HashMap<TreeItem<WorkspaceEntry>, ListChangeListener<WorkspaceEntry>>()

    /**
     * Also store a reference from each tree item to its weak listeners, so that the listener can be removed, when the tree item gets removed.
     */
    private val weakListeners = HashMap<TreeItem<WorkspaceEntry>, WeakListChangeListener<WorkspaceEntry>>()

    /**
     * A map of context menus assigned to the different tree items
     */
    private val contextMenus = HashMap<TreeItem<WorkspaceEntry>, ContextMenuFactory<FileEntry>>()

    private val items = SimpleObjectProperty<ObservableList<out WorkspaceEntry>>(this, "items")

    init {
        init()
    }

    /**
     * Initializes the tree view.
     */
    private fun init() {
        rootProperty().addListener { _, oldRoot, _ ->
            clear(oldRoot)
            updateItems()
        }

        setItems(FXCollections.observableArrayList())

        // Do not use ChangeListener, because it won't trigger if old list equals new list (but in fact different references).
        items.addListener(InvalidationListener {
            clear(root)
            updateItems()
        })

        // add context menu handler for items
        this.onContextMenuRequested = EventHandler { e ->
            this.contextMenu?.hide()
            this.contextMenu = this.contextMenus[this.selectionModel.selectedItem]?.create()
            this.contextMenu?.show(this@TreeViewWithItems, e.screenX, e.screenY)
            e.consume()
        }

        this.onDoubleClick {
            val clickedItem = this.selectionModel.selectedItem
            if (clickedItem != null) {
                val entry = clickedItem.value
                if (entry is FileEntry) {
                    EventBroker.fireEvent(DefaultFileActionRequest(entry, null))
                }
            }
        }
    }

    /**
     * Removes all listener from a root.
     *
     * @param root The root.
     */
    private fun clear(root: TreeItem<WorkspaceEntry>?) {
        if (root != null) {
            for (treeItem in root.children) {
                removeRecursively(treeItem)
            }

            removeRecursively(root)
            root.children.clear()
        }
    }

    /**
     * Updates the items.
     */
    private fun updateItems() {

        if (getItems() != null) {
            for (value in getItems()!!) {
                root.children.add(addRecursively(value))
            }

            val rootListener = getListChangeListener(root.children)
            val weakListChangeListener = WeakListChangeListener(rootListener)
            hardReferences[root] = rootListener
            weakListeners[root] = weakListChangeListener
            getItems()!!.addListener(weakListChangeListener)
        }
    }

    /**
     * Gets a [javafx.collections.ListChangeListener] for a  [TreeItem]. It listens to changes on the underlying list and updates the UI accordingly.
     *
     * @param treeItemChildren The associated tree item's children list.
     * @return The listener.
     */
    private fun getListChangeListener(treeItemChildren: ObservableList<TreeItem<WorkspaceEntry>>): ListChangeListener<WorkspaceEntry> {
        return ListChangeListener { change ->
            while (change.next()) {
                if (change.wasUpdated()) {
                    // http://javafx-jira.kenai.com/browse/RT-23434
                    continue
                }
                if (change.wasRemoved()) {
                    for (i in change.removedSize - 1 downTo 0) {
                        removeRecursively(treeItemChildren.removeAt(change.from + i))
                    }
                }
                // If items have been added
                if (change.wasAdded()) {
                    // Get the new items
                    for (i in change.from until change.to) {
                        treeItemChildren.add(i, addRecursively(change.list[i]))
                    }
                }
                // If the list was sorted.
                if (change.wasPermutated()) {
                    // Store the new order.
                    val tempMap = HashMap<Int, TreeItem<WorkspaceEntry>>()

                    for (i in change.to - 1 downTo change.from) {
                        val a = change.getPermutation(i)
                        tempMap[a] = treeItemChildren.removeAt(i)
                    }

                    selectionModel.clearSelection()

                    // Add the items in the new order.
                    for (i in change.from until change.to) {
                        treeItemChildren.add(tempMap.remove(i))
                    }
                }
            }
        }
    }

    /**
     * Removes the listener recursively.
     *
     * @param item The tree item.
     */
    private fun removeRecursively(item: TreeItem<WorkspaceEntry>): TreeItem<WorkspaceEntry> {
        if (weakListeners.containsKey(item)) {
            item.value.children.removeListener(weakListeners.remove(item))
            hardReferences.remove(item)
        }
        if (contextMenus.containsKey(item)) {
            contextMenus.remove(item)
        }
        for (treeItem in item.children) {
            removeRecursively(treeItem)
        }
        return item
    }

    /**
     * Adds the children to the tree recursively.
     *
     * @param value The initial value.
     * @return The tree item.
     */
    private fun addRecursively(value: WorkspaceEntry): TreeItem<WorkspaceEntry> {
        // TODO construct generic type bound where WorkspaceEntry can be assumed
        val treeItem = TreeItem(value, ImageView(getImage((value).imageLocation)))

        if (value is FileEntry) {
            val menu = FileTreeItemContextFactory(value.fileBlob, value)
            contextMenus[treeItem] = menu
        }

        val listChangeListener = getListChangeListener(treeItem.children)
        val weakListener = WeakListChangeListener(listChangeListener)
        value.children.addListener(weakListener)

        hardReferences[treeItem] = listChangeListener
        weakListeners[treeItem] = weakListener
        for (child in value.children) {
            treeItem.children.add(addRecursively(child))
        }
        return treeItem
    }

    fun getItems(): ObservableList<out WorkspaceEntry>? {
        return items.get()
    }

    /**
     * Sets items for the tree.
     *
     * @param items The list.
     */
    fun setItems(items: ObservableList<out WorkspaceEntry>) {
        this.items.set(items)
    }
}
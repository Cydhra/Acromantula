package net.cydhra.acromantula.gui.controller.utility

import javafx.scene.Node
import net.cydhra.acromantula.files.utility.model.JumpTarget
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory

fun metaText(body: MetaTextWriter.() -> Unit): MetaText {
    return MetaTextWriter().apply(body).metaText
}

/**
 * A writer interface that can be used to write to [MetaText] instance conveniently.
 */
class MetaTextWriter {

    /**
     * The [MetaText] instance where all written results will go
     */
    val metaText = MetaText()

    /**
     * The current level of indentation
     */
    var currentIndentationLevel: Int = 0

    /**
     * Whether the next write operation has to be preceded by indentation
     */
    var isIndentationRequired: Boolean = false

    /**
     * The token used to increase the indentation by one level.
     */
    val indentationToken: String = "    "

    /**
     * A counter keeping track of the current paragraph index. Required to properly set paragraph style/meta information
     */
    private var paragraphCounter = 0;

    /**
     * A list of jump targets within the current paragraph that are used as part of the paragraph style to know where
     * to jump later in GUI
     */
    private var currentParagraphJumpTargets = mutableListOf<JumpTarget>()

    /**
     * Add a [MetaTextComponent] with a chunk of text and configuration. Will indent the line if required by the [currentIndentationLevel]
     *
     * @param text the chunk of text that shall be appended
     * @param onClick a callback function invoked on clicking the text
     * @param contextFactory
     * @param styleClasses some CSS style classes that shall be applied onto the text chunk
     */
    fun write(text: String, onClick: ((Node) -> Unit)? = null, contextFactory: ContextMenuFactory<*>? = null,
              paragraphJumpTarget: JumpTarget? = null, vararg styleClasses: String): MetaTextAppenderContext {
        // if indentation is required, append the indentation token depending on the indent level
        if (this.isIndentationRequired) {
            this.isIndentationRequired = false

            with(StringBuilder(indentationToken.length * currentIndentationLevel)) {
                for (i in (1..currentIndentationLevel)) {
                    this.append(indentationToken)
                }

                metaText.append(this.toString())
            }
        }

        if (text.contains("\n")) {
            throw IllegalArgumentException("no explicit newline symbols in metatext allowed. Use writeline() instead.")
        }

        metaText.append(text, onClick, contextFactory, *styleClasses)
        if (paragraphJumpTarget != null)
            currentParagraphJumpTargets.add(paragraphJumpTarget)

        return MetaTextAppenderContext(this)
    }

    private fun beginNewParagraph() {
        isIndentationRequired = true
        metaText.appendParagraphJumpTargets(paragraphCounter++, currentParagraphJumpTargets)
        currentParagraphJumpTargets.clear()
        metaText.append("\n")
    }

    fun newline() {
        beginNewParagraph()
    }


    /**
     * Add a chunk of text to the [MetaText] appending a newline symbol at its end. Will indent the line if required by [currentIndentationLevel]
     *
     * @param text the chunk of text where a newline is appended
     * @param onClick a callback function invoked on clicking the text
     * @param contextFactory
     * @param styleClasses an array of style classes to be applied to the chunk of text
     */
    fun writeLine(text: String, onClick: ((Node) -> Unit)? = null, contextFactory: ContextMenuFactory<Any>? = null,
                  paragraphJumpTarget: JumpTarget? = null, vararg styleClasses: String) {
        this.write(text, onClick, contextFactory, paragraphJumpTarget, *styleClasses)
        newline()
    }

    /**
     * DSL method to add a block of text into the built [MetaText] instance. The block consists of a header, some brackets and a body, that
     * is itself described through a builder method applied to this [MetaTextWriter] instance.
     *
     * @param text block header text
     * @param onClick block header click handler
     * @param contextFactory
     * @param enclosingStyle style of the used brackets
     * @param styleClasses block header style classes
     * @param body block content builder method
     */
    fun block(text: String, onClick: ((Node) -> Unit)? = null, contextFactory: ContextMenuFactory<*>? = null,
              enclosingStyle: EnclosingStyle = EnclosingStyle.BRACES, paragraphJumpTarget: JumpTarget? = null,
              vararg styleClasses: String, body: MetaTextWriter.() -> Unit) {
        write(text, onClick, contextFactory, paragraphJumpTarget, *styleClasses)
        writeLine(" " + (enclosingStyle.openingCharacter?.toString() ?: ""))
        indent()
        this.apply(body)
        unindent()
        writeLine(enclosingStyle.closingCharacter?.toString() ?: "")
    }

    /**
     * Increase the level of indentation
     */
    fun indent() {
        this.currentIndentationLevel += 1
    }

    /**
     * Decrease the level of indentation but not beneath zero
     */
    fun unindent() {
        this.currentIndentationLevel = Math.max(this.currentIndentationLevel - 1, 0)
    }
}

/**
 * An instance of this class can be returned by DSL methods of [MetaTextWriter] to allow quick appending of pure text. Instead of another
 * call to [MetaTextWriter.write] a simple plus operator and a string is sufficient.
 *
 * @param writer the [MetaTextWriter] that accepts the appendix
 *
 * @see plus
 */
class MetaTextAppenderContext(private val writer: MetaTextWriter) {
    /**
     * Append a simple style-less text to a [MetaText].
     *
     * @param text any string to be appended
     *
     * @return the same [MetaTextAppenderContext]
     */
    operator fun plus(text: String): MetaTextAppenderContext {
        writer.write(text)
        return this
    }
}

/**
 * Style of an enclosed block. Determines the brackets used for enclosing
 */
enum class EnclosingStyle(val openingCharacter: Char?, val closingCharacter: Char?) {
    NONE(null, null), PARENTHESES('(', ')'), BRACKETS('[', ']'), BRACES('{', '}')
}
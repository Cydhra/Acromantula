package net.cydhra.acromantula.gui.controller

import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.CheckBoxTreeItem
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.control.cell.CheckBoxTreeCell
import javafx.scene.layout.HBox
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.gui.controller.utility.StageDialog
import net.cydhra.acromantula.workspace.sanitizing.Sanitizer
import net.cydhra.acromantula.workspace.sanitizing.SanitizerInspection
import tornadofx.View
import tornadofx.multiSelect


class RunInspectionsDialogController(workspaceRoot: TreeItem<WorkspaceEntry>) : View(), StageDialog {
    override val root: Parent by fxml("/view/dialog/run_inspections_wrapper.fxml")

    /**
     * A pane holding all the buttons that can resolve this dialog
     */
    private val resolvePane: HBox by fxid()

    /**
     * A tree view displaying the workspace contents
     */
    private val tvFiles: TreeView<WorkspaceEntry> by fxid()

    /**
     * A tree view displaying available inspections
     */
    private val tvInspections: TreeView<String> by fxid()

    init {
        tvFiles.multiSelect(true)
        tvInspections.multiSelect(true)

        tvFiles.setCellFactory {
            object : CheckBoxTreeCell<WorkspaceEntry>() {

                override fun updateItem(item: WorkspaceEntry?, empty: Boolean) {
                    super.updateItem(item, empty)
                    if (item != null) {
                        text = item.displayName
                    }
                }
            }
        }

        tvFiles.root = generateCheckboxWorkspace(workspaceRoot)

        tvInspections.root = CheckBoxTreeItem("All Inspections").apply { isExpanded = true }
        tvInspections.setCellFactory {
            object : CheckBoxTreeCell<String>() {

                override fun updateItem(item: String?, empty: Boolean) {
                    super.updateItem(item, empty)
                    if (item != null) {
                        text = item
                    }
                }
            }
        }

        Sanitizer.groupedInspections.forEach { (group, inspections) ->
            tvInspections.root.children += CheckBoxTreeItem<String>(group).apply {
                isExpanded = true
                children += inspections.map(SanitizerInspection<*>::name).map(::CheckBoxTreeItem)
            }
        }
    }

    override fun addButton(button: Button) {
        resolvePane.add(button)
    }

    /**
     * @return a list of [FileEntries][FileEntry] that are selected in the dialog
     */
    fun getSelectedWorkspaceFiles(): List<FileEntry> {
        return findCheckedItems(tvFiles.root as CheckBoxTreeItem<WorkspaceEntry>)
                .map(TreeItem<WorkspaceEntry>::getValue)
                .filterIsInstance<FileEntry>()
    }

    /**
     * @return a list of names of selected inspections
     */
    fun getSelectedInspectionNames(): List<String> {
        return findCheckedItems(tvInspections.root as CheckBoxTreeItem<String>)
                .filter { it.children.isEmpty() }
                .map(TreeItem<String>::getValue)
    }

    /**
     * Generate a [CheckBoxTreeItem] tree from the given root.
     *
     * @param root the [TreeItem] with all its children that shall be mapped into a tree of [CheckBoxTreeItem]s
     *
     * @return a [CheckBoxTreeItem] that holds the same value as [root]
     */
    private fun generateCheckboxWorkspace(root: TreeItem<WorkspaceEntry>): CheckBoxTreeItem<WorkspaceEntry> {
        return CheckBoxTreeItem(root.value).apply {
            root.children.forEach { child ->
                children.add(generateCheckboxWorkspace(child))
            }
        }
    }

    private fun <E> findCheckedItems(root: CheckBoxTreeItem<E>): List<CheckBoxTreeItem<E>> {
        val list = mutableListOf<CheckBoxTreeItem<E>>()

        if (root.isSelected)
            list += root

        list += root.children.map { child -> findCheckedItems(child as CheckBoxTreeItem<E>) }.flatten()

        return list
    }
}

package net.cydhra.acromantula.gui.controller.utility

import javafx.scene.Node
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory

/**
 * Entity class containing style information for meta text
 */
data class EditorTextStyle(val styleClasses: Collection<String> = emptyList(),
                           val onClick: ((Node) -> Unit)? = null,
                           val contextFactory: ContextMenuFactory<Any>? = null) {
    companion object {
        val NO_STYLE = EditorTextStyle()
    }
}
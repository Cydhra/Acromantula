package net.cydhra.acromantula.gui.controller.components

import javafx.scene.Parent
import net.cydhra.acromantula.concurrency.ThreadPool
import net.cydhra.acromantula.concurrency.tasks.ContentOperationTask
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.database.META_DATA_KEY_ENCODING
import net.cydhra.acromantula.files.utility.model.JumpTarget
import net.cydhra.acromantula.gui.controller.EditorTabView
import net.cydhra.acromantula.plugins.internal.InternalPlugin
import net.cydhra.acromantula.workspace.SynchronizationHelper
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.nio.charset.Charset

/**
 * A JavaFX view controller for java editor views
 */
class TextEditorView(displayedFile: FileBlob, updateContentFunction: (EditorTabView, FileBlob, JumpTarget?) -> Unit) :
        EditorTabView(displayedFile, updateContentFunction) {

    companion object {
        private val logger = LogManager.getLogger()
    }

    override val root: Parent by fxml("/view/editors/text_pane.fxml")
    val codeArea: EditorTextArea by fxid()

    fun onButtonSave() {
        ThreadPool.submit(
                ContentOperationTask<Unit>(
                        title = "Edit Text ${displayedFile.filename}",
                        msg = "saving text into database...",
                        operation = {
                            transaction {
                                this@TextEditorView.displayedFile.data = connection.createBlob().apply {
                                    setBytes(1L,
                                            codeArea.text.toByteArray(
                                                    Charset.forName(displayedFile.metadataMap[META_DATA_KEY_ENCODING]
                                                            ?: InternalPlugin.defaultEncoding)
                                            ))
                                }
                                SynchronizationHelper.reportFileChange(displayedFile)
                            }
                        }
                )) {
            onSuccess {
                logger.debug("successfully edited raw text of ${displayedFile.filename}")
            }
            onFailure { t ->
                logger.error("error while saving text of ${displayedFile.filename}", t)
            }
            onCancelled {
                logger.debug("cancelled raw text edit of ${displayedFile.filename}")
            }
        }
    }

    override fun attemptJump(jumpTarget: JumpTarget) {
        this.jumpTargets[jumpTarget]?.also {
            this.codeArea.showParagraphInViewport(it)
        }
    }
}
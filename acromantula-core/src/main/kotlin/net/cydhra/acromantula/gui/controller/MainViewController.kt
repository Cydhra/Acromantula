package net.cydhra.acromantula.gui.controller

import javafx.application.Platform
import javafx.collections.ListChangeListener
import javafx.concurrent.Task
import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.control.Label
import javafx.scene.control.SplitPane
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.input.DragEvent
import javafx.scene.input.TransferMode
import javafx.scene.layout.Pane
import javafx.stage.FileChooser
import kotlinx.coroutines.launch
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.global.FileChangedEvent
import net.cydhra.acromantula.bus.event.local.*
import net.cydhra.acromantula.concurrency.ThreadPool
import net.cydhra.acromantula.concurrency.tasks.ContentOperationTask
import net.cydhra.acromantula.concurrency.tasks.ReadOperationTask
import net.cydhra.acromantula.config.ConfigurationManager
import net.cydhra.acromantula.config.GlobalConfigurationSpec
import net.cydhra.acromantula.files.FileHandlerService
import net.cydhra.acromantula.files.model.DefaultWorkspaceEntry
import net.cydhra.acromantula.files.model.WorkspaceEntry
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.gui.DisplayService
import net.cydhra.acromantula.gui.controller.components.TreeViewWithItems
import net.cydhra.acromantula.gui.controller.utility.showFileDialog
import net.cydhra.acromantula.gui.controller.utility.showStageCallbackDialog
import net.cydhra.acromantula.workspace.WorkspaceConfiguration
import net.cydhra.acromantula.workspace.sanitizing.InspectionIssue
import net.cydhra.acromantula.workspace.sanitizing.InspectionRunner
import net.cydhra.acromantula.workspace.sanitizing.Sanitizer
import org.apache.logging.log4j.LogManager
import org.controlsfx.control.TaskProgressView
import org.jetbrains.exposed.sql.transactions.transaction
import tornadofx.*
import java.io.File

private const val MAIN_VIEW_DEFINITION = "/view/main.fxml"

/**
 * Controller of the main view. The view itself is configured by the [MAIN_VIEW_DEFINITION] file.
 */
class MainViewController : View() {
    companion object {
        private val logger = LogManager.getLogger()
    }

    override val root: Parent by fxml(MAIN_VIEW_DEFINITION)

    private val workspaceFileExtensionFilter = listOf(FileChooser.ExtensionFilter("Workspace Database", "*.db"))

    private val contentPane: SplitPane by fxid()
    private val fileTreeView: TreeViewWithItems =
            TreeViewWithItems(DefaultWorkspaceEntry(null, "workspace", "/images/workspace.svg"))

    private val tabPaneCode: TabPane by fxid()
    private val statusPane: Pane by fxid()
    private val tasksLabel: Label by fxid()
    private val taskProgressView: TaskProgressView<Task<*>> = TaskProgressView()

    private val tabs = mutableListOf<EditorTabView>()

    init {
        this.title = "Acromantula"

        contentPane.items.add(0, fileTreeView)
        SplitPane.setResizableWithParent(fileTreeView, false)

        fileTreeView.setItems(FileHandlerService.sortedLoadedItems)

        // set current background in task progress view
        taskProgressView.tasks.setAll(ThreadPool.scheduledTaskList)
        tasksLabel.text = getTasksLabelText()

        // add a listener updating the shown number of running background tasks and the task progress view
        ThreadPool.scheduledTaskList.addListener(ListChangeListener<Task<*>> { change ->
            Platform.runLater {
                tasksLabel.text = getTasksLabelText()
            }
            while (change.next()) {
                // precalculate the lists to update, as calculating them in the Platform.runLater call can lead to
                // race conditions
                val sublist = change.addedSubList
                val removed = change.removed
                Platform.runLater {
                    taskProgressView.tasks.addAll(sublist)
                    taskProgressView.tasks.removeAll(removed)
                }
            }
        })

        // add the task progress view to the context menu of the task status label
        tasksLabel.contextMenu = contextmenu {
            @Suppress("MoveLambdaOutsideParentheses")
            separator({
                add(taskProgressView.apply {
                    paddingAll = .0
                })
            })
        }

        ThreadPool.coroutineScope.launch {
            EventBroker.registerEventListener(DisplayService, OpenEditorTabRequest::class,
                    this@MainViewController::onOpenEditorTabRequest)
            EventBroker.registerEventListener(DisplayService, ConnectToWorkspaceRequest::class,
                    this@MainViewController::onConnectToWorkspace)
            EventBroker.registerEventListener(DisplayService, FileChangedEvent::class,
                    this@MainViewController::onFileChange)
            EventBroker.registerEventListener(DisplayService, JumpInCodeRequest::class,
                    this@MainViewController::onJumpRequest)
        }

        root.stylesheets.add(ConfigurationManager.config[GlobalConfigurationSpec.theme])

        EventBroker.fireEvent(UserInterfaceInitializedEvent())
    }

    /**
     * Handler for menu item in view. Closes the application.
     */
    @Suppress("unused")
    fun onMenuItemClose() {
        Platform.exit()
    }

    /**
     * Called upon an action on the menu entry for opening a workspace. It will show a [FileChooser] and on success setting a new workspace
     * as active. The workspace is not created but loaded from a file.
     */
    @Suppress("unused")
    fun onMenuItemOpenWorkspace() {
        // TODO ask for export of default workspace
        val exportDefaultWorkspace = false

        // TODO ask whether to create new workspace or join remote workspace
        val joinRemote = false

        if (joinRemote) {
            // if remote: ask for adress and credentials

            EventBroker.fireEvent(ConnectToWorkspaceRequest(WorkspaceConfiguration(
                    url = TODO(),
                    username = TODO(),
                    password = TODO(),
                    local = false
            )))
        } else {
            // if local: ask for file

            // show file chooser to save new workspace
            val workspaceFile = showFileDialog(
                    title = "Connect To Workspace",
                    extensionFilters = workspaceFileExtensionFilter,
                    chooserTypeFunction = FileChooser::showOpenDialog,
                    parentWindow = this.currentWindow
            )

            if (workspaceFile != null) {
                // ask for credentials
                val (username, password) = "" to ""


                val event = ConnectToWorkspaceRequest(WorkspaceConfiguration(
                        url = "jdbc:h2:file:${workspaceFile.absoluteFile.absolutePath.removeSuffix(".mv.db")}",
                        username = username,
                        password = password,
                        local = true
                ))
                event.onFulfill {
                    FileHandlerService.loadWorkspace()
                }
                EventBroker.fireEvent(event)
            }
        }
    }

    /**
     * Ask the user, whether they want to save the current default workspace.
     *
     * @return true, if the user did not cancel the action allowing the caller to continue its current action, false otherwise
     */
    private fun askForDefaultWorkspaceSaving(): Boolean {
        TODO()
    }

    /**
     * Called upon an action on the menu entry for importing a JAR. It will show a [FileChooser] and then instruct the current workspace to
     * import it.
     */
    @Suppress("unused")
    fun onMenuItemImportJAR() {
        val file = showFileDialog(
                title = "Import Archive",
                extensionFilters = listOf(FileChooser.ExtensionFilter("Java Archive", "*.jar")),
                parentWindow = this.currentWindow
        )

        if (file != null) {
            importFiles(file)
        }
    }

    @Suppress("unused")
    fun onMenuItemRunInspections() {
        val dialog = RunInspectionsDialogController(this.fileTreeView.root)
        showStageCallbackDialog(
                title = "Run Inspections",
                content = dialog,
                resolveOptions = *arrayOf(
                        "Run" to { _ ->
                            val files = dialog.getSelectedWorkspaceFiles()
                            val inspections = dialog.getSelectedInspectionNames()

                            ThreadPool.submit(
                                    ReadOperationTask<Pair<List<InspectionRunner<Any>>, List<InspectionIssue<Any>>>>(
                                            title = "Run Inspections",
                                            msg = "Execute Runners",
                                            operation = {
                                                Sanitizer.executeRunners(files, inspections)
                                            }
                                    )) {
                                onFailure { t ->
                                    logger.error("error while executing inspection runners", t)
                                }
                                onSuccess { (runners, issues) ->
                                    showStageCallbackDialog(
                                            title = "Resolve Issues",
                                            content = ResolveIssuesDialogController(issues),
                                            resolveOptions = *arrayOf(
                                                    "Finish" to { _ ->
                                                        ThreadPool.submit(ContentOperationTask<Unit>(
                                                                title = "Resolve Issues",
                                                                msg = "Saving Entites",
                                                                operation = {
                                                                    runners.forEach(InspectionRunner<Any>::writeBackEntity)
                                                                }
                                                        )) {
                                                            onFailure { t ->
                                                                logger.error("error while saving inspection runners", t)
                                                            }
                                                        }
                                                    }
                                            )
                                    )
                                }
                            }
                        },
                        "Cancel" to { _ -> }))
    }

    /**
     * Event handler for drag and drop handling. Responds to a drag-and-drop mouse entry event by showing acceptance for
     * [TransferMode.COPY_OR_MOVE] actions and refusing other DnD actions.
     *
     * @param event the [DragEvent] that is handled
     */
    @Suppress("unused")
    fun onDragEntered(event: DragEvent) {
        if (event.gestureSource != this.root && event.dragboard.hasFiles()) {
            event.acceptTransferModes(*TransferMode.COPY_OR_MOVE)
        }
        event.consume()
    }

    /**
     * Event handler for drag and drop handling. Responds to a dropped file by loading it into workspace or ignoring the action if no file is
     * involved.
     *
     * @param event the [DragEvent] that is handled
     */
    @Suppress("unused")
    fun onDragDropped(event: DragEvent) {
        event.isDropCompleted = event.dragboard.hasFiles()
        if (event.dragboard.hasFiles()) {
            importFiles(*event.dragboard.files.toTypedArray())
        }

        event.consume()
    }

    /**
     * Import any number of files into the workspace. The import happens asynchronously.
     */
    private fun importFiles(vararg files: File) {
        EventBroker.fireEvent(ImportFilesRequest(files))
    }

    /**
     * @return the text the tasks status label shall be displaying
     */
    private fun getTasksLabelText(): String =
            "${when (ThreadPool.scheduledTaskList.size) {
                0 -> "0 tasks"
                1 -> "1 task"
                else -> "${ThreadPool.scheduledTaskList.size} tasks"
            }} running"

    /**
     * Event handler for [OpenEditorTabRequest]s that request any editor tab to be focussed or opened.
     */
    private suspend fun onOpenEditorTabRequest(event: OpenEditorTabRequest) {
        val existingTab = tabPaneCode.tabs.find { it.text == event.tabTitle && it.tag == event.tabTag }

        if (existingTab == null) {
            Platform.runLater {
                tabPaneCode
                        .also {
                            it.tabs.add(Tab(event.tabTitle, event.tabView.root).apply {
                                this.tag = event.tabTag
                                this.isClosable = true
                                this.onClosed = EventHandler {
                                    this@MainViewController.tabs.remove(event.tabView)
                                }
                            })
                            this.tabs.add(event.tabView)
                        }
                        .also { it.selectionModel.selectLast() }
            }
            event.fulfill()
        } else {
            Platform.runLater {
                tabPaneCode.selectionModel.select(existingTab)
            }
        }
    }

    private suspend fun onConnectToWorkspace(event: ConnectToWorkspaceRequest) {
        event.onFulfill {
            Platform.runLater {
                tabPaneCode.tabs.clear()
            }
        }
    }

    private suspend fun onFileChange(event: FileChangedEvent) {
        this.tabs
                .filter { it.displayedFile.id == event.file.id }
                .forEach { it.updateContentFunction(it, event.file, null) }
    }

    private suspend fun onJumpRequest(event: JumpInCodeRequest) {
        val openedTab = transaction {
            this@MainViewController.tabs
                    .filter { it.displayedFile.filename == event.codeFile.filename }
                    .firstOrNull { it.jumpTargets.containsKey(event.jumpTarget) }
        }

        if (openedTab != null) {
            Platform.runLater {
                tabPaneCode.selectionModel.select(tabPaneCode.tabs.find { it.content == openedTab.root })
            }
            openedTab.attemptJump(event.jumpTarget)
            event.fulfill()
        } else {
            // recursive function to find the file entry for the given jump target
            fun TreeViewWithItems.findFileEntry(blob: FileBlob): FileEntry? {
                fun findItem(current: WorkspaceEntry): FileEntry? {
                    if (current is FileEntry) {
                        if (current.fileBlob.filename == blob.filename) {
                            return current
                        }
                    } else {
                        for (child in current.children) {
                            val entry = findItem(child)
                            if (entry != null)
                                return entry
                        }
                    }

                    return null
                }

                this.getItems()?.forEach { item ->
                    val entry = findItem(item)
                    if (entry != null)
                        return entry
                }

                return null
            }

            val entry = fileTreeView.findFileEntry(event.codeFile)!!
            EventBroker.fireEvent(DefaultFileActionRequest(entry, event.jumpTarget))
            event.fulfill()
        }
    }


}
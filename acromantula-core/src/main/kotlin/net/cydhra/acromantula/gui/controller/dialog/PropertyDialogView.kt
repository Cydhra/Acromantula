package net.cydhra.acromantula.gui.controller.dialog

import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.Tab
import javafx.scene.layout.Pane
import net.cydhra.acromantula.gui.controller.utility.StageDialog
import org.controlsfx.control.PropertySheet
import tornadofx.View

/**
 * A view that holds metadata and other properties of single files. It is used in a stage by
 * [net.cydhra.acromantula.plugins.internal.database.types.action.properties.ShowPropertiesAction]
 *
 * @param propertySheet a constructed [PropertySheet] for all metadata options available
 */
class PropertyDialogView(propertySheet: PropertySheet) : View(), StageDialog {
    override val root: Parent by fxml("/view/dialog/metadata_wrapper.fxml")

    private val metadataTab: Tab by fxid()

    private val resolvePane: Pane by fxid()

    init {
        metadataTab.content = propertySheet
    }

    override fun addButton(button: Button) {
        resolvePane.add(button)
    }


}
package net.cydhra.acromantula.gui.controller.context

import javafx.application.Platform
import javafx.scene.control.ContextMenu
import net.cydhra.acromantula.concurrency.ThreadPool
import net.cydhra.acromantula.concurrency.tasks.ActionTask
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.types.ContentOperation
import net.cydhra.acromantula.gui.controller.utility.showErrorDialog
import org.apache.logging.log4j.LogManager
import tornadofx.item

/**
 * UI Components that are dynamically created during application execution may need context menus.
 *
 * @param E entity type for the context menu
 */
open class ContextMenuFactory<E>(private val fileBlob: FileBlob?,
                                 private val entity: E,
                                 private vararg val actions: Pair<() -> Boolean, ContentOperation<E>>) {

    companion object {
        private val logger = LogManager.getLogger()
    }

    /**
     * Create a context menu for the given item
     *
     * @param actions an array of actions that can be executed from this context
     *
     * @return a [ContextMenu] if the item offers any context actions or null otherwise
     */
    fun create(): ContextMenu? {
        if (fileBlob == null)
            return null

        return ContextMenu().apply {
            actions.forEach { (expression, action) ->
                item(action.name) {
                    if (expression()) {
                        setOnAction {
                            ThreadPool.submit(ActionTask(action, fileBlob, entity)) {
                                onFailure { t ->
                                    logger.error("error during \"${action.name}\" task execution", t)
                                    Platform.runLater {
                                        showErrorDialog("${action.name}: Error", t.message!!)
                                    }
                                }
                            }
                        }
                    } else {
                        isDisable = true
                    }
                }
            }
        }
    }

    /**
     * A context for the DSL syntax of constructing context menus
     */
    class ActionArrayContext<E> {
        val actions = mutableListOf<Pair<() -> Boolean, ContentOperation<E>>>()

        fun add(action: ContentOperation<E>) {
            actions += { true } to action
        }

        /**
         * Add an action to the context menu if the given condition is met
         *
         * @param condition a boolean expression that determines whether the action is active
         */
        fun ifTrue(condition: () -> Boolean, action: ContentOperation<E>) {
            actions += condition to action
        }

    }
}

/**
 * Utility function to construct an array of actions corresponding to the context menu entries. The actions are
 * guarded by conditions that must be met for the action to be active
 */
fun <E> constructActionArray(block: ContextMenuFactory.ActionArrayContext<E>.() -> Unit):
        Array<Pair<() -> Boolean, ContentOperation<E>>> {
    return ContextMenuFactory.ActionArrayContext<E>().apply(block).actions.toTypedArray()
}
package net.cydhra.acromantula.gui

import de.codecentric.centerdevice.javafxsvg.SvgImageLoaderFactory
import de.codecentric.centerdevice.javafxsvg.dimension.PrimitiveDimensionProvider
import javafx.application.Application
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.ApplicationCloseEvent
import net.cydhra.acromantula.bus.service.Service
import net.cydhra.acromantula.gui.controller.MainViewController
import org.apache.logging.log4j.LogManager
import tornadofx.App
import tornadofx.UIComponent

/**
 * Facade service for the graphical user interface. GUI is launched and controlled from here.
 */
object DisplayService : Service {

    override val name: String = "display-service"

    private val logger = LogManager.getLogger()

    private lateinit var mainView: MainViewController

    /**
     * Initialize everything GUI related.
     */
    override suspend fun initialize() {
        SvgImageLoaderFactory.install()
        SvgImageLoaderFactory.install(PrimitiveDimensionProvider())



        Thread {
            Application.launch(AcromantulaApp::class.java)
        }.start()
    }

    /**
     * Acromantula Java FX Application. Loads primary view and initializes/cleans up the main resources of the application.
     */
    class AcromantulaApp : App(MainViewController::class) {

        override fun onBeforeShow(view: UIComponent) {
            mainView = view as MainViewController
        }

        override fun init() {
            logger.info("initializing complete")
        }

        override fun stop() {
            EventBroker.fireEvent(ApplicationCloseEvent())
        }
    }
}


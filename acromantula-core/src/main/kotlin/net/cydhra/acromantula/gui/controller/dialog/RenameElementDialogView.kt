package net.cydhra.acromantula.gui.controller.dialog

import javafx.collections.ObservableList
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.scene.layout.Pane
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.JumpInCodeRequest
import net.cydhra.acromantula.files.operations.refactoring.model.RefactorableEntity
import net.cydhra.acromantula.gui.controller.utility.StageDialog
import tornadofx.View
import tornadofx.onDoubleClick

class RenameElementDialogView(references: ObservableList<RefactorableEntity>,
                              originalName: String) : View(), StageDialog {

    override val root: Parent by fxml("/view/dialog/rename_element_wrapper.fxml")

    /**
     * Pane for all dialog resolving buttons
     */
    private val resolvePane: Pane by fxid()

    /**
     * The text field were to write the new name
     */
    private val nameTextField: TextField by fxid()

    /**
     * List view for all references that get updated
     */
    private val referencesListView: ListView<RefactorableEntity> by fxid()

    init {
        referencesListView.items = references
        nameTextField.text = originalName
        nameTextField.selectAll()
        nameTextField.requestFocus()

        referencesListView.onDoubleClick {
            val selectedItem = referencesListView.selectionModel.selectedItem
            if (selectedItem != null) {
                val jumpRequest = JumpInCodeRequest(selectedItem.codeFileBlob,
                        selectedItem.codeReference.jumpTarget)
                EventBroker.fireEvent(jumpRequest)
            }
        }
    }

    override fun addButton(button: Button) {
        resolvePane.add(button)
    }

    /**
     * @return the name the current method name shall be replaced with
     */
    fun getNewMemberName(): String {
        return nameTextField.text
    }
}
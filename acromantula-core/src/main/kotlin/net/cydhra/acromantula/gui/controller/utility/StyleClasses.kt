package net.cydhra.acromantula.gui.controller.utility

const val TEXT_CODE = "syntax-code"
const val CODE_KEYWORD = "syntax-keyword"
const val CODE_INSTRUCTION = "syntax-instruction"
const val CODE_DESCRIPTOR = "syntax-descriptor"
const val CODE_STRING = "syntax-string-literal"
const val CODE_LITERAL = "syntax-number-literal"
const val CODE_LABEL = "syntax-label"
const val CODE_COMMENT = "syntax-comment"
const val CODE_PRAGMA = "syntax-comment"
const val CODE_OPERATOR = "syntax-operator"
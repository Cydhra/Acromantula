package net.cydhra.acromantula.gui.controller

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.utility.model.JumpTarget
import tornadofx.View

abstract class EditorTabView(val displayedFile: FileBlob,
                             val updateContentFunction: (EditorTabView, FileBlob, JumpTarget?) -> Unit) : View() {
    var jumpTargets: Map<JumpTarget, Int> = emptyMap()

    /**
     * Attempt to jump a the given jump target
     */
    abstract fun attemptJump(jumpTarget: JumpTarget)
}
package net.cydhra.acromantula.gui.controller.utility

import javafx.scene.Node
import net.cydhra.acromantula.files.utility.model.JumpTarget
import net.cydhra.acromantula.gui.controller.context.ContextMenuFactory
import org.fxmisc.richtext.model.StyleSpan

class MetaText {
    var text: String = ""
        private set

    private val stylesList = mutableListOf<StyleSpan<EditorTextStyle>>()

    val styleSpans: List<StyleSpan<EditorTextStyle>> = stylesList

    /**
     * A map of paragrpah indices mapped to a list of jump targets within that paragraph
     */
    private val paragraphJumpTargetList = mutableMapOf<JumpTarget, Int>()

    val jumpTargets: Map<JumpTarget, Int> = paragraphJumpTargetList

    /**
     * Append a piece of text with given style classes, on-click callback and a context menu builder.
     *
     * @param text the chunk of text
     * @param onClick a callback function invoked on clicking the text
     * @param contextFactory a factory for context menus for meta text nodes
     * @param styleClasses an array of style classes to be applied to the chunk of text
     */
    @Suppress("UNCHECKED_CAST")
    fun append(text: String, onClick: ((Node) -> Unit)? = null,
               contextFactory: ContextMenuFactory<*>? = null, vararg styleClasses: String) {
        this.text += text
        stylesList.add(StyleSpan(EditorTextStyle(styleClasses.toList(),
                onClick, contextFactory as ContextMenuFactory<Any>?), text.length))
    }

    fun appendParagraphJumpTargets(paragraphCounter: Int, currentParagraphJumpTargets: MutableList<JumpTarget>) {
        currentParagraphJumpTargets.forEach { target ->
            paragraphJumpTargetList[target] = paragraphCounter
        }
    }
}
package net.cydhra.acromantula.gui.controller.dialog

import javafx.collections.ObservableList
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ListView
import javafx.scene.layout.Pane
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.bus.event.local.JumpInCodeRequest
import net.cydhra.acromantula.files.utility.model.CodeReference
import net.cydhra.acromantula.gui.controller.utility.StageDialog
import tornadofx.View
import tornadofx.onDoubleClick

/**
 * A dialog that shows all references to a member and offers to jump their on double click
 */
class ShowReferencesDialogView(references: ObservableList<CodeReference>, name: String) : View(), StageDialog {

    override val root: Parent by fxml("/view/dialog/show_references_wrapper.fxml")

    /**
     * Pane for all dialog resolving buttons
     */
    private val resolvePane: Pane by fxid()

    /**
     * The text field were to write the new name
     */
    private val nameTextLabel: Label by fxid()

    /**
     * List view for all references that get updated
     */
    private val referencesListView: ListView<CodeReference> by fxid()

    init {
        referencesListView.items = references
        nameTextLabel.text = name

        referencesListView.onDoubleClick {
            val selectedItem = referencesListView.selectionModel.selectedItem
            if (selectedItem != null) {
                val jumpRequest = JumpInCodeRequest(selectedItem.codeFileBlob,
                        selectedItem.jumpTarget)
                EventBroker.fireEvent(jumpRequest)
            }
        }
    }

    override fun addButton(button: Button) {
        resolvePane.add(button)
    }
}
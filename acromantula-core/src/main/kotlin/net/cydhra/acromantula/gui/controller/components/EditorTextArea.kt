package net.cydhra.acromantula.gui.controller.components

import javafx.event.EventHandler
import net.cydhra.acromantula.gui.controller.utility.EditorTextStyle
import org.fxmisc.richtext.LineNumberFactory
import org.fxmisc.richtext.StyledTextArea
import org.fxmisc.richtext.TextExt
import java.util.function.BiConsumer

/**
 * A text area specifically designed to be used with [EditorTextStyle]d text
 */
class EditorTextArea : StyledTextArea<Unit, EditorTextStyle>(
        Unit,
        BiConsumer { _, _ -> },
        EditorTextStyle.NO_STYLE,
        BiConsumer { textFlow: TextExt, style: EditorTextStyle ->
            textFlow.styleClass.addAll(style.styleClasses)
            if (style.contextFactory != null) {
                textFlow.onContextMenuRequested = EventHandler { event ->
                    style.contextFactory.create()!!.show(textFlow, event.screenX,
                            event.screenY)
                    event.consume()
                }
            }

            textFlow.onMouseClicked = EventHandler { style.onClick?.invoke(textFlow) }
            textFlow.onMouseEntered =
                    EventHandler { if (style.onClick != null || style.contextFactory != null) textFlow.isUnderline = true }
            textFlow.onMouseExited = EventHandler { textFlow.isUnderline = false }
        }) {
    init {
        this.paragraphGraphicFactory = LineNumberFactory.get(this)
    }
}
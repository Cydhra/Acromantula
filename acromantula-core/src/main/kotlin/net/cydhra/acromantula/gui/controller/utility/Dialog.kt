package net.cydhra.acromantula.gui.controller.utility

import javafx.event.ActionEvent
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.ButtonBar
import javafx.scene.control.ButtonType
import javafx.stage.FileChooser
import javafx.stage.Stage
import javafx.stage.Window
import net.cydhra.acromantula.config.ConfigurationManager
import net.cydhra.acromantula.config.GlobalConfigurationSpec
import tornadofx.View
import java.io.File
import java.util.*

/**
 * Show a dialog with given title and content giving options "Yes", "No" and "Cancel" to the user. The dialog is shown and the thread is
 * blocked until the user chose an action.
 *
 * @param title message title
 * @param content message content text
 *
 * @return optional [ButtonType]. Can be used to react to the user's action.
 */
fun showYesNoCancelDialog(title: String, content: String): Optional<ButtonType> {
    val alert = Alert(Alert.AlertType.CONFIRMATION)

    alert.title = title
    alert.contentText = content

    alert.buttonTypes.setAll(
            ButtonType("Yes", ButtonBar.ButtonData.YES),
            ButtonType("No", ButtonBar.ButtonData.NO),
            ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE)
    )

    return alert.showAndWait()
}

/**
 * Show an error dialog with a title and message. The thread will wait until the user has confirmed the dialog.
 *
 * @param title dialog window title
 * @param content dialog text
 */
fun showErrorDialog(title: String, content: String) {
    val alert = Alert(Alert.AlertType.ERROR)
    alert.title = title
    alert.contentText = content
    alert.showAndWait()
}

/**
 * Show a file chooser dialog and return the file that was chosen or null, if the dialog was cancelled.
 *
 * @param title optional title of the dialog
 * @param extensionFilters optional list of extension filters
 * @param chooserTypeFunction optional function of [FileChooser] to be used as file chooser type. By default, an open file dialog is
 * displayed
 * @param parentWindow owner of the dialog
 *
 * @return the chosen file or null, if none was chosen
 */
fun showFileDialog(
        title: String = "Choose File",
        extensionFilters: Collection<FileChooser.ExtensionFilter> = listOf(FileChooser.ExtensionFilter("All Files", "*.*")),
        chooserTypeFunction: FileChooser.(Window?) -> File? = FileChooser::showOpenDialog,
        parentWindow: Window?): File? {
    val dialog = FileChooser()
    dialog.title = title
    dialog.extensionFilters += extensionFilters

    return dialog.chooserTypeFunction(parentWindow)
}

/**
 * Show a view as a dialog with a list of buttons and their respective callbacks to resolve it.
 *
 * @param title dialog title
 * @param content the view which is the dialog content. Must implement [StageDialog]
 * @param resolveOptions all options to resolve the dialog
 */
fun <T> showStageCallbackDialog(title: String, content: T, vararg resolveOptions: Pair<String, (ActionEvent) -> Unit>)
        where T : View, T : StageDialog {
    resolveOptions.forEach { (text, callback) ->
        content.addResolveButton(text, callback)
    }

    val stage = Stage()
    stage.title = title
    stage.scene = Scene(content.root).apply {
        this.stylesheets.add(ConfigurationManager.config[GlobalConfigurationSpec.theme])
    }
    stage.show()
}
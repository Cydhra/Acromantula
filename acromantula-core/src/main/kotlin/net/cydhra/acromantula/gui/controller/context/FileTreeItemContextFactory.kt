package net.cydhra.acromantula.gui.controller.context

import net.cydhra.acromantula.files.FileTypeRegistry
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.model.file.FileEntry

/**
 * Context menu strategy for items of the file tree view
 */
class FileTreeItemContextFactory(file: FileBlob, fileEntry: FileEntry) :
        ContextMenuFactory<FileEntry>(
                fileBlob = file,
                entity = fileEntry,
                actions = *FileTypeRegistry.getFileTypeActions(file.fileType).map { { true } to it }.toTypedArray()
        )
package net.cydhra.acromantula.gui.controller.utility

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.stage.WindowEvent

/**
 * A full-grown stage used as a dialog with different options to resolve it.
 */
interface StageDialog {

    /**
     * Add a button for resolving the dialog with a custom callback function.
     *
     * @param buttonText the button text
     * @param callback the function called upon button press
     */
    fun addResolveButton(buttonText: String, callback: (ActionEvent) -> Unit) {
        val button = Button()
        button.text = buttonText
        button.onAction = EventHandler { e ->
            callback(e)
            (e.source as Node).scene.window.fireEvent(WindowEvent((e.source as Node).scene.window,
                    WindowEvent.WINDOW_CLOSE_REQUEST))
        }
        button.prefWidth = 100.0
        this.addButton(button)
    }

    /**
     * Add a button to the bottom of the dialog
     *
     * @param button [Button] instance
     */
    fun addButton(button: Button)


}
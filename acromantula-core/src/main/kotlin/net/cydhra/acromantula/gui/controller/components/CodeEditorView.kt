package net.cydhra.acromantula.gui.controller.components

import javafx.application.Platform
import javafx.scene.Parent
import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.utility.model.JumpTarget
import net.cydhra.acromantula.gui.controller.EditorTabView
import org.fxmisc.richtext.NavigationActions

/**
 * A JavaFX view controller for java editor views
 */
class CodeEditorView(displayedFile: FileBlob, contentUpdateFunction: (EditorTabView, FileBlob, JumpTarget?) -> Unit)
    : EditorTabView(displayedFile, contentUpdateFunction) {
    override val root: Parent by fxml("/view/editors/code_pane.fxml")
    val codeArea: EditorTextArea by fxid()

    override fun attemptJump(jumpTarget: JumpTarget) {
        this.jumpTargets[jumpTarget]?.also {
            Platform.runLater {
                this.codeArea.showParagraphInViewport(it)
                this.codeArea.moveTo(it, 0, NavigationActions.SelectionPolicy.CLEAR)
                this.codeArea.moveTo(it, this.codeArea.getParagraphLength(it), NavigationActions.SelectionPolicy.ADJUST)
            }
        }
    }
}
package net.cydhra.acromantula.plugins.internal

import com.google.auto.service.AutoService
import com.uchuhimo.konf.ConfigSpec
import net.cydhra.acromantula.config.ConfigurationManager
import net.cydhra.acromantula.config.getValue
import net.cydhra.acromantula.config.setValue
import net.cydhra.acromantula.files.FileTypeRegistry
import net.cydhra.acromantula.files.inspections.ClassFileInspectionRunner
import net.cydhra.acromantula.files.inspections.SignatureInspection
import net.cydhra.acromantula.files.processors.JavaClassProcessor
import net.cydhra.acromantula.files.types.ClassFileType
import net.cydhra.acromantula.files.types.GenericBinaryFileType
import net.cydhra.acromantula.files.types.GenericCodeFileType
import net.cydhra.acromantula.files.types.action.decompile.DecompileAction
import net.cydhra.acromantula.files.types.action.disassembly.DisassembleClassAction
import net.cydhra.acromantula.files.types.action.properties.ShowPropertiesAction
import net.cydhra.acromantula.files.types.action.text.EditTextAction
import net.cydhra.acromantula.plugins.AcromantulaPlugin
import net.cydhra.acromantula.workspace.sanitizing.Sanitizer
import org.apache.logging.log4j.LogManager
import org.objectweb.asm.tree.ClassNode

@AutoService(AcromantulaPlugin::class)
class InternalPlugin : AcromantulaPlugin {
    companion object {
        private val logger = LogManager.getLogger()

        private object Config : ConfigSpec("base") {
            val defaultEncoding by optional("UTF-8")
        }

        var defaultEncoding by Config.defaultEncoding
    }

    override val name: String = "Base Content"
    override val author: String = "Acromantula"

    override fun initialize() {
        ConfigurationManager.addSpecification(Config)
        ConfigurationManager.reloadConfig()

        FileTypeRegistry.registerFileType(GenericBinaryFileType)
        FileTypeRegistry.registerFileType(GenericCodeFileType)
        FileTypeRegistry.registerFileType(ClassFileType)

        FileTypeRegistry.registerFileAction(ClassFileType, DisassembleClassAction, isDefaultAction = true)
        FileTypeRegistry.registerFileAction(ClassFileType, DecompileAction)
        FileTypeRegistry.registerFileAction(GenericBinaryFileType, EditTextAction, isDefaultAction = true)
        FileTypeRegistry.registerFileAction(GenericBinaryFileType, ShowPropertiesAction)

        FileTypeRegistry.registerFileInspectionRunner(ClassFileType, ::ClassFileInspectionRunner)

        FileTypeRegistry.registerFileProcessor(ClassFileType, JavaClassProcessor)

        Sanitizer.registerInspection(ClassNode::class, "Java Class Inspections", SignatureInspection)

        logger.info("loaded acromantula base plugin")
    }

}
package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.files.model.file.FileEntry
import net.cydhra.acromantula.files.utility.model.JumpTarget

/**
 * Request the execution of the default action for the given file. Request can only be fulfilled if a default action
 * has been specified.
 *
 * @param file the file which default action shall be executed
 * @param jumpTarget if the action is a view action, a jump target for the view can be defined. If a jump target is
 * given and the default action is not a view action, an exception will be thrown by the event handler
 */
data class DefaultFileActionRequest(val file: FileEntry, val jumpTarget: JumpTarget?) : Request()
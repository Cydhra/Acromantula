package net.cydhra.acromantula.bus.event.global

import net.cydhra.acromantula.files.model.WorkspaceEntry

/**
 * Global event fired when new files are added to the workspace.
 *
 * @param entry the workspace entry containing the imported file tree
 * @param importedLocally whether this client was responsible for the import
 */
data class AddFilesEvent(val entry: WorkspaceEntry, val importedLocally: Boolean) : GlobalEvent()
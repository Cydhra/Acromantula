package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.bus.event.Event

/**
 * Fired by the display service when the user interface is ready. Not fired, if the user interface service is not
 * being loaded.
 */
class UserInterfaceInitializedEvent : Event
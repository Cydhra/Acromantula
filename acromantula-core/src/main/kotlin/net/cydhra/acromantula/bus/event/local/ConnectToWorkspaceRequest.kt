package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.workspace.WorkspaceConfiguration

/**
 * Fired when the user wants to connect to a new workspace.
 */
data class ConnectToWorkspaceRequest(val workspaceConfiguration: WorkspaceConfiguration) : Request()
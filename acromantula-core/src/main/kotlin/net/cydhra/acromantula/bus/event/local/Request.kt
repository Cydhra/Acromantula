package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.bus.event.Event

abstract class Request : Event {

    private var fulfilled = false

    private var callbackList = mutableListOf<() -> Unit>()

    /**
     * Immutable view on all registered callbacks for event fulfillment.
     */
    val callbacks: List<() -> Unit> = callbackList

    /**
     * Mark the request as fulilled. A listener does this to trigger execution of the fulfillment callback.
     */
    fun fulfill() {
        this.fulfilled = true
    }

    /**
     * @return whether any listener called [fulfill] marking the request as sufficiently handled.
     */
    fun wasFulfilled(): Boolean {
        return this.fulfilled
    }

    /**
     * Set a callback function that is lauched in a new coroutine when the event gets fulfilled by at least one
     * listener.
     */
    fun onFulfill(callback: () -> Unit) {
        this.callbackList.add(callback)
    }
}
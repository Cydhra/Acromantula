package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.gui.controller.EditorTabView

/**
 * Fired to request the opening of a tab in the editor. Handled by display service. Should not be fired by other
 * subsystems, as the display service might not be active thus rendering the event a noop.
 */
data class OpenEditorTabRequest(val tabTitle: String, val tabTag: String, val tabView: EditorTabView) : Request()
package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.bus.event.Event

/**
 * Event that is dispatched when the application main window closes and the application is required to shut down
 */
class ApplicationCloseEvent : Event
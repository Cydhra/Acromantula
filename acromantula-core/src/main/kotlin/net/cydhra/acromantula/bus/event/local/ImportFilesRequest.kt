package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.bus.event.Event
import java.io.File

/**
 * Fired when the user requests files to be imported
 */
data class ImportFilesRequest(val files: Array<out File>) : Event
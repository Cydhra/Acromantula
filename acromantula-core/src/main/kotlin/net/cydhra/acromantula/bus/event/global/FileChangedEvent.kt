package net.cydhra.acromantula.bus.event.global

import net.cydhra.acromantula.files.model.database.FileBlob

/**
 * Fired when the given [file] changes content during a critical database interaction.
 */
data class FileChangedEvent(val file: FileBlob) : GlobalEvent()
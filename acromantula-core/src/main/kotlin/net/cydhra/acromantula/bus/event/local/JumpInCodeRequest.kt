package net.cydhra.acromantula.bus.event.local

import net.cydhra.acromantula.files.model.database.FileBlob
import net.cydhra.acromantula.files.utility.model.JumpTarget

/**
 * Request to jump to a specific location in code
 */
data class JumpInCodeRequest(val codeFile: FileBlob, val jumpTarget: JumpTarget) : Request()
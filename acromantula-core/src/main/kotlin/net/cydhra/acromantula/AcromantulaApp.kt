package net.cydhra.acromantula

import kotlinx.coroutines.runBlocking
import net.cydhra.acromantula.bus.EventBroker
import net.cydhra.acromantula.files.FileHandlerService
import net.cydhra.acromantula.gui.DisplayService
import net.cydhra.acromantula.plugins.PluginService
import net.cydhra.acromantula.workspace.WorkspaceService
import org.apache.logging.log4j.LogManager


const val MAX_IDENTIFIER_LENGTH = 65535

private val logger = LogManager.getLogger()

fun main() {
    logger.info("application started")
    runBlocking {
        EventBroker.registerService(EventBroker)
        EventBroker.registerService(PluginService)
        EventBroker.registerService(FileHandlerService)
        EventBroker.registerService(WorkspaceService)
        EventBroker.registerService(DisplayService)
    }
}

@Deprecated("refactoring into service architecture pending")
class AcromantulaApp
UPDATE JavaFields J
SET J.type = (
  SELECT JAVACLASSES.id
  FROM JAVAIDENTIFIERS
         INNER JOIN JAVAFIELDS ON (JAVAFIELDS.DESCRIPTOR = CONCAT('L', JAVAIDENTIFIERS.IDENTIFIER, ';'))
         INNER JOIN JAVACLASSES ON (JAVACLASSES.IDENTIFIER = JAVAIDENTIFIERS.ID)
  WHERE JAVAFIELDS.ID = J.ID
  )
WHERE J.TYPE IS NULL